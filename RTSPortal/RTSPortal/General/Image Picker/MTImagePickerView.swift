//
//  MTImagePickerView.swift
//  RTSPortal
//
//  Created by sajan on 07/08/19.
//  Copyright © 2019 Bolas intelli Solutions. All rights reserved.
//

import UIKit

protocol MTImagePickerDelegate : NSObjectProtocol {
    
    func selectedImages(imagesArray:[UIImage])
    
}

class MTImagePickerView: UIView,UIGestureRecognizerDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate,ImagePickerViewControllerDelegate {
    
    var parentController : UIViewController!
    var actionView : UIView!
    var screenFrame:CGRect!
    weak var  delegate : MTImagePickerDelegate?
    
    init(withController ViewController:UIViewController ) {
        
        self.parentController = ViewController
        
        if let viewFrame = self.parentController.navigationController?.view.frame{
            
            super.init(frame: viewFrame)
            self.backgroundColor = UIColor.init(white: 0, alpha: 0.8)
            screenFrame = viewFrame
        }
        else{
            
            super.init(frame: self.parentController.view.frame)
            screenFrame = self.parentController.view.frame
        }
        self.createActionView()
        
        let tapRecogonizer = UITapGestureRecognizer.init(target: self, action: #selector(removeView))
            self.addGestureRecognizer(tapRecogonizer)
            tapRecogonizer.delegate = self
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func createActionView(){
        let SCREENHEIGHT = screenFrame.size.height
        let SCREENWIDTH  = screenFrame.size.width
        let VIEWHEIGHT = SCREENHEIGHT * 0.3
        let VIEWWIDTH  = SCREENWIDTH - 40 // 20 padding
        let actionframe = CGRect.init(x: 20, y:( (SCREENHEIGHT/2) - VIEWHEIGHT/2), width: VIEWWIDTH, height: VIEWHEIGHT)
        actionView = UIView.init(frame: actionframe)
        actionView.backgroundColor = UIColor.lightGray
//        actionView.backgroundColor = UIColor(red: 28.0/255.0 , green: 20.0/255.0, blue: 50.0/255.0, alpha: 1.0)
        self.addSubview(actionView)
        /*
        //let BUTTONHEIGHT : CGFloat = 50
//        let BUTTONWIDTH  : CGFloat = ((VIEWWIDTH-60)/2)
//        let BUTTONHEIGHT : CGFloat = BUTTONWIDTH * 0.88
//        let cameraFrame = CGRect.init(x: 20, y: (VIEWHEIGHT/2)-BUTTONHEIGHT/2 , width: BUTTONWIDTH, height: BUTTONHEIGHT)
//        let xPosition : CGFloat = VIEWWIDTH / 3
//
//       let cameraFrame = CGRect.init(x: xPosition-, y: (VIEWHEIGHT/2)-BUTTONHEIGHT/2 , width: BUTTONWIDTH, height: BUTTONHEIGHT)
        */
        
        let BUTTONWIDTH  : CGFloat = VIEWWIDTH * 0.25
        let BUTTONHEIGHT : CGFloat = BUTTONWIDTH
        let padding  = (VIEWWIDTH - (2*BUTTONWIDTH))/3
        let cameraX  = padding
        let cameraFrame = CGRect.init(x: padding, y: (VIEWHEIGHT/2)-BUTTONHEIGHT/2 , width: BUTTONWIDTH, height: BUTTONHEIGHT)
        
        
        let cameraButton = UIButton.init(frame: cameraFrame)
           // cameraButton.setTitle("camera", for: .normal)
            cameraButton.setBackgroundImage(UIImage.init(named: "camera_icon_blue"), for: .normal)
            cameraButton.setTitleColor(UIColor.black, for: .normal)
//            cameraButton.backgroundColor = UIColor.red
        
        let galleryFrame =  CGRect.init(x:(2*padding)+BUTTONWIDTH, y: VIEWHEIGHT/2-BUTTONHEIGHT/2 , width: BUTTONWIDTH, height: BUTTONHEIGHT)
        
        let galleryButton  = UIButton.init(frame: galleryFrame)
//            galleryButton.backgroundColor = UIColor.green
            galleryButton.setTitleColor(UIColor.black, for: .normal)
            galleryButton.setBackgroundImage(UIImage.init(named: "gallery_icon_blue"), for: .normal)
        actionView.addSubview(galleryButton)
        actionView.addSubview(cameraButton)
        
        let cameralbl = UILabel.init(frame: cameraFrame)
            cameralbl.frame.origin.y += cameraButton.frame.size.height
            cameralbl.frame.size.height = 50;
            cameralbl.text = "Camera"
            cameralbl.textColor = UIColor.white
            cameralbl.textAlignment = .center
        
        let gallerylbl =  UILabel.init(frame: galleryFrame)
            gallerylbl.frame.origin.y += galleryButton.frame.size.height
            gallerylbl.frame.size.height = 50;
            gallerylbl.text = "Gallery"
            gallerylbl.textColor = UIColor.white
            gallerylbl.textAlignment = .center
        
        actionView.addSubview(gallerylbl)
        actionView.addSubview(cameralbl)
        
        cameraButton.addTarget(self, action: #selector(showCamera), for: .touchUpInside)
        
        galleryButton.addTarget(self, action: #selector(showGallery), for: .touchUpInside)
    }
    
    @objc func showCamera (){
        
        print("tap on camera")
        self.showImagePickerWithSourcetype(sourceType:.camera)

    }
    @objc func showGallery(){
        
        self.showImagePickerWithSourcetype(sourceType:.photoLibrary)
        print("tap on gallery")
    }
    @objc func removeView(){
        
        self.removeFromSuperview()
        
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view?.isDescendant(of: self.actionView) == true {
            return false
        }
        return true
    }
    
    func showImagePickerWithSourcetype(sourceType:UIImagePickerController.SourceType)  {
        
        if sourceType == .photoLibrary {
        let storyboard = UIStoryboard(name: "General", bundle:Bundle.main)
        let imageController =  storyboard.instantiateViewController(withIdentifier: "ImagePickerViewController") as! ImagePickerViewController
        self.parentController.present(imageController, animated: true, completion: nil)
        imageController.delegate = self as ImagePickerViewControllerDelegate
        }
        else{
        
        
        let imagePicker = UIImagePickerController.init()
            imagePicker.sourceType = sourceType
            imagePicker.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imagePicker.allowsEditing = true
            self.parentController.present(imagePicker, animated: true, completion: nil)
        
        }
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let tempImage:UIImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        self.parentController.dismiss(animated: true, completion: nil)
        self.delegate?.selectedImages(imagesArray: [tempImage])
        self.removeFromSuperview()

    }
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]){
//        let tempImage:UIImage = info[UIImagePickerControllerOriginalImage] as! UIImage
//        self.parentController.dismiss(animated: true, completion: nil)
//        self.delegate?.selectedImages(imagesArray: [tempImage])
//        self.removeFromSuperview()
//
//    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        
        print("cancelled")
        self.parentController.dismiss(animated: true, completion: nil)
    }
    
    func choosenImages(imagesArray: [UIImage]) {
        
         self.removeFromSuperview()
         self.delegate?.selectedImages(imagesArray: imagesArray)
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
