//
//  ImageCollectionViewCell.swift
//  RTSPortal
//
//  Created by sajan on 07/08/19.
//  Copyright © 2019 Bolas intelli Solutions. All rights reserved.
//

import UIKit
import Photos

class ImageCollectionViewCell: UICollectionViewCell {
    @IBOutlet var thumbImage: UIImageView!
    @IBOutlet var tickImage: UIImageView!
    var imageManager : PHImageManager = PHImageManager.default()
    
//    required init?(coder aDecoder: NSCoder) {
//
//        super.init(coder: aDecoder)
//     //   fatalError("init(coder:) has not been implemented")
//    }
//
//    override func awakeFromNib() {
//
//        imageManager =  (imageManager == nil) ? PHImageManager.default() : imageManager
//
//    }
    
    func showImageFromPHAsset(phAsset:PHAsset)  {
        
        imageManager.requestImage(for: phAsset, targetSize: CGSize.init(width: self.contentView.frame.size.width, height:  self.contentView.frame.size.height), contentMode: .aspectFit, options: nil) { (result: UIImage!, info) in
            self.thumbImage.image = result
            
        }
        
        
    }
    
    
}
