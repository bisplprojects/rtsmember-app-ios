//
//  ImagePickerViewController.swift
//  RTSPortal
//
//  Created by sajan on 07/08/19.
//  Copyright © 2019 Bolas intelli Solutions. All rights reserved.
//

import UIKit
import Photos

protocol ImagePickerViewControllerDelegate : NSObjectProtocol {
    
    func choosenImages(imagesArray:[UIImage])
   
}

class ImagePickerViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    @IBOutlet var imageCollectionView: UICollectionView!
    
    var allPhotos :PHFetchResult<PHAsset>?
    var indexArray :[IndexPath]?
    var imageArray : [UIImage]?
    weak  var delegate : ImagePickerViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
     // let photoLibrary =   PHPhotoLibrary.shared()
        PHPhotoLibrary.requestAuthorization { (status) in
            
            switch status
                        {
                        case .authorized:
                            print("Good to proceed")
                            let fetchOptions = PHFetchOptions()
                            self.allPhotos = PHAsset.fetchAssets(with: .image, options: fetchOptions)
                            print("Found \(String(describing: self.allPhotos?.count)) images")
                            DispatchQueue.main.async {
                                self.imageCollectionView.reloadData()
                            }
                
                        case .denied:
                            print("Not allowed")
                        case .notDetermined:
                            print("Not determined yet")
                        default:
                            print("error in library")
            
                        }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        let availableWidth = (self.imageCollectionView.frame.size.width/3)-20+5//self.view.frame.size.width - 10 - 5
        
        return CGSize(width: availableWidth, height: availableWidth)
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return   (self.allPhotos != nil) ? self.allPhotos!.count : 0;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let imageCell : ImageCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCollectionCell", for: indexPath) as! ImageCollectionViewCell
        let asset = allPhotos?.object(at: indexPath.item)
//        imageCell.backgroundColor = UIColor.red;
        imageCell.showImageFromPHAsset(phAsset: asset!)
        if self.indexArray != nil{
        imageCell.tickImage.image = (self.indexArray?.contains(indexPath))! ?  UIImage.init(named: "tick") : nil
        }
        return imageCell
    }
    @IBAction func actionCancel(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func actionChoose(_ sender: Any) {
        
        if (self.imageArray?.count == 0 || self.imageArray == nil){
            
            let alert = UIAlertController(title: "Image Picker", message: "Please choose image(s)", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            self.delegate?.choosenImages(imagesArray: self.imageArray!)
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.cellForItem(at: indexPath) as! ImageCollectionViewCell
        
        
//        self.indexArray = (self.indexArray == nil) ? [IndexPath]() : self.indexArray
//        self.imageArray = (self.imageArray == nil) ? [UIImage]() : self.imageArray
        
        
//        if (self.indexArray?.contains(indexPath))!{
//            self.indexArray =  self.indexArray?.filter{$0 != indexPath}
//            cell.tickImage.image = nil
//            self.imageArray = self.imageArray?.filter{$0 != cell.thumbImage.image}
//        }
//        else{
//
//            self.indexArray?.append(indexPath)
//            self.imageArray?.append(cell.thumbImage.image!)
//            cell.tickImage.image = UIImage.init(named:"tick")
//        }
        
        self.indexArray = [IndexPath]()
        self.imageArray = [UIImage]()

        self.indexArray?.append(indexPath)
        self.imageArray?.append(cell.thumbImage.image!)
        
        if (self.imageArray?.count == 0 || self.imageArray == nil){
            
            let alert = UIAlertController(title: "Image Picker", message: "Please choose image(s)", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            self.delegate?.choosenImages(imagesArray: self.imageArray!)
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
