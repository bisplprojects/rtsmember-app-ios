//
//  UserDefaults+Extensions.swift
//  RTSPortal
//
//  Created by Sajan on 11/07/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import Foundation

enum UserDefaultsKeys : String {
    case isLoggedIn
    case userID
    case storedUserData
    case registered
    case isTouchIDEnabled

}

extension UserDefaults{
    
    //MARK:- Check Login
    func setLoggedIn(value: Bool) {
        set(value, forKey: UserDefaultsKeys.isLoggedIn.rawValue)
        //synchronize()
    }
    
    func isLoggedIn()-> Bool {
        return bool(forKey: UserDefaultsKeys.isLoggedIn.rawValue)
    }
    
    //MARK:- Check Touch ID
    func setTouchIDEnabled(value: Bool) {
        set(value, forKey: UserDefaultsKeys.isTouchIDEnabled.rawValue)
        //synchronize()
    }
    
    func isTouchIDEnabled()-> Bool {
        return bool(forKey: UserDefaultsKeys.isTouchIDEnabled.rawValue)
    }

    //Check if User Registered
    func setRegisteredStatus(value: Bool){
        set(value, forKey: UserDefaultsKeys.registered.rawValue)
    }
    func hasUserRegistered()-> Bool {
        return bool(forKey:  UserDefaultsKeys.registered.rawValue)
    }
    
    //MARK:- Save BGS User Data
    
    func setBGSUserData(dict:[String:String]){
        
        set(dict, forKey: "SavedBGSLoginData")
    }
    
    func getBGSUserData() -> Any {
        return(object(forKey: "SavedBGSLoginData")) as Any
    }
    
    func removeBGSUserData()  {
        UserDefaults.standard.removeObject(forKey: "SavedBGSLoginData")
        UserDefaults.standard.synchronize()
    }

    //MARK:- Save Device Token
    
    func setDeviceToken(value: String){
        set(value, forKey: "DeviceToken")
    }
    
    func getDeviceToken() -> String? {
        return string(forKey: "DeviceToken")
    }
    
    func removeDeviceToken()  {
        UserDefaults.standard.removeObject(forKey: "DeviceToken")
        UserDefaults.standard.synchronize()
    }

    //MARK:- Save Device UUID
    
    func setDeviceUUID(value: String){
        set(value, forKey: "DeviceUUID")
    }
    
    func getDeviceUUID() -> String? {
        return string(forKey: "DeviceUUID")
    }

    //MARK:- Save FCM Token
    
    func setFCMToken(value: String){
        set(value, forKey: "FCMToken")
    }
    
    func getFCMToken() -> String? {
        return string(forKey: "FCMToken")
    }

    //MARK:- Save User Role
    
    func setUserRoles(value: [String]){
        set(value, forKey: "UserRoles")
    }
    
    func getUserRoles() -> [String] {
        return  (array(forKey: "UserRoles") as! [String])
    }

    func removeUserRoles()  {
        UserDefaults.standard.removeObject(forKey: "UserRoles")
        UserDefaults.standard.synchronize()
    }


    func setUserID(value: Int){
        set(value, forKey: UserDefaultsKeys.userID.rawValue)
        //synchronize()
    }
    
    //MARK:- Retrieve User Data
    func getUserID() -> Int{
        return integer(forKey: UserDefaultsKeys.userID.rawValue)
    }
    
    //MARK:- Test 
}

/*
 
 Save in UserDefaults where you want
 
 UserDefaults.standard.setLoggedIn(value: true)          // Bool
 UserDefaults.standard.setUserID(value: result.User.id!) // integer
 
 Retrieve data anywhere in app
 
 print("ID : \(UserDefaults.standard.getUserID())")
 UserDefaults.standard.getUserID()
 
 Remove Values
 
 UserDefaults.standard.removeObject(forKey: UserDefaultsKeys.userID)

 */
