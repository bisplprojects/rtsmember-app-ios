//
//  DatabaseHelper.swift
//  RTSPortal
//
//  Created by sajan on 03/03/20.
//  Copyright © 2020 BISPL. All rights reserved.
//
import Foundation
import UIKit
import CoreData

class DatabaseHelper {
    
    static let shareInstance = DatabaseHelper()
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
   
    func saveNewsData(newsId:String, displayOrderID:String, newsTitle:String, newsIntroductoryText:String, newsCategory:String, newsCreatedDate:String, newsColor:String, newsImgData:Data) {
        
        //let newsInstance = Entit
//        let entity = NSEntityDescription.entity(forEntityName: "NewsData", in: context)
//        let newsInstance = NSManagedObject(entity: entity!, insertInto: context)

        let newsInstance = NewsData(context:context)
        
        newsInstance.newsId = newsId

//        let tmp = (displayOrderID as NSString).integerValue

        newsInstance.displayOrderID = displayOrderID//Int32(tmp)
        newsInstance.newsTitle = newsTitle
        newsInstance.newsIntroductoryText = newsIntroductoryText
        newsInstance.newsCategory = newsCategory
        newsInstance.newsDate = newsCreatedDate
        newsInstance.newsColor = newsColor
        newsInstance.newsImgData = newsImgData

        do {
        try context.save()
        print("Data is saved")
        } catch {
        print(error.localizedDescription)
        }
    }
    
    func saveNewsDataWithoutImg(newsId:String, displayOrderID:String, newsTitle:String, newsIntroductoryText:String, newsCategory:String, newsCreatedDate:String, newsColor:String) {
        
        let newsInstance = NewsData(context:context)
        
        newsInstance.newsId = newsId
        newsInstance.displayOrderID = displayOrderID//Int32(tmp)
        newsInstance.newsTitle = newsTitle
        newsInstance.newsIntroductoryText = newsIntroductoryText
        newsInstance.newsCategory = newsCategory
        newsInstance.newsDate = newsCreatedDate
        newsInstance.newsColor = newsColor
        newsInstance.newsImgData = nil

        do {
        try context.save()
        print("Data is saved without IMG")
        } catch {
        print(error.localizedDescription)
        }
    }
    
    func updateImageDataForNews(newsID:String, newsImageData:Data)  {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "NewsData")

        fetchRequest.predicate = NSPredicate(format: "newsId = %@",
                                                 argumentArray: [newsID])

        do {
            let results = try context.fetch(fetchRequest) as? [NSManagedObject]
            if results?.count != 0 { // Atleast one was returned

                // In my case, I only updated the first item in results
                results![0].setValue(newsImageData, forKey: "newsImgData")
            }
        } catch {
            print("Fetch Failed: \(error)")
        }

        do {
            try context.save()
            print("update successfull")
           }
        catch {
            print("Saving Core Data Failed: \(error)")
        }

    }
    
    func fetchNewsData() -> [NewsData] {
        
        var newsList = [NewsData]()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "NewsData")
                
//        let sort = NSSortDescriptor(key: "displayOrderID", ascending: true)
//        fetchRequest.sortDescriptors = [sort]

        do {
            newsList = try context.fetch(fetchRequest) as! [NewsData]
        } catch {
            print("Error while fetching the news")
        }
        
        return newsList
    }
    
    func deleteAllNewsRecords() {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let context = delegate.persistentContainer.viewContext

        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "NewsData")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)

        do {
            try context.execute(deleteRequest)
            try context.save()
        } catch {
            print ("There was an error")
        }
    }
    
    func deleteNews(Id:String)
    {

        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName:"NewsData")
        fetchRequest.predicate = NSPredicate(format: "newsId = %@", "\(Id)")
        do
        {
            let fetchedResults =  try context.fetch(fetchRequest) as? [NSManagedObject]

            for entity in fetchedResults! {

                context.delete(entity)
                do
                {
                    try context.save()
                }

                catch let error as Error?
                {
                    print(error!.localizedDescription)
                }
            }
        }
        catch _ {
            print("Could not delete")

        }
    }

}
