//
//  UserActionManager.swift
//  RTSPortal
//
//  Created by sajan on 17/07/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit

class UserActionManager: NSObject {

    static  var revealMenuController : MenuViewController = MenuViewController()

    class  func displayLeftMenu(){
    
        let currentVc : UIViewController = (UIApplication.shared.delegate?.window??.rootViewController as! UINavigationController).topViewController!
        UserActionManager.revealMenuController.displayRevealMenuToVC(currentVC: currentVc)
    
    }

}
