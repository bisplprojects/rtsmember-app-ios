//
//  ApiHandler.swift
//  RTSPortal
//
//  Created by Sajan on 08/07/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ApiHandler: NSObject {
    static func getPostString(params:[String:Any]) -> String
    {
        var data = [String]()
        for(key, value) in params
        {
            data.append(key + "=\(value)")
        }
        return data.map { String($0) }.joined(separator: "&")
    }

    func postData(url: String, parameter: [String: AnyObject], completionHandler: @escaping (_ json: JSON, _ status: Bool) -> ())
    {
        let headers = ["Authorization": ""]

        Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding.default, headers:headers) .validate()
            .responseJSON { response in
                
                if let temp = response.result.value
                {
                    let json = JSON(temp)
                    
                    if json == JSON.null
                    {
                        completionHandler(json, false)
                    }
                    else
                    {
                        completionHandler(json, true)
                    }
                }
                else
                {
                    if let data = response.data {
                        let json = String(data: data, encoding: String.Encoding.utf8)
                        
                        if let data = json?.data(using: .utf8) {
                            if let json = try? JSON(data: data) {
                                
                                completionHandler(json, false)
                            }
                        }
                        completionHandler(JSON.null, false)
                    }
                }
        }
    }
    
    func getData(url: String, completionHandler: @escaping (_ json: JSON, _ status: Bool) -> ())
    {
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default)
            .responseJSON { response in
                
                if let temp = response.result.value
                {
                    let json = JSON(temp)
                    
                    if json == JSON.null
                    {
                        completionHandler(json, false)
                    }
                    else
                    {
                        completionHandler(json, true)
                    }
                }
                else
                {
                    completionHandler(JSON.null, false)
                }
        }
    }

    func getDataWithHeader(url: String, parameter: [String: AnyObject], completionHandler: @escaping (_ json: JSON, _ status: Bool) -> ())
    {
        let headers = ["user_id": "9"]
        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers:headers) .validate()
            .responseJSON { response in
                
                if let temp = response.result.value
                {
                    let json = JSON(temp)
                    
                    if json == JSON.null
                    {
                        completionHandler(json, false)
                    }
                    else
                    {
                        completionHandler(json, true)
                    }
                }
                else
                {
                    if let data = response.data {
                        let json = String(data: data, encoding: String.Encoding.utf8)
                        
                        if let data = json?.data(using: .utf8) {
                            if let json = try? JSON(data: data) {
                                
                                completionHandler(json, false)
                            }
                        }
                        completionHandler(JSON.null, false)
                    }
                }
        }
    }
    
    // Multipart
    func requestWith(endUrl: String, imageData: Data?, parameters: [String : Any], onCompletion: ((JSON?) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        
        let url = endUrl//"http://google.com" /* your API url */
        
        let headers: HTTPHeaders = [
            /* "Authorization": "your_access_token",  in case you need authorization header */
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            if let data = imageData{
                multipartFormData.append(data, withName: "image", fileName: "image.png", mimeType: "image/png")
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print("Succesfully uploaded")
                    if let err = response.error{
                        onError?(err)
                        return
                    }
                    if let temp = response.result.value
                    {
                        let json = JSON(temp)
                        
                        if json == JSON.null
                        {
                            onCompletion?(json)
                        }
                        else
                        {
                           onCompletion?(json)
                        }
                    }
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                onError?(error)
            }
        }
    }

}
