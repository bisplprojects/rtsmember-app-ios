//
//  EventDetailsViewController.swift
//  RTSPortal
//
//  Created by sajan on 25/07/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import SwiftyJSON
import SVProgressHUD
import GoogleMaps

class EventDetailsViewController: UIViewController,GMSMapViewDelegate,EventRegistrationDelegate {
    

    @IBOutlet weak var mapContainerView: GMSMapView!
    @IBOutlet weak var titleContainerView: UIView!
    @IBOutlet weak var eventTitleLbl: UILabel!
    @IBOutlet weak var startDateLbl: UILabel!
    @IBOutlet weak var startTimeLbl: UILabel!
    @IBOutlet weak var endDateLbl: UILabel!
    @IBOutlet weak var endTimeLbl: UILabel!
    @IBOutlet weak var descTitleLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var startDateImgView: UIImageView!
    @IBOutlet weak var startTimeImgView: UIImageView!
    @IBOutlet weak var endDateImgView: UIImageView!
    @IBOutlet weak var endTimeImgView: UIImageView!
    
    @IBOutlet weak var alreadyRegisteredLbl: UILabel!
    @IBOutlet weak var registerBtn: UIButton!
    
    @IBOutlet var scanButton: UIButton!
    @IBOutlet weak var findBtn: UIButton!
    
    @IBAction func scanButtonClicked(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Profile", bundle: Bundle.main)
         let scnVC = storyboard.instantiateViewController(withIdentifier: "QRScanViewController") as! QRScanViewController
        
        self.navigationController?.pushViewController(scnVC, animated: true)
        
    }
    
    @IBAction func actionFindBtnTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "General", bundle: Bundle.main)
         let findVC = storyboard.instantiateViewController(withIdentifier: "StatusUpdateListViewController") as! StatusUpdateListViewController
        findVC.eventID = self.eventId
        self.navigationController?.pushViewController(findVC, animated: true)
    }
    
    var eventId:String?
    var eventDetails : Event?

    var latitude:Double?
    var longitude:Double?
    var latitudeString:String?
    var longitudeString:String?

    var isRegistrationDone:Bool?
    var priceFieldsArray:[Any] = []
    
    var scanBtnFlagString:String?
    var registerBtnFlagString:String?
    var viewRegisterFlagString:String?
    var findBtnFlagString:String?

    @IBOutlet weak var registerBtnConstraintHeight: NSLayoutConstraint!
    @IBOutlet weak var scanBtnConstraintHieght: NSLayoutConstraint!
    @IBOutlet weak var findBtnConstaintHeight: NSLayoutConstraint!
    
    // MARK: -

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.titleContainerView.layer.cornerRadius = 5
        self.titleContainerView.layer.shadowColor = UIColor.gray.cgColor
        self.titleContainerView.layer.shadowOpacity = 0.3
        self.titleContainerView.layer.shadowOffset = .zero //CGSize(width: 1, height: 1)
        self.titleContainerView.layer.shadowRadius = 4
        
        self.mapContainerView.delegate = self
        self.scanButton.isHidden = true
        self.registerBtn.isHidden = true
        
        self.hideUI()
        self.loadEventDetailsApiCallFunc()

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        self.alreadyRegisteredLbl.text = (self.eventDetails?.alreadyRegistered == "1") ? "YOU'VE ALREADY REGISTERED FOR THIS EVENT" : ""
//        self.registerBtn.setTitle((self.eventDetails?.alreadyRegistered == "1") ? "VIEW REGISTRATION" : "REGISTER", for: UIControl.State.normal)
        
        self.alreadyRegisteredLbl.text = (self.viewRegisterFlagString == "1") ? "YOU'VE ALREADY REGISTERED FOR THIS EVENT" : ""
        self.registerBtn.setTitle((self.viewRegisterFlagString == "1") ? "VIEW REGISTRATION" : "REGISTER", for: UIControl.State.normal)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let label = UILabel(frame: CGRect(x: 10, y: 10, width: 100, height: 20))
        label.backgroundColor = UIColor.white
        label.textColor = UIColor(red: 19.0/255.0 , green: 106.0/255.0, blue: 244.0/255.0, alpha: 1.0)
        label.text = "View larger map"
        label.font = UIFont(name: "Arial", size: 12)
        label.textAlignment = .center
        label.layer.cornerRadius = 2
        label.clipsToBounds = true
        label.isUserInteractionEnabled = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.actionViewLargerMapLblTapped))
        label.addGestureRecognizer(tap)
        
        self.mapContainerView.settings.consumesGesturesInView = false
        self.mapContainerView.addSubview(label)
        self.mapContainerView.bringSubviewToFront(label)
        
    }

    // MARK: - EventRegistration Delegate

    func didFinishRegistration(message: String) {
        self.loadEventDetailsApiCallFunc()
    }

    // MARK: - IBActions

    @IBAction func actionBackBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionRegisterBtnTapped(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.priceFieldList = nil
        if (self.eventDetails?.alreadyRegistered != "1" ){
            let storyboard = UIStoryboard(name: "Event", bundle: Bundle.main)
            let eventRegisterVC = storyboard.instantiateViewController(withIdentifier: "EventRegistrationViewController") as! EventRegistrationViewController
            eventRegisterVC.eventId = self.eventDetails?.eventId
            eventRegisterVC.eventRegistrationDelegate = self
            
            eventRegisterVC.eventDetails = self.eventDetails
            
            self.navigationController?.pushViewController(eventRegisterVC, animated: true)

        }
        if self.eventDetails?.alreadyRegistered == "1"{
            
            let storyboard = UIStoryboard(name: "Event", bundle: Bundle.main)
            let vc = storyboard.instantiateViewController(withIdentifier: "MyEventDetailViewController") as! MyEventDetailViewController
            vc.event = self.eventDetails
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func updateUI()  {
        self.eventTitleLbl.text = eventDetails?.eventTitle
        
        if self.eventDetails?.eventStartDate?.trimmingCharacters(in: .whitespaces) != ""{
            let startDate = self.eventDetails?.eventStartDate?.toDate()!
            let startDateString = startDate?.toString()
            self.startDateLbl.text = startDateString
            let startTime = self.getTimeFromDateString(dateString: self.eventDetails!.eventStartDate!)
            self.startTimeLbl.text = startTime
        }

        if self.eventDetails?.eventEndDate?.trimmingCharacters(in: .whitespaces) != ""{
            let endDate = self.eventDetails?.eventEndDate?.toDate()!
            let endDateString = endDate?.toString()
            self.endDateLbl.text = endDateString
            let endTime = self.getTimeFromDateString(dateString: self.eventDetails!.eventEndDate!)
            self.endTimeLbl.text = endTime
        }

        let attributedString = NSMutableAttributedString(string: (self.eventDetails?.eventDescription!.htmlToString)!)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 3
        paragraphStyle.alignment = .justified
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        self.descriptionLbl.attributedText = attributedString

        self.getLatLngForZip(zipCode: self.eventDetails!.postalCode!)
        
        if (self.latitude != nil || self.longitude != nil){
            let position = CLLocationCoordinate2D(latitude: self.latitude!, longitude: self.longitude!)
            let marker = GMSMarker(position: position)
            marker.title = ""
            marker.map = self.mapContainerView
            self.mapContainerView.animate(toZoom: 10)
            self.mapContainerView.animate(toLocation: CLLocationCoordinate2D(latitude: self.latitude!, longitude: self.longitude!))

        }

        self.alreadyRegisteredLbl.text = (self.viewRegisterFlagString == "1") ? "YOU'VE ALREADY REGISTERED FOR THIS EVENT" : ""
        self.registerBtn.setTitle((self.viewRegisterFlagString == "1") ? "VIEW REGISTRATION" : "REGISTER", for: UIControl.State.normal)
        

    }
    
    func hideUI()  {
        self.eventTitleLbl.text = ""
        self.startDateLbl.text = ""
        self.startTimeLbl.text = ""
        self.endDateLbl.text = ""
        self.endTimeLbl.text = ""
        self.descriptionLbl.text = ""
        self.descTitleLbl.text = ""
        self.alreadyRegisteredLbl.text = ""
        self.titleContainerView.isHidden = true
        self.scanButton.isHidden = true
        self.findBtn.isHidden = true
        self.registerBtn.isHidden = true
        self.mapContainerView.isHidden = true
        self.registerBtn.setTitle("", for: UIControl.State.normal)
        
        self.scanBtnConstraintHieght.constant = 5
        self.registerBtnConstraintHeight.constant = 5
        self.findBtnConstaintHeight.constant = 5
        self.view.layoutIfNeeded()


    }
    func showUI()  {
        self.titleContainerView.isHidden = false
        self.descTitleLbl.text = (eventDetails?.eventDescription == "") ? "" : "DESCRIPTION"
        //self.registerBtn.isHidden = false
        self.mapContainerView.isHidden = false
        self.startDateImgView.isHidden = self.eventDetails?.eventStartDate?.trimmingCharacters(in: .whitespaces) != "" ? false : true
        self.startTimeImgView.isHidden = self.eventDetails?.eventStartDate?.trimmingCharacters(in: .whitespaces) != "" ? false : true
        self.endDateImgView.isHidden = self.eventDetails?.eventEndDate?.trimmingCharacters(in: .whitespaces) != "" ? false : true
        self.endTimeImgView.isHidden = self.eventDetails?.eventEndDate?.trimmingCharacters(in: .whitespaces) != "" ? false : true
        
        if (self.scanBtnFlagString == "1") {
            self.scanBtnConstraintHieght.constant = 50
            self.scanButton.isHidden = false
        }
        if (self.registerBtnFlagString == "1") {
            self.registerBtn.isHidden = false
            self.registerBtnConstraintHeight.constant = 50
        }
        if (self.viewRegisterFlagString == "1") {
            self.registerBtn.isHidden = false
            self.registerBtnConstraintHeight.constant = 50
        }
        if (self.findBtnFlagString == "1") {
            self.findBtnConstaintHeight.constant = 50
            self.findBtn.isHidden = false
        }
        self.view.layoutIfNeeded()

    }

    func getTimeFromDateString(dateString:String) -> String {
        let array = dateString.components(separatedBy: .whitespaces)
        let time = array.last
        let timeComponentsArray = time?.components(separatedBy: ":")
        let timeString = timeComponentsArray![0] + ":" + timeComponentsArray![1]
        return timeString
    }

    func getLatLngForZip(zipCode: String) {
        
        let zipString = zipCode.removingWhitespaces()
        
        let baseUrl = kGoogleMapsBaseUrl
        let apikey = kGoogleMapsAPIKey
        
        if zipString != ""{
            let url = NSURL(string: "\(baseUrl)address=\(zipString)&key=\(apikey)")
            let data = NSData(contentsOf: url! as URL)
            if let json = try? JSON(data: data! as Data) {
                
                if let result = json["results"].array {
                    if result.count > 0{
                        if let geometry = result[0]["geometry"].dictionary {
                            if let location = geometry["location"]?.dictionary {
                                let latitude = location["lat"]?.doubleValue
                                let longitude = location["lng"]?.doubleValue
                                self.latitudeString = location["lat"]?.stringValue
                                self.longitudeString = location["lng"]?.stringValue
                                self.latitude = latitude
                                self.longitude = longitude
                                print("\n\(String(describing: latitude)), \(String(describing: longitude))")
                            }
                        }
                        
                    }
                }
            }
        }
        
    }

    @objc func actionViewLargerMapLblTapped() {
        
        if (self.latitudeString == nil || self.longitudeString == nil){
            self.view.makeToast("Address not found", duration: 3.0, position: .center)
        }else{
            let markerLat = self.latitudeString
            let markerLong = self.longitudeString
            
            if (markerLat != "" && markerLong != ""){
                
                if (UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL)) {
                    
                    let urlString = "https://www.google.com/maps/search/?api=1&query=" + markerLat! + "," + markerLong!
                    
                    UIApplication.shared.open(NSURL(string:urlString)! as URL, options: [:], completionHandler: nil)
                }
                else {
                    
                    let alert = UIAlertController(title: "Google Maps not found", message: "Please install Google Maps in your device.", preferredStyle: UIAlertController.Style.alert)
                    
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else{
                self.view.makeToast("Address not found", duration: 3.0, position: .center)
            }
        }
    }

    // MARK: - Webservice
    
    func loadEventDetailsApiCallFunc() {
        
        SVProgressHUD.show()
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        
        // logintype : veda or live
        let token = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "pin": dict["PIN"],
                     "sessionId": dict["sessionId"],
                     "logintype": "live"];
        let tokenString = JSONStringify(value: token as! [String : String])
        let param = ["entity" : "CiviMobileEvent",
                     "action" : "geteventdetails",
                     "token" : tokenString,
                     "event_id" : self.eventId,
                     "cid" :  dict["ContactId"]];
        print("Events Details request params : \(param)")
        
        apiObj.postData(url: kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("Events Details response : \(json)")
            if status
            {
                if json["is_error"] == 0{
                    let dictonary = json["values"]
                    if dictonary["validuser"] == true{
                        
                        let dataDict = dictonary["data"].dictionaryValue
                        let eventDetail = dataDict["event_details"]?.arrayValue
                        
                     let buttonDetail = dataDict["buttons"]?.arrayValue
                        
                        if buttonDetail!.count > 0{
                            let responseButton = buttonDetail?.first
                            let response = buttonDetail?.first
                            
                            self.scanBtnFlagString = responseButton?["scan"].stringValue
                            self.registerBtnFlagString = responseButton?["register"].stringValue
                            self.viewRegisterFlagString = responseButton?["view_registration"].stringValue
                            self.findBtnFlagString = responseButton?["find"].stringValue


//                         let btnValue = responseButton?["scan"].stringValue
//                            let regBtnValue = response?["register"].stringValue
//                            print ("value of register: \(regBtnValue)")
//
//                            if btnValue == "1" {
//                                self.scanButton.isHidden = false}
//                            else {self.scanButton.isHidden = true}
//
//                            if regBtnValue == "1" {
//                                self.registerBtn.isHidden = false
//                            } else{self.registerBtn.isHidden = true}
                            
                        }
                        
                        if eventDetail!.count > 0 {
                            
                            let responseDict = eventDetail?.first
                            self.eventDetails = Event(allEventDetailsDict: (responseDict?.dictionary)!)
                            
                                
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                // TO DO : Display toast msg
                                self.updateUI()
                                self.showUI()
                            }
                        }
                        else {
                            //Display msg
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                self.view.makeToast("Event details not found", duration: 3.0, position: .center)
                            }
                        }
                    }
                    else{
                        //Display msg
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                        }
                    }
                }
                else{
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.view.makeToast("Event details not found", duration: 3.0, position: .center)

                    }
                }
            }
            else{
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
