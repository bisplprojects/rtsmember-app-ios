//
//  EventRegChooseSessionViewController.swift
//  RTSPortal
//
//  Created by sajan on 01/08/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import SVProgressHUD
import Toast_Swift

class EventRegChooseSessionViewController: UIViewController {

    @IBOutlet weak var chooseSessionTableView: UITableView!
    @IBOutlet weak var continueBtn: UIButton!
    
    var currentVC:UIViewController?

    let cellIdentifier = "ChooseSessionCell"

    var eventDetails : Event?
    var priceFieldList:[PriceField] = []
    var selectedOptionsArray:[Option] = []

    var eventRegistrationDetails : EventRegister?

    var isRequiredFieldSelected:Bool = false
    var requiredPriceCount: Int = 0
    
    var selectedPriceOptionsStringArray:[String] = []
    var totalAmount:Double = 0.0

    var isTotalAmountZero:Bool = true
    var participantId : String?

    // MARK: -

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.currentVC = self

        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if appDelegate.priceFieldList != nil{
            self.priceFieldList = appDelegate.priceFieldList!
        }
        else{
            self.loadPriceFieldsApiCallFunc()
        }

        self.chooseSessionTableView.sectionHeaderHeight = UITableView.automaticDimension
        self.chooseSessionTableView.estimatedSectionHeaderHeight = 40
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let totalAmount = self.getTotalAmountForSelectedItems()
        if (totalAmount <= 0.0){
            self.isTotalAmountZero = true
        }
        else if(totalAmount > 1.0){
            self.isTotalAmountZero = false
        }
        self.updateContinueBtnTitle(self.isTotalAmountZero)
    }
    
    func isAnyPriceOptionSelected() -> Bool {
        
        for price in self.priceFieldList{
            for opt in price.options{
                if(opt.isOptionSelected!){
                    return true
                }
            }
        }
        return false
    }
    
    // MARK: - IBActions

    @IBAction func actionBackBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionContinueBtnTapped(_ sender: Any) {
        
        var messageString = ""
        
        for price in self.priceFieldList{
            for opt in price.options{
                if opt.isOptionSelected == true{
                    if !selectedOptionsArray.contains(opt){
                        self.selectedOptionsArray.append(opt)
                    }
                }
                else{
                    if selectedOptionsArray.contains(opt){
                        for selOpt in selectedOptionsArray{
                            if selOpt == opt{
                                selectedOptionsArray.removeAll { $0.optionId == selOpt.optionId }
                            }
                        }
                    }
                }
            }
        }
        
        self.requiredPriceCount = 0

        for priceField in self.priceFieldList {
            if priceField.isRequired == "1"{
                
                self.requiredPriceCount += 1
                
                messageString = priceField.priceFieldLabel!
                for opt in priceField.options{
                    if self.selectedOptionsArray.contains(opt){
                        self.isRequiredFieldSelected = true
                    }
                }
            }
        }
        
        if self.requiredPriceCount >= 1{
            if self.isRequiredFieldSelected == false{
                self.view.makeToast("Please select one from \(messageString)", duration: 3.0, position: .center)
            }
            else{
                self.selectedPriceOptionsStringArray.removeAll()
                self.totalAmount = 0.0
                for opt in self.selectedOptionsArray{
                    self.selectedPriceOptionsStringArray.append(opt.optionId!)
                    let amount:Double = (Double(opt.displayAmount!))!
                    self.totalAmount += amount
                }
                
                if (self.totalAmount > 0.0){
                    let storyboard = UIStoryboard(name: "Event", bundle: Bundle.main)
                    let paymentModeVC = storyboard.instantiateViewController(withIdentifier: "PaymentModeViewController") as! PaymentModeViewController
                    var eventDetail:Event = Event()
                    eventDetail = self.eventDetails!
                    paymentModeVC.eventDetails = eventDetail

                    paymentModeVC.eventRegistrationDetails = self.eventRegistrationDetails
                    paymentModeVC.totalAmount = self.totalAmount
                    paymentModeVC.selectedPriceOptionsStringArray = self.selectedPriceOptionsStringArray
                    self.navigationController?.pushViewController(paymentModeVC, animated: true)
                }
                else{
                    //Register Event
                    self.eventRegistrationApiCallFunc()
                }
            }
        }
        else{
            self.selectedPriceOptionsStringArray.removeAll()
            self.totalAmount = 0.0
            for opt in self.selectedOptionsArray{
                self.selectedPriceOptionsStringArray.append(opt.optionId!)
                let amount:Double = (Double(opt.displayAmount!))!
                self.totalAmount += amount
            }
            
            if (self.totalAmount > 0.0){
                let storyboard = UIStoryboard(name: "Event", bundle: Bundle.main)
                let paymentModeVC = storyboard.instantiateViewController(withIdentifier: "PaymentModeViewController") as! PaymentModeViewController
                var eventDetail:Event = Event()
                eventDetail = self.eventDetails!
                paymentModeVC.eventDetails = eventDetail
                paymentModeVC.eventRegistrationDetails = self.eventRegistrationDetails
                paymentModeVC.totalAmount = self.totalAmount
                paymentModeVC.selectedPriceOptionsStringArray = self.selectedPriceOptionsStringArray
                self.navigationController?.pushViewController(paymentModeVC, animated: true)

            }
            else{
                //Register Event
                self.eventRegistrationApiCallFunc()
            }
        }
        
    }
    
    func getTotalAmountForSelectedItems() -> Double {
        var totalAmt:Double = 0.0
        for option in self.priceFieldList {
            for opt in option.options{
                if opt.isOptionSelected == true{
                    totalAmt += (Double(opt.displayAmount!))!
                }
            }
        }
        return totalAmt
    }
    
    func updateContinueBtnTitle(_ isTotalAmtZero:Bool)  {
        let isAnyOptionSelected = self.isAnyPriceOptionSelected()
        self.continueBtn.setTitle((isAnyOptionSelected == true && isTotalAmtZero == true) ? "SUBMIT" : "CONTINUE >>", for: .normal)

    }
    
    func getSymbol(forCurrencyCode code: String) -> String? {
        let locale = NSLocale(localeIdentifier: code)
        if locale.displayName(forKey: .currencySymbol, value: code) == code {
            let newlocale = NSLocale(localeIdentifier: code.dropLast() + "_en")
            return newlocale.displayName(forKey: .currencySymbol, value: code)
        }
        return locale.displayName(forKey: .currencySymbol, value: code)
    }

    // MARK: - Webservice
    
    func loadPriceFieldsApiCallFunc() {
        
        SVProgressHUD.show()
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        
        // logintype : veda or live
        let token = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "pin": dict["PIN"],
                     "sessionId": dict["sessionId"],
                     "logintype": "live"];
        let tokenString = JSONStringify(value: token as! [String : String])
        let param = ["entity" : "CiviMobileEvent",
                     "action" : "pricefields",
                     "token" : tokenString,
                     "cid" :  dict["ContactId"],
                     "event_id" :  self.eventDetails?.eventId,
                     "profession_id" :  self.eventRegistrationDetails?.profession];
        print("Price Fields  params : \(param)")
        
        apiObj.postData(url: kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("Price Fields response : \(json)")
            let style = ToastStyle()

            if status
            {
                if json["is_error"] == 0{
                    
                    let dictonary = json["values"]
                    if dictonary["validuser"] == true{
                        
                        let dataDict = dictonary["data"].dictionaryValue
                        let message = dataDict["message"]?.stringValue
                        
                        if dataDict["status"] == true{
                            
                            if dataDict["price_fields"]!.arrayValue.count > 0{
                                for price in (dataDict["price_fields"]?.array)! {
                                    let priceField = PriceField(dict: price.dictionaryValue)
                                    self.priceFieldList.append(priceField)
                                }
                            }
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.priceFieldList = self.priceFieldList
                            
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                //TODO : Add data to modal class
                                self.chooseSessionTableView.reloadData()
                                if self.priceFieldList.count == 0 {
                                    self.chooseSessionTableView.setEmptyMessage("Nothing found")
                                } else {
                                    self.chooseSessionTableView.restore()
                                }
                            }
                        }
                        else{
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                self.view.makeToast(message, duration: 3.0, position: .center)
                                if self.priceFieldList.count == 0 {
                                    self.chooseSessionTableView.setEmptyMessage("Nothing found")
                                } else {
                                    self.chooseSessionTableView.restore()
                                }
                            }
                        }
                    }
                    else{
                        let message = dictonary["data"]["message"].stringValue
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                            self.view.makeToast(message, duration: 3.0, position: .center)
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                }
                else{
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.view?.makeToast("Could not load data", duration: 2.0, position: ToastPosition.center, title:"", image:nil, style: style, completion: { (true) in
                            self.navigationController?.popViewController(animated: true)
                        })
                    }
                }
            }
            else{
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                    self.view?.makeToast("Could not load data", duration: 2.0, position: ToastPosition.center, title:"", image:nil, style: style, completion: { (true) in
                        self.navigationController?.popViewController(animated: true)
                    })
                }
            }
        }
    }

    func eventRegistrationApiCallFunc() {
        
        SVProgressHUD.show()
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        
        // logintype : veda or live
        let token = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "pin": dict["PIN"],
                     "sessionId": dict["sessionId"],
                     "logintype": "live"];
        let tokenString = JSONStringify(value: token as! [String : String])
        
        let fields = ["email-Primary": self.eventRegistrationDetails?.primaryEmail,
                      "email-5": self.eventRegistrationDetails?.billingEmail,
                      "formal_title": self.eventRegistrationDetails?.title,
                      "first_name": self.eventRegistrationDetails?.firstName,
                      "last_name": self.eventRegistrationDetails?.lastName,
                      "job_title": self.eventRegistrationDetails?.jobTitle,
                      "custom_213": self.eventRegistrationDetails?.placeOfWork,
                      "gender_id": self.eventRegistrationDetails?.gender,
                      "custom_107": self.eventRegistrationDetails?.profession,
                      "phone-Primary-2": self.eventRegistrationDetails?.mobile,
                      "url-11": self.eventRegistrationDetails?.twitterName,
                      "custom_324": self.eventRegistrationDetails?.dietaryRequirement,
                      "custom_404": self.eventRegistrationDetails?.otherDietaryRequirement,
                      "custom_326": self.eventRegistrationDetails?.accessRequirement,
                      "custom_485": self.eventRegistrationDetails?.familyFriendlyRoomAccess,
                      "custom_354": self.eventRegistrationDetails?.volunteerChairSession,
                      "custom_403": self.eventRegistrationDetails?.joinDelegate,
                      "custom_221": self.eventRegistrationDetails?.biography,
                      "image_URL": self.eventRegistrationDetails?.picture];
        
        let fieldsString = JSONStringify(value: (fields as? [String : String])!)
        let priceOptionString = convertIntoJSONString(arrayObject: self.selectedPriceOptionsStringArray)

        let param = ["entity" : "CiviMobileParticipant",
                     "action" : "registration",
                     "token" : tokenString,
                     "event_id" : self.eventRegistrationDetails?.eventId as Any,
                     "cid" :  dict["ContactId"]!,
                     "price_options" : priceOptionString as Any,
                     "is_paylater" : 0,
                     "payment_successful" : 1,
                     "trxn_id" : "",
                     "total_amount" : "0",
                     "debug" : "1",
                     "fields" : fieldsString] as [String : Any];
        
        print("Events Registration request params : \(param)")
        
        apiObj.postData(url: kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("Events Registration response : \(json)")
            if status
            {
                if json["is_error"] == 0{
                    let dictonary = json["values"]
                    let dataDictonary = dictonary["data"]
                    
                    let msg = dataDictonary["message"].stringValue
                    if dataDictonary["status"] == true{
                        
                        let dataDict = dataDictonary["event_registration"].dictionaryValue

                        self.participantId = dataDict["pid"]?.stringValue
                        
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                            //  Display toast msg
                            let style = ToastStyle()
                            self.view?.makeToast(msg, duration: 2.0, position: ToastPosition.center, title:"", image:nil, style: style, completion: { (true) in
                                // Move to Event Details
                                self.moveToEventDetailsVC(self.eventDetails!.eventId!)
                            })
                        }
                    }
                    else{
                        //Display msg
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                            self.view.makeToast(msg, duration: 3.0, position: .center)
                        }
                    }
                }
                else{
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.view.makeToast("Event registraion failed", duration: 3.0, position: .center)
                    }
                }
            }
            else{
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
        }
    }
    
    func moveToParticipantDetailsVC(_ participantId:String) {
        var viewControllers = ( self.currentVC?.navigationController?.viewControllers)!
        let storyboard = UIStoryboard(name: "Event", bundle: Bundle.main)
        let myEventDetailVC = storyboard.instantiateViewController(withIdentifier: "MyEventDetailViewController") as! MyEventDetailViewController
        let event:Event = Event()
        event.participantId = participantId
        myEventDetailVC.event = event
    self.currentVC?.navigationController?.setViewControllers([viewControllers[0],viewControllers[1],myEventDetailVC], animated: true)
        
    }

    func moveToEventDetailsVC(_ evendId:String) {
        var viewControllers = ( self.currentVC?.navigationController?.viewControllers)!
        
        let storyboard = UIStoryboard(name: "Event", bundle: Bundle.main)
        let eventDetailsVC = storyboard.instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
        eventDetailsVC.eventId = evendId
        self.currentVC?.navigationController?.setViewControllers([viewControllers[0],viewControllers[1],eventDetailsVC], animated: true)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension EventRegChooseSessionViewController: UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.priceFieldList[section].options.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let chooseSessionCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! ChooseSessionCell
        
        let option:Option = self.priceFieldList[indexPath.section].options[indexPath.row]
        
        chooseSessionCell.titleLbl.text = option.label
        
        let currencySymbol = getSymbol(forCurrencyCode: "GBP")
        
        let temp:Double = (Double(option.taxAmount!))!
        let taxAmount = String(format: "%.2f", temp)
        chooseSessionCell.priceLbl.text = currencySymbol! + option.displayAmount! + " (includes VAT of " + currencySymbol! + taxAmount + ")"
    
        if(option.optionId != nil) {
            chooseSessionCell.selectButton.isSelected = true
        }
        else {
            chooseSessionCell.selectButton.isSelected = false
        }
        
        chooseSessionCell.radioImgView.image = option.isOptionSelected! ?  UIImage.init(named: "radio_on_blue_icon") : UIImage.init(named: "radio_off_icon")
        
        return chooseSessionCell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let option:Option = self.priceFieldList[indexPath.section].options[indexPath.row]

        let cell = tableView.cellForRow(at: indexPath) as! ChooseSessionCell
        
        self.isRequiredFieldSelected = false

        for opt in self.priceFieldList[indexPath.section].options {
            if opt == option{
                opt.isOptionSelected = opt.isOptionSelected! ? false : true
            }
            else{
                opt.isOptionSelected = false
            }
        }
        
        cell.radioImgView.image = option.isOptionSelected! ?  UIImage.init(named: "radio_on_blue_icon") : UIImage.init(named: "radio_off_icon")

        self.chooseSessionTableView.reloadSections([indexPath.section], with: .fade)
        

        let totalAmount = self.getTotalAmountForSelectedItems()
        if (totalAmount <= 0.0){
            self.isTotalAmountZero = true
        }
        else if(totalAmount > 1.0){
            self.isTotalAmountZero = false
        }
        self.updateContinueBtnTitle(self.isTotalAmountZero)
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return self.priceFieldList.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 40))
        view.backgroundColor = UIColor.white
        
        let label = UILabel(frame: CGRect(x: 10, y: 0, width: tableView.frame.size.width - 20, height: 40))
        label.numberOfLines = 0
        label.font = UIFont(name: "Arial", size: 15)
        label.textColor = UIColor(red: 19.0/255.0 , green: 106.0/255.0, blue: 244.0/255.0, alpha: 1.0)
        
        let priceField:PriceField = self.priceFieldList[section]
        label.text = priceField.priceFieldLabel

        view.addSubview(label)
        
        return view

    }

}
