//
//  EventRegistrationStep2ViewController.swift
//  RTSPortal
//
//  Created by sajan on 06/08/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import SVProgressHUD
import Toast_Swift

class EventRegistrationStep2ViewController: UIViewController,UIImagePickerControllerDelegate,MTImagePickerDelegate,UINavigationControllerDelegate,UITextFieldDelegate {
   
    var currentVC:UIViewController?

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var regStepTwoTableView: UITableView!
    
    @IBOutlet weak var continueBtn: UIButton!
    
    let CellIdentifier_Text = "RegistrationTextCell"
    let CellIdentifier_Radio = "RegRadioBtnCell"
    let CellIdentifier_Option = "RegOptionCell"
    let CellIdentifier_DropDown = "RegDropDownCell"
    let CellIdentifier_UploadImg = "RegUploadImgCell"

    // Defining the tableview field tags
    let kTwitterNameTag         = 300
    let kOthersTag              = 301
    let kAccessRequirementTag   = 302
    let kBiographyTag           = 303

    let EMPTY_STRING = ""

    var registrationTitleList:[String] = []

    var eventDetails : Event?
    var eventRegistrationDetails : EventRegister?

    var regImage : UIImage?
    var imageString:String = ""
    var isImageSelected:Bool = false
    
    var formFieldList:[FormField]?

    var dietaryRequirementArray:[SelectOption]?
    var dietaryRequirementCount:Int?
    var selectedDietaryRequirement:String = ""
    var isOtherDietaryRequirementSelected:Bool = false
    
    var participantId : String?

    // MARK: -

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.currentVC = self

        self.containerView.layer.cornerRadius = 0
        self.containerView.layer.shadowColor = UIColor.gray.cgColor
        self.containerView.layer.shadowOpacity = 0.3
        self.containerView.layer.shadowOffset = .zero //CGSize(width: 1, height: 1)
        self.containerView.layer.shadowRadius = 4

        self.registrationTitleList = ["Other - Dietary Requirements","Access Requirements","Family friendly room access requested","Volunteer - Chair Session","Join Delegate list*","Biography","Picture"]
        
        if let tmp = self.eventDetails?.formFields{
            self.formFieldList = tmp
        }

        continueBtn.setTitle((self.eventDetails?.isMonetary == "1") ? "CONTINUE >>" : "SUBMIT", for: .normal)
        
        self.getImageFromModal()
        self.checkOtherRequirementSelected()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }
    
    func getImageFromModal() {
        if (self.eventRegistrationDetails?.picture != ""){
            self.regImage = self.ConvertBase64StringToImage(imageBase64String: self.eventRegistrationDetails!.picture!)
            self.isImageSelected = (self.regImage != nil) ? true : false
        }
    }
    
    func checkOtherRequirementSelected() {
        if (self.eventRegistrationDetails?.dietaryRequirement == "Other"){
            self.isOtherDietaryRequirementSelected = true
        }else{
            self.isOtherDietaryRequirementSelected = false
        }
    }
    
    func saveDataToEventRegisterModal()  {
        guard self.formFieldList!.count > 1 else {
            return
        }
        for item in self.formFieldList! {
            let name = item.name
            switch name {
            case "url-11":
                self.eventRegistrationDetails?.twitterName = (item.defaultValue?.first != nil) ? (item.defaultValue?.first as! String) : ""
                break
            case "custom_324":
                self.eventRegistrationDetails?.dietaryRequirement = (item.defaultValue?.first != nil) ? (item.defaultValue?.first as! String) : ""
                break
            case "custom_404":
                self.eventRegistrationDetails?.otherDietaryRequirement = (item.defaultValue?.first != nil) ? (item.defaultValue?.first as! String) : ""
                break
            case "custom_326":
                self.eventRegistrationDetails?.accessRequirement = (item.defaultValue?.first != nil) ? (item.defaultValue?.first as! String) : ""
                break
            case "custom_485":
                self.eventRegistrationDetails?.familyFriendlyRoomAccess = (item.defaultValue?.first != nil) ? (item.defaultValue?.first as! String) : ""
                break
            case "custom_354":
                self.eventRegistrationDetails?.volunteerChairSession = (item.defaultValue?.first != nil) ? (item.defaultValue?.first as! String) : ""
                break
            case "custom_403":
                self.eventRegistrationDetails?.joinDelegate = (item.defaultValue?.first != nil) ? (item.defaultValue?.first as! String) : ""
                break
            case "custom_221":
                self.eventRegistrationDetails?.biography = (item.defaultValue?.first != nil) ? (item.defaultValue?.first as! String) : ""
                break
            case "image_URL":
                self.eventRegistrationDetails?.picture = (item.defaultValue?.first != nil) ? (item.defaultValue?.first as! String) : ""
                if (self.eventRegistrationDetails?.picture != ""){
                    self.regImage = self.ConvertBase64StringToImage(imageBase64String: self.eventRegistrationDetails!.picture!)
                    self.isImageSelected = (self.regImage != nil) ? true : false
                }
                break
                
            default:
                break
            }
            
        }
    }
    
    func saveTextfieldString(textField:UITextField)  {
        
        let strText = textField.text
        switch textField.tag {
        case kTwitterNameTag:
            self.eventRegistrationDetails?.twitterName = strText
            break
        case kOthersTag:
            self.eventRegistrationDetails?.otherDietaryRequirement = strText
            break
        case kAccessRequirementTag:
            self.eventRegistrationDetails?.accessRequirement = strText
            break
        case kBiographyTag:
            self.eventRegistrationDetails?.biography = strText
            break
            
        default:
            break
        }
    }

    // MARK: - UITextField delegates
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.saveTextfieldString(textField: textField)
    }

    // MARK: - IBActions

    @IBAction func actionNavigtionBackBtnTapped(_ sender: Any) {
        self.view.endEditing(true)
        for opt in self.dietaryRequirementArray!{
            if opt.isOptionSelected == true{
                self.selectedDietaryRequirement = opt.optionId!
            }
        }
        self.eventRegistrationDetails?.dietaryRequirement = self.selectedDietaryRequirement

        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionBackBtnTapped(_ sender: Any) {
        self.view.endEditing(true)
        for opt in self.dietaryRequirementArray!{
            if opt.isOptionSelected == true{
                self.selectedDietaryRequirement = opt.optionId!
            }
        }
        self.eventRegistrationDetails?.dietaryRequirement = self.selectedDietaryRequirement

        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionContinueBtnTapped(_ sender: Any) {
        
        self.view.endEditing(true)
        
        for opt in self.dietaryRequirementArray!{
            if opt.isOptionSelected == true{
                self.selectedDietaryRequirement = opt.optionId!
            }
        }
        self.eventRegistrationDetails?.dietaryRequirement = self.selectedDietaryRequirement
        
        var isValid:Bool?
        
        for formField in self.formFieldList!{
            
            if formField.isRequired == "1"{
                
                var validation:Valid = .success
                
                switch formField.name {
                    
                case "url-11":
                    validation = Validation.shared.validate(values: (ValidationType.RegistrationTwitterName, self.eventRegistrationDetails!.twitterName!))
                    break
                case "custom_324":
                    validation = Validation.shared.validate(values: (ValidationType.RegistrationDietaryRequirements, self.eventRegistrationDetails!.dietaryRequirement!))
                    break
                case "custom_404":
                    validation = Validation.shared.validate(values: (ValidationType.RegistrationOtherDietaryRequirements, self.eventRegistrationDetails!.otherDietaryRequirement!))
                    break
                case "custom_326":
                    validation = Validation.shared.validate(values: (ValidationType.RegistrationAccessRequirements, self.eventRegistrationDetails!.accessRequirement!))
                    break
                case "custom_485":
                    validation = Validation.shared.validate(values: (ValidationType.RegistrationFamilyFriendlyRoomAccessRequirements, self.eventRegistrationDetails!.familyFriendlyRoomAccess!))
                    break
                case "custom_354":
                    validation = Validation.shared.validate(values: (ValidationType.RegistrationVolunteer, self.eventRegistrationDetails!.volunteerChairSession!))
                    break
                case "custom_403":
                    validation = Validation.shared.validate(values: (ValidationType.RegistrationJoinDelegate, self.eventRegistrationDetails!.joinDelegate!))
                    break
                case "custom_221":
                    validation = Validation.shared.validate(values: (ValidationType.RegistrationBiography, self.eventRegistrationDetails!.biography!))
                    break
                    
                case "image_URL":
                    validation = Validation.shared.validate(values: (ValidationType.RegistrationPicture, self.eventRegistrationDetails!.picture!))
                    break
                    
                default:
                    break
                }
                
                switch validation {
                    
                case .success:
                    print("Validation Success")
                    
                    if self.isOtherDietaryRequirementSelected == true{
                    if((self.eventRegistrationDetails?.otherDietaryRequirement!.trimmingCharacters(in: .whitespaces).isEmpty)!){
                        
                            self.view.makeToast("Enter Other - Dietary Requirements", duration: 3.0, position: .center)
                            isValid = false
                            return
                        }
                        else{
                            isValid = true
                        }
                    }
                    else{
                        isValid = true
                    }
                    break
                    
                case .failure(_, let message):
                    isValid = false
                    print(message.localized())
                    self.view.makeToast(message.localized(), duration: 3.0, position: .center)
                    return
                }
            }
        }
        
        if isValid == true {
            print("Final Validation Success")
            
            if(self.eventDetails?.isMonetary == "1"){
                let storyboard = UIStoryboard(name: "Event", bundle: Bundle.main)
                let chooseSessionVC = storyboard.instantiateViewController(withIdentifier: "EventRegChooseSessionViewController") as! EventRegChooseSessionViewController
                var eventDetail:Event = Event()
                eventDetail = self.eventDetails!
                chooseSessionVC.eventDetails = eventDetail
                chooseSessionVC.eventRegistrationDetails = self.eventRegistrationDetails
                self.navigationController?.pushViewController(chooseSessionVC, animated: true)
            }
            else{
                //Register Event
                self.eventRegistrationApiCallFunc()
            }
        }
        else{
            print(" Validation failed")
        }

    }
    
    @objc func actionChooseImageBtnTapped(_ sender: UIButton) {
        
        self.showImagePickerWithSourcetype(sourceType:.photoLibrary)
    }

    @objc func actionYesBtnTapped(_ sender: UIButton) {
        
        switch sender.tag {
        case 2:
            self.eventRegistrationDetails?.familyFriendlyRoomAccess = "1"
            break
        case 3:
            self.eventRegistrationDetails?.volunteerChairSession = "1"
            break
        case 4:
            self.eventRegistrationDetails?.joinDelegate = "1"
            break

        default:
            break
        }
        let indexPath = IndexPath(item: sender.tag, section: 2)
        self.regStepTwoTableView.reloadRows(at: [indexPath], with: .none)
        
    }
    
    @objc func actionNoBtnTapped(_ sender: UIButton) {
        
        switch sender.tag {
        case 2:
            self.eventRegistrationDetails?.familyFriendlyRoomAccess = "0"
            break
        case 3:
            self.eventRegistrationDetails?.volunteerChairSession = "2"
            break
        case 4:
            self.eventRegistrationDetails?.joinDelegate = "2"
            break

        default:
            break
        }
        let indexPath = IndexPath(item: sender.tag, section: 2)
        self.regStepTwoTableView.reloadRows(at: [indexPath], with: .none)
        
    }

    // MARK: - TapGesture Methods
    
    @objc func imageTapped(_ sender: UITapGestureRecognizer) {
        if self.regImage != nil {
            let newImageView = UIImageView(image: self.regImage)
            newImageView.contentMode = .scaleAspectFit
            newImageView.frame = UIScreen.main.bounds
            newImageView.backgroundColor = .black
            newImageView.isUserInteractionEnabled = true
            let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
            newImageView.addGestureRecognizer(tap)
            self.view.addSubview(newImageView)
        }
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        sender.view?.removeFromSuperview()
    }

    // MARK: - Webservice
    
    func eventRegistrationApiCallFunc() {
        
        SVProgressHUD.show()
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        // logintype : veda or live
        let token = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "pin": dict["PIN"],
                     "sessionId": dict["sessionId"],
                     "logintype": "live"];
        let tokenString = JSONStringify(value: token as! [String : String])
        
        let fields = ["email-Primary": self.eventRegistrationDetails?.primaryEmail,
                      "email-5": self.eventRegistrationDetails?.billingEmail,
                      "formal_title": self.eventRegistrationDetails?.title,
                      "first_name": self.eventRegistrationDetails?.firstName,
                      "last_name": self.eventRegistrationDetails?.lastName,
                      "job_title": self.eventRegistrationDetails?.jobTitle,
                      "custom_213": self.eventRegistrationDetails?.placeOfWork,
                      "gender_id": self.eventRegistrationDetails?.gender,
                      "custom_107": self.eventRegistrationDetails?.profession,
                      "phone-Primary-2": self.eventRegistrationDetails?.mobile,
                      "url-11": self.eventRegistrationDetails?.twitterName,
                      "custom_324": self.eventRegistrationDetails?.dietaryRequirement,
                      "custom_404": self.eventRegistrationDetails?.otherDietaryRequirement,
                      "custom_326": self.eventRegistrationDetails?.accessRequirement,
                      "custom_485": self.eventRegistrationDetails?.familyFriendlyRoomAccess,
                      "custom_354": self.eventRegistrationDetails?.volunteerChairSession,
                      "custom_403": self.eventRegistrationDetails?.joinDelegate,
                      "custom_221": self.eventRegistrationDetails?.biography,
                      "image_URL": self.eventRegistrationDetails?.picture];
        
        let fieldsString = JSONStringify(value: (fields as? [String : String])!)
        
        let param = ["entity" : "CiviMobileParticipant",
                     "action" : "registration",
                     "token" : tokenString,
                     "event_id" : self.eventRegistrationDetails?.eventId as Any,
                     "cid" :  dict["ContactId"]!,
                     "price_options" : 0,
                     "is_paylater" : 0,
                     "payment_successful" : 1,
                     "trxn_id" : "",
                     "total_amount" : "0",
                     "debug" : "1",
                     "fields" : fieldsString] as [String : Any];
        
        print("Events Registration request params : \(param)")
        
        apiObj.postData(url: kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("Events Registration response : \(json)")
            if status
            {

                if json["is_error"] == 0{
                    let dictonary = json["values"]
                    let dataDictonary = dictonary["data"]

                    let msg = dataDictonary["message"].stringValue
                    if dataDictonary["status"] == true{
                        
                        let dataDict = dataDictonary["event_registration"].dictionaryValue
                        
                        self.participantId = dataDict["pid"]?.stringValue
                        
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                            //  Display toast msg
                            let style = ToastStyle()
                            self.view?.makeToast(msg, duration: 2.0, position: ToastPosition.center, title:"", image:nil, style: style, completion: { (true) in
                                // Move to Event Details
                                self.moveToEventDetailsVC(self.eventDetails!.eventId!)
                            })
                        }
                    }
                    else{
                        //Display msg
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                            self.view.makeToast(msg, duration: 3.0, position: .center)
                        }
                    }
                }
                else{
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.view.makeToast("Event registraion failed", duration: 3.0, position: .center)
                    }
                }
            }
            else{
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
        }
    }
    
    func moveToParticipantDetailsVC(_ participantId:String) {
        var viewControllers = ( self.currentVC?.navigationController?.viewControllers)!
        let storyboard = UIStoryboard(name: "Event", bundle: Bundle.main)
        let myEventDetailVC = storyboard.instantiateViewController(withIdentifier: "MyEventDetailViewController") as! MyEventDetailViewController
        let event:Event = Event()
        event.participantId = participantId
        myEventDetailVC.event = event
        self.currentVC?.navigationController?.setViewControllers([viewControllers[0],viewControllers[1],myEventDetailVC], animated: true)
        
    }

    func moveToEventDetailsVC(_ evendId:String) {
        var viewControllers = ( self.currentVC?.navigationController?.viewControllers)!
        
        let storyboard = UIStoryboard(name: "Event", bundle: Bundle.main)
        let eventDetailsVC = storyboard.instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
        eventDetailsVC.eventId = evendId
        self.currentVC?.navigationController?.setViewControllers([viewControllers[0],viewControllers[1],eventDetailsVC], animated: true)
        
    }

    // MARK: -
    
    func showImagePickerWithSourcetype(sourceType:UIImagePickerController.SourceType)  {
        
        if sourceType == .photoLibrary {
            
            let imagePicker = UIImagePickerController.init()
            imagePicker.sourceType = sourceType
            imagePicker.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)

        }
        else{
            
            let imagePicker = UIImagePickerController.init()
            imagePicker.sourceType = sourceType
            imagePicker.cameraDevice = .front
            imagePicker.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let tempImage:UIImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        self.regImage = tempImage
        
        if tempImage.imageAsset != nil {
            self.isImageSelected = true
        }
        let indexPath = IndexPath(item: 6, section: 2)
        self.regStepTwoTableView.reloadRows(at: [indexPath], with: .none)

        self.imageString = ConvertImageToBase64String(img: tempImage)
        
        self.eventRegistrationDetails?.picture = self.imageString
        
        self.dismiss(animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        
        print("cancelled")
        self.dismiss(animated: true, completion: nil)
    }

//    func ConvertImageToBase64String (img: UIImage) -> String {
//        return img.jpegData(compressionQuality: 0.2)?.base64EncodedString() ?? ""
//    }
//    func ConvertBase64StringToImage (imageBase64String:String) -> UIImage {
//        let imageData = Data.init(base64Encoded: imageBase64String, options:NSData.Base64DecodingOptions())
//        let image = UIImage(data: imageData!)
//        return image!
//    }

    func ConvertImageToBase64String (img: UIImage) -> String {
        let imageData:NSData = img.jpegData(compressionQuality: 0.2)! as NSData
        let imgString = imageData.base64EncodedString(options: .init(rawValue: 0))
        return imgString
    }
    
    func ConvertBase64StringToImage (imageBase64String:String) -> UIImage {
        let imageData = Data.init(base64Encoded: imageBase64String, options: .init(rawValue: 0))
        if imageData != nil{
            let image = UIImage(data: imageData!)
            return image!
        }else{
            return UIImage()
        }
    }

    // MARK: - MTImagePickerDelegate
    func selectedImages(imagesArray: [UIImage]) {
        self.regImage = imagesArray.first
        let indexPath = IndexPath(item: 6, section: 2)
        self.regStepTwoTableView.reloadRows(at: [indexPath], with: .none)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension EventRegistrationStep2ViewController: UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return (section == 1) ? 40 : 0
    }
    func tableView(_ tableView: UITableView, estimatedHeightForFooterInSection section: Int) -> CGFloat {
        return 0 //(section == 1) ? 5 : 0

    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 1{
            let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 40))
            view.backgroundColor = UIColor.white
            
            let label = UILabel(frame: CGRect(x: 10, y: 0, width: tableView.frame.size.width - 20, height: 40))
            label.numberOfLines = 0
            label.font = UIFont(name: "Arial", size: 14)
            label.textColor = UIColor.black//UIColor(red: 19.0/255.0 , green: 106.0/255.0, blue: 244.0/255.0, alpha: 1.0)
            
            for item in self.formFieldList!{
                if item.name == "custom_324"{
                    label.text = item.title! + "*"
                }
            }
            
            view.addSubview(label)
            
            return view

        }
        else{
            return nil
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }
        else if section == 1{
            return self.dietaryRequirementCount!
        }
        else{
            return 7
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.section == 2 && indexPath.row == 0) {
            return (isOtherDietaryRequirementSelected == true) ? UITableView.automaticDimension : 0
        }
        else{
            return UITableView.automaticDimension
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let regTextCell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier_Text) as! RegTextCell
            
            self.configureRegistrationCellWithIndexPath(indexPath: indexPath, txtField: regTextCell.txtField)
            
            regTextCell.titleLbl.text = "Twitter Name"//self.registrationTitleList[indexPath.row]
            regTextCell.descLbl.text = ""
            regTextCell.preHelpLbl.text = ""

            return regTextCell

        }
        else if indexPath.section == 1{
            let radioCell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier_Radio) as! RegRadioBtnCell
            
            radioCell.titleLbl.text = self.dietaryRequirementArray![indexPath.row].name
            
            let option:SelectOption = self.dietaryRequirementArray![indexPath.row]

            radioCell.radioBtn.setImage((option.isOptionSelected! ? UIImage(imageLiteralResourceName: "radio_on_icon") : UIImage(imageLiteralResourceName: "radio_off_icon")), for: .normal)

            return radioCell

        }
        else{
            
            if indexPath.row == 0{
                let regTextCell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier_Text) as! RegTextCell
                
                self.configureRegistrationCellWithIndexPath(indexPath: indexPath, txtField: regTextCell.txtField)
                
                regTextCell.titleLbl.text = self.registrationTitleList[indexPath.row]
                regTextCell.descLbl.text = ""
                
                for item in self.formFieldList!{
                    if item.name == "custom_404"{
                        regTextCell.preHelpLbl.text = item.helpPre
                    }
                }
                return regTextCell

            }
            else if indexPath.row == 1{
                let regTextCell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier_Text) as! RegTextCell
                
                self.configureRegistrationCellWithIndexPath(indexPath: indexPath, txtField: regTextCell.txtField)
                
                regTextCell.titleLbl.text = self.registrationTitleList[indexPath.row]
                regTextCell.descLbl.text = ""
                regTextCell.preHelpLbl.text = ""

                return regTextCell

            }
            else if indexPath.row == 2{
                
                let optionCell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier_Option) as! RegOptionCell
                
                optionCell.titleLbl.text = self.registrationTitleList[indexPath.row]
                
                for item in self.formFieldList!{
                    if item.name == "custom_485"{
                        optionCell.descLbl.text = item.helpPost
                    }
                }
                
                optionCell.yesBtn.tag = indexPath.row
                optionCell.noBtn.tag = indexPath.row

                optionCell.yesBtn.addTarget(self, action:#selector(actionYesBtnTapped(_:)), for: .touchUpInside)
                optionCell.noBtn.addTarget(self, action:#selector(actionNoBtnTapped(_:)), for: .touchUpInside)

                let familyFriendlyAccess = self.eventRegistrationDetails?.familyFriendlyRoomAccess
                
                switch familyFriendlyAccess{
                    
                case "0":
                    optionCell.yesBtn.setImage(UIImage(named: "radio_off_icon"), for: .normal)
                    optionCell.noBtn.setImage(UIImage(named: "radio_on_icon"), for: .normal)
                    break
                case "1":
                    optionCell.yesBtn.setImage(UIImage(named: "radio_on_icon"), for: .normal)
                    optionCell.noBtn.setImage(UIImage(named: "radio_off_icon"), for: .normal)
                    break
                    
                default:
                    optionCell.yesBtn.setImage(UIImage(named: "radio_off_icon"), for: .normal)
                    optionCell.noBtn.setImage(UIImage(named: "radio_off_icon"), for: .normal)
                    break
                }

                return optionCell
            }
            else if indexPath.row == 3{
                
                let optionCell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier_Option) as! RegOptionCell
                
                optionCell.titleLbl.text = self.registrationTitleList[indexPath.row]
                for item in self.formFieldList!{
                    if item.name == "custom_354"{
                        optionCell.descLbl.text = item.helpPost
                    }
                }

                optionCell.yesBtn.tag = indexPath.row
                optionCell.noBtn.tag = indexPath.row
                
                optionCell.yesBtn.addTarget(self, action:#selector(actionYesBtnTapped(_:)), for: .touchUpInside)
                optionCell.noBtn.addTarget(self, action:#selector(actionNoBtnTapped(_:)), for: .touchUpInside)

                let volunteerChairSession = self.eventRegistrationDetails?.volunteerChairSession
                
                switch volunteerChairSession{
                    
                case "1":
                    optionCell.yesBtn.setImage(UIImage(named: "radio_on_icon"), for: .normal)
                    optionCell.noBtn.setImage(UIImage(named: "radio_off_icon"), for: .normal)
                    break
                case "2":
                    optionCell.yesBtn.setImage(UIImage(named: "radio_off_icon"), for: .normal)
                    optionCell.noBtn.setImage(UIImage(named: "radio_on_icon"), for: .normal)
                    break
                    
                default:
                    optionCell.yesBtn.setImage(UIImage(named: "radio_off_icon"), for: .normal)
                    optionCell.noBtn.setImage(UIImage(named: "radio_off_icon"), for: .normal)
                    break
                }
                return optionCell
            }
            else if indexPath.row == 4{
                
                let optionCell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier_Option) as! RegOptionCell
                
                optionCell.titleLbl.text = self.registrationTitleList[indexPath.row]
                for item in self.formFieldList!{
                    if item.name == "custom_403"{
                        optionCell.descLbl.text = item.helpPost
                    }
                }

                optionCell.yesBtn.tag = indexPath.row
                optionCell.noBtn.tag = indexPath.row
                
                optionCell.yesBtn.addTarget(self, action:#selector(actionYesBtnTapped(_:)), for: .touchUpInside)
                optionCell.noBtn.addTarget(self, action:#selector(actionNoBtnTapped(_:)), for: .touchUpInside)

                let joinDelegate = self.eventRegistrationDetails?.joinDelegate
                
                switch joinDelegate{
                    
                case "1":
                    optionCell.yesBtn.setImage(UIImage(named: "radio_on_icon"), for: .normal)
                    optionCell.noBtn.setImage(UIImage(named: "radio_off_icon"), for: .normal)
                    break
                case "2":
                    optionCell.yesBtn.setImage(UIImage(named: "radio_off_icon"), for: .normal)
                    optionCell.noBtn.setImage(UIImage(named: "radio_on_icon"), for: .normal)
                    break
                    
                default:
                    optionCell.yesBtn.setImage(UIImage(named: "radio_off_icon"), for: .normal)
                    optionCell.noBtn.setImage(UIImage(named: "radio_off_icon"), for: .normal)
                    break
                }

                return optionCell

            }
            else if indexPath.row == 5{
                let regTextCell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier_Text) as! RegTextCell
                
                self.configureRegistrationCellWithIndexPath(indexPath: indexPath, txtField: regTextCell.txtField)
                
                regTextCell.titleLbl.text = self.registrationTitleList[indexPath.row]

                for item in self.formFieldList!{
                    if item.name == "custom_221"{
                        if (item.isRequired == "1"){
                            regTextCell.titleLbl.text = self.registrationTitleList[indexPath.row] + "*"
                        }
                    }
                }

                regTextCell.descLbl.text = ""
                regTextCell.preHelpLbl.text = ""

                return regTextCell

            }
            else{
                let imgCell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier_UploadImg) as! RegUploadImgCell
                
                imgCell.titleLbl.text = self.registrationTitleList[indexPath.row]
                
                if self.regImage != nil{
                    imgCell.imgView.image = self.regImage
                }
                
                imgCell.chooseFileBtn.tag = indexPath.row
                imgCell.chooseFileBtn.addTarget(self, action:#selector(actionChooseImageBtnTapped(_:)), for: .touchUpInside)
                
                imgCell.statusLbl.text = (self.isImageSelected) ? "" : "No file choosen"
                
                let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(_:)))
                imgCell.imgView.isUserInteractionEnabled = true
                imgCell.imgView.addGestureRecognizer(tapGestureRecognizer)

                return imgCell
            }
        }
        
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 1{
            let option:SelectOption = self.dietaryRequirementArray![indexPath.row]
            
            let cell = tableView.cellForRow(at: indexPath) as! RegRadioBtnCell
            
            for opt in self.dietaryRequirementArray! {
                if opt == option{
                    if opt.isOptionSelected == false{
                        opt.isOptionSelected = opt.isOptionSelected! ? false : true
                    }
                }
                else{
                    opt.isOptionSelected = false
                }
            }
            cell.radioBtn.setImage((option.isOptionSelected! ? UIImage(imageLiteralResourceName: "radio_on_icon") : UIImage(imageLiteralResourceName: "radio_off_icon")), for: .normal)
            
            self.regStepTwoTableView.reloadSections([indexPath.section], with: .fade)
            
            if (option.name == "Other"){
                self.isOtherDietaryRequirementSelected = true
            }else{
                self.isOtherDietaryRequirementSelected = false
            }
            
            let indexPath = IndexPath(item: 0, section: 2)
            self.regStepTwoTableView.reloadRows(at: [indexPath], with: .automatic)
        }
    }
    
    func configureRegistrationCellWithIndexPath(indexPath: IndexPath, txtField:UITextField) {

        txtField.delegate = self
        txtField.clearButtonMode = UITextField.ViewMode.whileEditing
        
        
        if indexPath.section == 0 {
            self.configureTextField(textField: txtField, tag: kTwitterNameTag, keyboardType: .default)
        }
        else if indexPath.section == 2{
            switch indexPath.row {
            case 0:
                self.configureTextField(textField: txtField, tag: kOthersTag, keyboardType: .default)
                break
            case 1:
                self.configureTextField(textField: txtField, tag: kAccessRequirementTag, keyboardType: .default)
                break
            case 5:
                self.configureTextField(textField: txtField, tag: kBiographyTag, keyboardType: .default)
                break
                
            default:
                break
            }
        }
        
    }

    func configureTextField(textField:UITextField, tag:Int, keyboardType:UIKeyboardType) {
        textField.keyboardType = keyboardType
        textField.tag = tag
        switch textField.tag {
        case kTwitterNameTag:
            textField.placeholder = "Twitter Name"
            
            textField.text = self.eventRegistrationDetails?.twitterName?.count == 0 ? EMPTY_STRING : self.eventRegistrationDetails?.twitterName
            break
        case kOthersTag:
            textField.placeholder = "Other - Dietary Requirements"
            textField.text = self.eventRegistrationDetails?.otherDietaryRequirement?.count == 0 ? EMPTY_STRING : self.eventRegistrationDetails?.otherDietaryRequirement
            break
        case kAccessRequirementTag:
            textField.placeholder = "Access Requirements"
            textField.text = self.eventRegistrationDetails?.accessRequirement?.count == 0 ? EMPTY_STRING : self.eventRegistrationDetails?.accessRequirement
            break
        case kBiographyTag:
            textField.placeholder = "Biography"
            textField.text = self.eventRegistrationDetails?.biography?.count == 0 ? EMPTY_STRING : self.eventRegistrationDetails?.biography
            break
        default:
            break
        }
    }
    
}

extension UIImage {
    func toBase64() -> String? {
        guard let imageData = self.pngData() else { return nil }
        return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    func compressTo(_ expectedSizeInMb:Int) -> UIImage? {
        let sizeInBytes = expectedSizeInMb * 1024 * 1024
        var needCompress:Bool = true
        var imgData:Data?
        var compressingValue:CGFloat = 1.0
        while (needCompress && compressingValue > 0.0) {
            if let data:Data = self.jpegData(compressionQuality: compressingValue) {
                if data.count < sizeInBytes {
                    needCompress = false
                    imgData = data
                } else {
                    compressingValue -= 0.1
                }
            }
        }
        
        if let data = imgData {
            if (data.count < sizeInBytes) {
                return UIImage(data: data)
            }
        }
        return nil
    }

}
