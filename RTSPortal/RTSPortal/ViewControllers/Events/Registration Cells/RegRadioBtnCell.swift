//
//  RegRadioBtnCell.swift
//  RTSPortal
//
//  Created by sajan on 06/08/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit

class RegRadioBtnCell: UITableViewCell {

    @IBOutlet weak var radioBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
