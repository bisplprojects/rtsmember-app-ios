//
//  RegTextCell.swift
//  RTSPortal
//
//  Created by sajan on 06/08/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit

class RegTextCell: UITableViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var txtField: UITextField!
    @IBOutlet weak var descLbl: UILabel!
    
    @IBOutlet weak var preHelpLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
