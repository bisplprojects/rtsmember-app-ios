//
//  EventRegistrationViewController.swift
//  RTSPortal
//
//  Created by sajan on 26/07/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import SVProgressHUD
import Toast_Swift
import SwiftyJSON

protocol EventRegistrationDelegate {
    func didFinishRegistration(message:String)
}

class EventRegistrationViewController: UIViewController,SelectionListViewDelegate,UITextFieldDelegate {

    var eventRegistrationDelegate: EventRegistrationDelegate?

    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var regStepOneTableView: UITableView!
    
    var eventDetails : Event?

    var eventId:String?
    
    let CellIdentifier_Default = "RegTextCell"
    let CellIdentifier_DropDown = "RegDropDownCell"
    let CellIdentifier_Gender = "RegGenderCell"

    // Defining the tableview field tags
    let kPrimaryEmailTag     = 200
    let kBillingEmailTag     = 201
    let kTitleTag            = 202
    let kFirstNameTag        = 203
    let kLastNameTag         = 204
    let kProfessionTag       = 205
    let kJobTitleTag         = 206
    let kPlaceOfWorkTag      = 207
    let kGenderTag           = 208
    let kMobileTag           = 209

    let EMPTY_STRING = ""
    
    var registrationTitleList:[String] = []
    
    var formFieldList:[FormField]?
    
    var profession:String?
    var professionId:String?

    var eventRegisterDetails:EventRegister = EventRegister()
    var dietaryRequirementArray:[SelectOption] = [SelectOption]()
    var dietaryRequirementCount:Int?
    var isOtherDietaryRequirementSelected:Bool = false

    // MARK: -

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.containerView.layer.cornerRadius = 0
        self.containerView.layer.shadowColor = UIColor.gray.cgColor
        self.containerView.layer.shadowOpacity = 0.3
        self.containerView.layer.shadowOffset = .zero //CGSize(width: 1, height: 1)
        self.containerView.layer.shadowRadius = 4
        
        self.registrationTitleList = ["Email (Primary)*","Email (Billing)","Title","First Name*","Last Name*","Profession*","Job Title*","Place of Work*","Gender*","Mobile*"]
        
        if let tmp = self.eventDetails?.formFields{
            self.formFieldList = tmp
        }
        self.profession = ""
        self.professionId = ""

        self.saveDataToEventRegisterModal()
        self.getDietaryRequiremetArray()

    }
    
    func saveDataToEventRegisterModal()  {
        
        self.eventRegisterDetails.eventId = self.eventDetails?.eventId
        
        guard self.formFieldList!.count > 1 else {
            return
        }
        for item in self.formFieldList! {
            let name = item.name
            switch name {
            case "email-Primary":
            
                self.eventRegisterDetails.primaryEmail = (item.defaultValue?.first != nil) ? (item.defaultValue?.first as! String) : ""
                break
            case "email-5":
                self.eventRegisterDetails.billingEmail = (item.defaultValue?.first != nil) ? (item.defaultValue?.first as! String) : ""
                break
            case "formal_title":
                self.eventRegisterDetails.title = (item.defaultValue?.first != nil) ? (item.defaultValue?.first as! String) : ""
                break
            case "first_name":
                self.eventRegisterDetails.firstName = (item.defaultValue?.first != nil) ? (item.defaultValue?.first as! String) : ""
                break
            case "last_name":
                self.eventRegisterDetails.lastName = (item.defaultValue?.first != nil) ? (item.defaultValue?.first as! String) : ""
                break
            case "job_title":
                self.eventRegisterDetails.jobTitle = (item.defaultValue?.first != nil) ? (item.defaultValue?.first as! String) : ""
                break
            case "custom_213":
                self.eventRegisterDetails.placeOfWork = (item.defaultValue?.first != nil) ? (item.defaultValue?.first as! String) : ""
                break
            case "phone-Primary-2":
                self.eventRegisterDetails.mobile = (item.defaultValue?.first != nil) ? (item.defaultValue?.first as! String) : ""
                break

            case "gender_id":
                self.eventRegisterDetails.gender = (item.defaultValue?.first != nil) ? (item.defaultValue?.first as! String) : ""
                break
            case "custom_107":
                self.eventRegisterDetails.profession = (item.defaultValue?.first != nil) ? (item.defaultValue?.first as! String) : ""
                break
                
            case "url-11":
                self.eventRegisterDetails.twitterName = (item.defaultValue?.first != nil) ? (item.defaultValue?.first as! String) : ""
                break
            case "custom_324":
                self.eventRegisterDetails.dietaryRequirement = (item.defaultValue?.first != nil) ? (item.defaultValue?.first as! String) : ""
                break
            case "custom_404":
                self.eventRegisterDetails.otherDietaryRequirement = (item.defaultValue?.first != nil) ? (item.defaultValue?.first as! String) : ""
                break
            case "custom_326":
                self.eventRegisterDetails.accessRequirement = (item.defaultValue?.first != nil) ? (item.defaultValue?.first as! String) : ""
                break
            case "custom_485":
                self.eventRegisterDetails.familyFriendlyRoomAccess = (item.defaultValue?.first != nil) ? (item.defaultValue?.first as! String) : ""
                break
            case "custom_354":
                self.eventRegisterDetails.volunteerChairSession = (item.defaultValue?.first != nil) ? (item.defaultValue?.first as! String) : ""
                break
            case "custom_403":
                self.eventRegisterDetails.joinDelegate = (item.defaultValue?.first != nil) ? (item.defaultValue?.first as! String) : ""
                break
            case "custom_221":
                self.eventRegisterDetails.biography = (item.defaultValue?.first != nil) ? (item.defaultValue?.first as! String) : ""
                break
            case "image_URL":
                self.eventRegisterDetails.picture = (item.defaultValue?.first != nil) ? (item.defaultValue?.first as! String) : ""
                break

            default:
                break
            }
            
        }
    }
    
    func getDietaryRequiremetArray() {
        for item in self.formFieldList!{
            if item.name == "custom_324"
            {
                for opt in item.dietaryRequirementOptions{
                    self.dietaryRequirementArray.append(opt)
                }
                self.dietaryRequirementCount = item.dietaryRequirementOptions.count
                
                for dietaryItem in self.dietaryRequirementArray{
                    
                    if((item.defaultValue?.first as? String) != nil){
                        if dietaryItem.name == (item.defaultValue?.first as! String){
                            dietaryItem.isOptionSelected = true
                        }
                        else{
                            dietaryItem.isOptionSelected = false
                        }
                        
                    }
                }
            }
        }
    }
    
    func saveTextfieldString(textField:UITextField)  {
        
        let strText = textField.text
        switch textField.tag {
        case kPrimaryEmailTag:
            self.eventRegisterDetails.primaryEmail = strText
            break
        case kBillingEmailTag:
            self.eventRegisterDetails.billingEmail = strText
            break
        case kTitleTag:
            self.eventRegisterDetails.title = strText
            break
        case kFirstNameTag:
            self.eventRegisterDetails.firstName = strText
            break
        case kLastNameTag:
            self.eventRegisterDetails.lastName = strText
            break
        case kJobTitleTag:
            self.eventRegisterDetails.jobTitle = strText
            break
        case kPlaceOfWorkTag:
            self.eventRegisterDetails.placeOfWork = strText
            break
        case kMobileTag:
            self.eventRegisterDetails.mobile = strText
            break

        default:
            break
        }
        
    }
    
    
    func setUpParamsForRegistration() {
        
    }
    
    // MARK: - UITextField delegates
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(textField.tag != kProfessionTag) {
            self.saveTextfieldString(textField: textField)
        }
    }

    // MARK: - IBActions

    @IBAction func actionBackBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionRegisterBtnTapped(_ sender: Any) {
        self.view.endEditing(true)
        
        var isValid:Bool?

        for formField in self.formFieldList!{
            
            if formField.isRequired == "1"{
                
                var validation:Valid = .success

                switch formField.name {
                    
                case "email-Primary":
                    validation = Validation.shared.validate(values: (ValidationType.RegistrationPrimaryEmail, self.eventRegisterDetails.primaryEmail!))
                    break
                case "email-5":
                    validation = Validation.shared.validate(values: (ValidationType.RegistrationBillingEmail, self.eventRegisterDetails.billingEmail!))
                    break
                case "formal_title":
                    validation = Validation.shared.validate(values: (ValidationType.RegistrationTitle, self.eventRegisterDetails.title!))
                    break
                case "first_name":
                    validation = Validation.shared.validate(values: (ValidationType.RegistrationFirstName, self.eventRegisterDetails.firstName!))
                    break
                case "last_name":
                    validation = Validation.shared.validate(values: (ValidationType.RegistrationLastName, self.eventRegisterDetails.lastName!))
                    break
                case "job_title":
                    validation = Validation.shared.validate(values: (ValidationType.RegistrationJobTitle, self.eventRegisterDetails.jobTitle!))
                    break
                case "custom_213":
                    validation = Validation.shared.validate(values: (ValidationType.RegistrationPlaceOfWork, self.eventRegisterDetails.placeOfWork!))
                    break
                case "phone-Primary-2":
                    validation = Validation.shared.validate(values: (ValidationType.RegistrationMobile, self.eventRegisterDetails.mobile!))
                    break
                    
                case "gender_id":
                    validation = Validation.shared.validate(values: (ValidationType.RegistrationGender, self.eventRegisterDetails.gender!))
                    break
                case "custom_107":
                    validation = Validation.shared.validate(values: (ValidationType.RegistrationProfession, self.eventRegisterDetails.profession!))
                    break
                    
                default:
                    break
                }
                
                
                switch validation {
                    
                case .success:
                    print("Validation Success")
                    isValid = true
                    // API Call
                    //            self.eventRegistrationApiCallFunc()
                    break
                    
                case .failure(_, let message):
                    isValid = false
                    print(message.localized())
                    self.view.makeToast(message.localized(), duration: 3.0, position: .center)
                    return
                }

            }
        }
        

        if isValid == true {
            print("Final Validation Success")
            let storyboard = UIStoryboard(name: "Event", bundle: Bundle.main)
            let regStep2VC = storyboard.instantiateViewController(withIdentifier: "EventRegistrationStep2ViewController") as! EventRegistrationStep2ViewController
            var eventDetail:Event = Event()
            eventDetail = self.eventDetails!

            regStep2VC.eventDetails = eventDetail
            regStep2VC.eventRegistrationDetails = self.eventRegisterDetails
            
            var dietaryReqArray:[SelectOption] = [SelectOption]()
            dietaryReqArray = self.dietaryRequirementArray
            regStep2VC.dietaryRequirementArray = dietaryReqArray
            regStep2VC.dietaryRequirementCount = self.dietaryRequirementCount
            
            self.navigationController?.pushViewController(regStep2VC, animated: true)

        }
        else{
            print(" Validation failed")

        }

    }
    
    
    @objc func actionDropDownBtnTapped(_ sender: UIButton) {
        
        for formField in self.eventDetails!.formFields {
            if (formField.title == "Profession" && formField.htmlType == "Select"){
                if formField.selectOptions.count > 0{
                    // Safe Present
                    if let vc = UIStoryboard(name: "Event", bundle: Bundle.main).instantiateViewController(withIdentifier: "SelectionListViewController") as? SelectionListViewController
                    {
                        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                        vc.selectOptionList = formField.selectOptions
                        vc.selectionDelegate = self
                        present(vc, animated: true, completion: nil)
                    }

                }
                else{
                    self.view.makeToast("No data", duration: 3.0, position: .center)
                }
            }
        }
        
    }

    @objc func actionGenderBtnTapped(_ sender: UIButton) {
        
        switch sender.tag {
        case 1:
            self.eventRegisterDetails.gender = "1"
            break
        case 2:
            self.eventRegisterDetails.gender = "2"
            break
        case 3:
            self.eventRegisterDetails.gender = "3"
            break
        default:
            break
        }
        let indexPath = IndexPath(item: 8, section: 0)
        self.regStepOneTableView.reloadRows(at: [indexPath], with: .none)

    }

    // MARK: - SelectionListView Delegate
    
    func didFinishSelectingItem(item: SelectOption) {
        
        self.profession = item.name
        self.professionId = item.optionId
        self.eventRegisterDetails.profession = self.professionId
        let indexPath = IndexPath(item: 5, section: 0)
        self.regStepOneTableView.reloadRows(at: [indexPath], with: .none)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.priceFieldList = nil


    }

    // MARK: - Webservice
    
    func eventRegistrationApiCallFunc() {
        
        SVProgressHUD.show()
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]

        // logintype : veda or live
        let token = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "pin": dict["PIN"],
                     "logintype": "live"];
        let tokenString = JSONStringify(value: token as! [String : String])
        let param = ["entity" : "CiviMobileParticipant",
                     "action" : "register",
                     "token" : tokenString,
                     "event_id" : self.eventId,
                     "cid" :  dict["ContactId"]];
        print("Events Registration request params : \(param)")
        
        apiObj.postData(url: kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("Events Registration response : \(json)")
            if status
            {
                if json["is_error"] == 0{
                    
                    var style = ToastStyle()
                    style.messageColor = UIColor.white
                    style.titleFont.withSize(16)

                    let dictonary = json["values"]
                    if dictonary["status"] == true{
                        
                        let message = dictonary["message"].stringValue
                        
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                self.view?.makeToast(message, duration: 3.0, position: ToastPosition.center, title:"", image:nil, style: style, completion: { (true) in
                                    self.eventRegistrationDelegate?.didFinishRegistration(message: "success")
                                    self.navigationController?.popViewController(animated: true)
                                })
                            }
                    }
                    else{
                        let message = dictonary["message"].stringValue
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                            self.view?.makeToast(message, duration: 3.0, position: ToastPosition.center, title:"", image:nil, style: style, completion: { (true) in
                                self.navigationController?.popViewController(animated: true)
                            })
                        }
                    }
                }
                else{
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.view.makeToast("Registration failed", duration: 3.0, position: .center)
                    }
                }
            }
            else{
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                    self.view.makeToast("Registration failed", duration: 3.0, position: .center)
                }
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension EventRegistrationViewController: UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.registrationTitleList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 5{
            let dropDownCell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier_DropDown) as! RegDropDownCell
            
            dropDownCell.titleLbl.text = "Profession"
            dropDownCell.txtField.placeholder = "Profession"
            dropDownCell.txtField.isEnabled = false
            if (self.eventRegisterDetails.profession != "" ) {
                //todo change variable to modal class
                for item in self.formFieldList!{
                    if item.name == "custom_107"
                    {
                        for opt in item.selectOptions{
                            if opt.optionId == self.eventRegisterDetails.profession{
                                self.profession = opt.name
                            }
                        }
                    }
                }
                dropDownCell.txtField.text = self.profession
            }

            dropDownCell.dropDownBtn.tag = indexPath.row
            dropDownCell.dropDownBtn.addTarget(self, action:#selector(actionDropDownBtnTapped(_:)), for: .touchUpInside)

            return dropDownCell

        }
        else if indexPath.row == 8{
            let genderCell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier_Gender) as! RegGenderCell
            
            genderCell.femaleBtn.addTarget(self, action:#selector(actionGenderBtnTapped(_:)), for: .touchUpInside)
            genderCell.maleBtn.addTarget(self, action:#selector(actionGenderBtnTapped(_:)), for: .touchUpInside)
            genderCell.noneBtn.addTarget(self, action:#selector(actionGenderBtnTapped(_:)), for: .touchUpInside)

            let genderId = self.eventRegisterDetails.gender
            
            switch genderId{
                
            case "1":
                genderCell.femaleBtn.setImage(UIImage(named: "radio_on_icon"), for: .normal)
                genderCell.maleBtn.setImage(UIImage(named: "radio_off_icon"), for: .normal)
                genderCell.noneBtn.setImage(UIImage(named: "radio_off_icon"), for: .normal)
                break
            case "2":
                genderCell.femaleBtn.setImage(UIImage(named: "radio_off_icon"), for: .normal)
                genderCell.maleBtn.setImage(UIImage(named: "radio_on_icon"), for: .normal)
                genderCell.noneBtn.setImage(UIImage(named: "radio_off_icon"), for: .normal)
                break
            case "3":
                genderCell.femaleBtn.setImage(UIImage(named: "radio_off_icon"), for: .normal)
                genderCell.maleBtn.setImage(UIImage(named: "radio_off_icon"), for: .normal)
                genderCell.noneBtn.setImage(UIImage(named: "radio_on_icon"), for: .normal)
                break

            default:
                genderCell.femaleBtn.setImage(UIImage(named: "radio_off_icon"), for: .normal)
                genderCell.maleBtn.setImage(UIImage(named: "radio_off_icon"), for: .normal)
                genderCell.noneBtn.setImage(UIImage(named: "radio_off_icon"), for: .normal)

                break
                
            }
            
            return genderCell

        }
        else{
            let regTextCell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier_Default) as! RegTextCell

            self.configureRegistrationCellWithIndexPath(indexPath: indexPath, txtField: regTextCell.txtField)
            
            regTextCell.titleLbl.text = self.registrationTitleList[indexPath.row]
            
            if indexPath.row == 6{
                for item in self.formFieldList!{
                    if item.name == "job_title"{
                        regTextCell.descLbl.text = item.helpPost
                    }
                }
            }
            else{
                regTextCell.descLbl.text = ""
            }
            
            if (indexPath.row == 4){
                regTextCell.txtField.smartQuotesType = .no
            }
            return regTextCell

        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func configureRegistrationCellWithIndexPath(indexPath: IndexPath, txtField:UITextField) {
        
//        let textfield:UITextField = txtField
        txtField.delegate = self
        txtField.clearButtonMode = UITextField.ViewMode.whileEditing

        switch indexPath.row {
        case 0:
            self.configureTextField(textField: txtField, tag: kPrimaryEmailTag, keyboardType: .emailAddress)
            break
        case 1:
            self.configureTextField(textField: txtField, tag: kBillingEmailTag, keyboardType: .emailAddress)
            break
        case 2:
            self.configureTextField(textField: txtField, tag: kTitleTag, keyboardType: .default)
            break
        case 3:
            self.configureTextField(textField: txtField, tag: kFirstNameTag, keyboardType: .default)
            break
        case 4:
            self.configureTextField(textField: txtField, tag: kLastNameTag, keyboardType: .default)
            break
        case 6:
            self.configureTextField(textField: txtField, tag: kJobTitleTag, keyboardType: .default)
            break
        case 7:
            self.configureTextField(textField: txtField, tag: kPlaceOfWorkTag, keyboardType: .default)
            break
        case 9:
            self.configureTextField(textField: txtField, tag: kMobileTag, keyboardType: .phonePad)
            break

        default:
            break
        }
        
    }
    
    func configureTextField(textField:UITextField, tag:Int, keyboardType:UIKeyboardType) {
        textField.keyboardType = keyboardType
        textField.tag = tag
        switch textField.tag {
        case kPrimaryEmailTag:
            textField.placeholder = "Email (Primary)"
            textField.text = self.eventRegisterDetails.primaryEmail?.count == 0 ? EMPTY_STRING : self.eventRegisterDetails.primaryEmail
            break
        case kBillingEmailTag:
            textField.placeholder = "Email (Billing)"
            textField.text = self.eventRegisterDetails.billingEmail?.count == 0 ? EMPTY_STRING : self.eventRegisterDetails.billingEmail
            break
        case kTitleTag:
            textField.placeholder = "Title"
            textField.text = self.eventRegisterDetails.title?.count == 0 ? EMPTY_STRING : self.eventRegisterDetails.title
            break
        case kFirstNameTag:
            textField.placeholder = "First Name"
            textField.text = self.eventRegisterDetails.firstName?.count == 0 ? EMPTY_STRING : self.eventRegisterDetails.firstName
            break
        case kLastNameTag:
            textField.placeholder = "Last Name"
            textField.text = self.eventRegisterDetails.lastName?.count == 0 ? EMPTY_STRING : self.eventRegisterDetails.lastName
            break
        case kJobTitleTag:
            textField.placeholder = "Job Title"
            textField.text = self.eventRegisterDetails.jobTitle?.count == 0 ? EMPTY_STRING : self.eventRegisterDetails.jobTitle
            break
        case kPlaceOfWorkTag:
            textField.placeholder = "Place of Work"
            textField.text = self.eventRegisterDetails.placeOfWork?.count == 0 ? EMPTY_STRING : self.eventRegisterDetails.placeOfWork
            break
        case kMobileTag:
            textField.placeholder = "Mobile"
            textField.text = self.eventRegisterDetails.mobile?.count == 0 ? EMPTY_STRING : self.eventRegisterDetails.mobile
            break
        default:
            break
        }
    }

}

