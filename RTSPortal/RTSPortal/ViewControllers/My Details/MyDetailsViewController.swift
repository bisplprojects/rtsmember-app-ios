//
//  MyDetailsViewController.swift
//  RTSPortal
//
//  Created by sajan on 23/07/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import SVProgressHUD

class MyDetailsViewController: UIViewController,MyDetailsDelegate {
    
    
    @IBOutlet weak var myDetailsTableView: UITableView!
    
    @IBOutlet weak var editDetailsBtn: UIButton!
    
    var userDetails : User?
    var regImage:UIImage?

    let cellIdentifier = "MyDetailCell"
    let cellIdentifierAddress = "AddressCell"
    let cellIdentifierHeader = "MyDetailHeaderCell"

    // MARK: -

    override func viewDidLoad() {
        super.viewDidLoad()
        self.editDetailsBtn.isHidden = true
        // Do any additional setup after loading the view.
        self.loadMyDetailsApiCallFunc()
       
    }

    func ConvertBase64StringToImage (imageBase64String:String) -> UIImage {
        let imageData = Data.init(base64Encoded: imageBase64String, options: .init(rawValue: 0))
        if imageData != nil{
            let image = UIImage(data: imageData!)
            return image!
        }else{
            return UIImage()
        }
    }

    // MARK: - IBActions

    @IBAction func actionBackBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionEditDetailsBtnTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Profile", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: "EditMyDetailsViewController") as! EditMyDetailsViewController
        vc.myDetails = self.userDetails
        vc.myDetailsDelegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - TapGesture Methods
    
    @objc func imageTapped(_ sender: UITapGestureRecognizer) {
        if self.regImage != nil {
            let newImageView = UIImageView(image: self.regImage)
            newImageView.contentMode = .scaleAspectFit
            newImageView.frame = UIScreen.main.bounds
            newImageView.backgroundColor = .black
            newImageView.isUserInteractionEnabled = true
            let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
            newImageView.addGestureRecognizer(tap)
            self.view.addSubview(newImageView)
        }
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        sender.view?.removeFromSuperview()
    }

    // MARK: - My Details Delegate Method

    func didFinishUpdatingMyDetails(isUpdated: Bool) {
        if (isUpdated == true) {
            self.loadMyDetailsApiCallFunc()
        }
    }

    // MARK: - Webservice
    
    func loadMyDetailsApiCallFunc() {
        
        SVProgressHUD.show()
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        
        // logintype : veda or live
        let token = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "pin": dict["PIN"],
                     "sessionId": dict["sessionId"],
                     "logintype": "live"];
        let tokenString = JSONStringify(value: token as! [String : String])
        let param = ["entity" : "CiviMobileContact",
                     "action" : "mydetails",
                     "token" : tokenString,
                     "cid" : dict["ContactId"]];
        print("My Membership Details request params : \(param)")
        
        apiObj.postData(url: kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("My Membership Details response : \(json)")
            
            if status
            {
                if json["is_error"] == 0{
                    let dictonary = json["values"]
                    if dictonary["validuser"] == true{
                        
                        let dataDict = dictonary["data"].dictionaryValue
                        let message = dataDict["message"]?.stringValue
                        
                        if dataDict["status"] == true{
                            
                            let myDetail = dataDict["mydetails"]?.arrayValue
                            if myDetail!.count > 0 {
                                
                                let responseDict = myDetail?.first
                                self.userDetails = User(dict: (responseDict?.dictionary)!)
                                
                                DispatchQueue.main.async {
                                    SVProgressHUD.dismiss()
                                    self.editDetailsBtn.isHidden = false
                                    self.updateUserData()
                                    self.myDetailsTableView.reloadData()
                                    // TO DO : Display toast msg
                                }
                            }
                            else {
                                //Display msg
                                DispatchQueue.main.async {
                                    SVProgressHUD.dismiss()
                                    self.view.makeToast("Details not found", duration: 3.0, position: .center)
                                }
                            }
                        }
                        else{
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                self.view.makeToast(message, duration: 3.0, position: .center)
                            }
                        }
                    }
                    else
                    {
                        //Display msg
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                        }
                    }
                }
                else
                {
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                }
            }
            else
            {
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
        }
    }

    func updateUserData()  {
        
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        let userName = self.userDetails?.userName
        let userImageString = self.userDetails?.userProfileImgURL
        let sessionID = dict["sessionId"]
        let contactId = dict["ContactId"]
        let roleStr = dict["role"]
        let pinStr = dict["PIN"]

        let bgsUserDict = ["UserName": userName, "ContactId": contactId, "PIN" : pinStr, "sessionId" : sessionID, "role" : roleStr, "ImageString" : userImageString]
        UserDefaults.standard.setBGSUserData(dict: bgsUserDict as! [String : String])
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MyDetailsViewController: UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.userDetails != nil) ? 6 : 0//6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        if indexPath.row == 0 {
            let headerCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifierHeader) as! MyDetailHeaderCell
            //headerCell.sizeToFit()
           //tableView.rowHeight = headerCell.frame.height
            if (self.userDetails?.userProfileImgURL != nil){
                
                if self.userDetails?.userProfileImgURL != ""{
                    if (self.userDetails?.userProfileImgURL!.hasPrefix("https"))!{
                        headerCell.profileImgView.sd_setImage(with: URL(string: self.userDetails!.userProfileImgURL!), placeholderImage: UIImage(named: ""))
                    }
                    else{
                        self.regImage = self.ConvertBase64StringToImage(imageBase64String: self.userDetails!.userProfileImgURL!)
                        if (self.regImage != nil){
                            headerCell.profileImgView.image = self.regImage
                        }
                    }
                }
            }

            headerCell.layoutIfNeeded()

            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(_:)))
            headerCell.profileImgView.isUserInteractionEnabled = true
            headerCell.profileImgView.addGestureRecognizer(tapGestureRecognizer)

            headerCell.nameLbl.text = self.userDetails?.userName
            headerCell.jobTitleLbl.text = self.userDetails?.jobTitle
            headerCell.placeOfWorkLbl.text = self.userDetails?.placeOfWork

            return headerCell

        }
        else if indexPath.row == 4{
            let addressCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifierAddress) as! AddressCell
           // addressCell.sizeToFit()
            //tableView.rowHeight = addressCell.frame.height
            
            addressCell.imgView.image = UIImage(imageLiteralResourceName: "location_icon_gray")
            addressCell.streetNameLbl.text = self.userDetails?.streetName
            addressCell.supAddress1Lbl.text = self.userDetails?.supplimentalAddress1
            addressCell.aupAddress2Lbl.text = self.userDetails?.supplimentalAddress2
            addressCell.cityLbl.text = self.userDetails?.city
            addressCell.postCodeLbl.text = self.userDetails?.postcode
            addressCell.stateLbl.text = self.userDetails?.stateName
            addressCell.countryLbl.text = self.userDetails?.country

            return addressCell

        }
        else{
            let myDetailCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! MyDetailCell
            
            //myDetailCell.sizeToFit();
           // tableView.rowHeight = myDetailCell.frame.height
            
            
            switch indexPath.row {
            case 1:
                myDetailCell.imgView.image = UIImage(imageLiteralResourceName: "phone_icon")
            myDetailCell.imgView.sizeToFit()
                
                myDetailCell.titleLbl.text = self.userDetails?.userPhoneNumber
               // myDetailCell.titleLbl.sizeToFit()
                //myDetailCell.sizeToFit()
                //tableView.rowHeight = myDetailCell.frame.height
                break
            case 2:
                myDetailCell.imgView.image = UIImage(imageLiteralResourceName: "email_icon")
                myDetailCell.titleLbl.text = self.userDetails?.userEmail
                break
            case 3:
                myDetailCell.imgView.image = UIImage(imageLiteralResourceName: "profession_icon")
                myDetailCell.titleLbl.text = self.userDetails?.profession
                break
            case 5:
                myDetailCell.imgView.image = UIImage(imageLiteralResourceName: "message_icon")
                myDetailCell.titleLbl.text = self.userDetails?.biography
                break
            default:
                break
            }
            return myDetailCell

        }
    }
        
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}
