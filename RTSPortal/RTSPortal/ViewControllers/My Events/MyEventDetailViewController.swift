//
//  MyEventDetailViewController.swift
//  RTSPortal
//
//  Created by sajan on 18/07/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import SwiftyJSON
import SVProgressHUD
import GoogleMaps
import GooglePlaces
import MapKit
import CoreLocation
import AddressBookUI

class MyEventDetailViewController: UIViewController,GMSMapViewDelegate,UpdateAmendSessionDelegate {
    
    
    @IBOutlet weak var eventDetailScrollView: UIScrollView!
    @IBOutlet weak var qrCodeImgView: UIImageView!
    @IBOutlet weak var eventTitleContentView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var amendRegistrationBtn: UIButton!
    @IBOutlet weak var cancelRegistrationBtn: UIButton!
    @IBOutlet weak var descTitleLbl: UILabel!
    @IBOutlet weak var mapTitleLbl: UILabel!
    
    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var mySessionContainerView: UIView!
    @IBOutlet weak var sessionTitleLbl: UILabel!
    @IBOutlet weak var sessionDescriptionLbl: UILabel!
    
    var event : Event?
    var eventDetails : Event?
    
    var latitude:Double?
    var longitude:Double?
    var latitudeString:String?
    var longitudeString:String?

    var sessionChoiceString:String?
    

    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.eventTitleContentView.layer.cornerRadius = 5
        self.eventTitleContentView.layer.shadowColor = UIColor.gray.cgColor
        self.eventTitleContentView.layer.shadowOpacity = 0.3
        self.eventTitleContentView.layer.shadowOffset = .zero //CGSize(width: 1, height: 1)
        self.eventTitleContentView.layer.shadowRadius = 4

        self.mySessionContainerView.layer.cornerRadius = 5
        self.mySessionContainerView.layer.shadowColor = UIColor.gray.cgColor
        self.mySessionContainerView.layer.shadowOpacity = 0.3
        self.mySessionContainerView.layer.shadowOffset = .zero //CGSize(width: 1, height: 1)
        self.mySessionContainerView.layer.shadowRadius = 4

        self.mapView.delegate = self
        

        self.hideUI()
        self.loadMyEventDetailsApiCallFunc()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let label = UILabel(frame: CGRect(x: 10, y: 10, width: 100, height: 20))
        label.backgroundColor = UIColor.white
        label.textColor = UIColor(red: 19.0/255.0 , green: 106.0/255.0, blue: 244.0/255.0, alpha: 1.0)
        label.text = "View larger map"
        label.font = UIFont(name: "Arial", size: 12)
        label.textAlignment = .center
        label.layer.cornerRadius = 2
        label.clipsToBounds = true
        label.isUserInteractionEnabled = true
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.actionViewLargerMapLblTapped))
        label.addGestureRecognizer(tap)
        
        self.mapView.settings.consumesGesturesInView = false
        self.mapView.addSubview(label)
        self.mapView.bringSubviewToFront(label)
        
    }

    // MARK: - UpdateAmendSession Delegate Method

    func didFinishUpdatingAmendSessions(isUpdated: Bool) {
        if isUpdated == true {
            self.loadMyEventDetailsApiCallFunc()
        }
    }

    // MARK: - IBActions

    @IBAction func actionBackBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionAmendRegistrationBtnTapped(_ sender: Any) {
        
        if (self.eventDetails?.priceFields.count)! > 0 {
            let storyboard = UIStoryboard(name: "Event", bundle: Bundle.main)
            let vc = storyboard.instantiateViewController(withIdentifier: "AmendRegistrationViewController") as! AmendRegistrationViewController
            vc.participantId = self.event?.participantId
            vc.priceFieldList = self.eventDetails!.priceFields
            vc.updateAmendSessionDelegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else{
            self.view.makeToast("No amend sessions", duration: 3.0, position: .center)
        }

    }
    
    @IBAction func actionCancelRegistrationBtnTapped(_ sender: Any) {
        if let cancelRegVC = UIStoryboard(name: "Event", bundle: Bundle.main).instantiateViewController(withIdentifier: "CancelRegistrationViewController") as? CancelRegistrationViewController
        {
            cancelRegVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            cancelRegVC.cancelText = self.eventDetails?.eventCancellationText
            present(cancelRegVC, animated: true, completion: nil)
        }

    }
    
    
    // MARK: - Webservice
    
    func loadMyEventDetailsApiCallFunc() {
        
        SVProgressHUD.show()
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        
        // logintype : veda or live
        let token = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "pin": dict["PIN"],
                     "sessionId": dict["sessionId"],
                     "logintype": "live"];
        let tokenString = JSONStringify(value: token as! [String : String])
        let param = ["entity" : "CiviMobileParticipant",
                     "action" : "getdetails",
                     "token" : tokenString,
                     "pid" : self.event?.participantId];
        print("My Events Details request params : \(param)")
        
        apiObj.postData(url: kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("My Events Details response : \(json)")
            
            if status
            {
                if json["is_error"] == 0{
                    let dictonary = json["values"]
                    if dictonary["validuser"] == true{
                        
                        let dataDict = dictonary["data"].dictionaryValue
                        let myEventDetail = dataDict["participant_details"]?.arrayValue
                        
                      
                        if myEventDetail!.count > 0 {
                            
                            
                            
                            let responseDict = myEventDetail?.first
                            let amdSession = responseDict?["amend_sessions"].arrayValue
                            if amdSession!.count > 0 {
                                self.showAmmendSessionBtn();
                            }
                            self.eventDetails = Event(eventDetailsDict: (responseDict?.dictionary)!)
                            
                            self.sessionChoiceString = ""
                            for (_, str) in ((self.eventDetails?.sessionChoices!.enumerated())!) {
                                self.sessionChoiceString = self.sessionChoiceString! + str + "\n\n"
                            }

                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                // TO DO : Display toast msg
                                self.updateUI()
                                self.showUI()
                            }
                        }
                        else {
                            //Display msg
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                self.view.makeToast("Event details not found", duration: 3.0, position: .center)
                            }
                        }
                    }
                    else{
                        //Display msg
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                        }
                    }
                }
                else{
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                }
            }
            else{
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
        }
    }

    func updateUI()  {
        self.qrCodeImgView.sd_setImage(with: URL(string:self.eventDetails!.qrCodeURL!), placeholderImage: UIImage(named: ""))
        self.titleLbl.text = self.eventDetails?.eventTitle
        self.statusLbl.text = self.eventDetails?.participantStatus
        
        let attributedString = NSMutableAttributedString(string: (self.eventDetails?.eventDescription!.htmlToString)!)
        // *** Create instance of `NSMutableParagraphStyle`
        let paragraphStyle = NSMutableParagraphStyle()
        // *** set LineSpacing property in points ***
        paragraphStyle.lineSpacing = 3 // Whatever line spacing you want in points
        paragraphStyle.alignment = .justified
        // *** Apply attribute to string ***
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        
        // *** Set Attributed String to your label ***
        self.descriptionLbl.attributedText = attributedString

        self.sessionDescriptionLbl.text = self.sessionChoiceString
        
        self.getLatLngForZip(zipCode: self.eventDetails!.postalCode!)
        
        if (self.latitude != nil || self.longitude != nil){
            let position = CLLocationCoordinate2D(latitude: self.latitude!, longitude: self.longitude!)
            let marker = GMSMarker(position: position)
            marker.title = ""
            marker.map = self.mapView
            self.mapView.animate(toZoom: 10)
            self.mapView.animate(toLocation: CLLocationCoordinate2D(latitude: self.latitude!, longitude: self.longitude!))
        }
        
        let startDate = self.eventDetails?.eventStartDate?.toDate()!
        let startDateString = startDate?.toString()
        self.dateLbl.text = startDateString
        let timeStr = self.getTimeFromDateString(dateString: self.eventDetails!.eventStartDate!)
        self.timeLbl.text = timeStr
        
        if self.eventDetails?.eventCancellationBtnText != "" {
            self.cancelRegistrationBtn.setTitle(self.eventDetails?.eventCancellationBtnText?.uppercased(), for: .normal)
        }

    }
    
    func hideUI()  {
        self.titleLbl.text = ""
        self.statusLbl.text = ""
        self.descriptionLbl.text = ""
        self.dateLbl.text = ""
        self.timeLbl.text = ""
        self.sessionTitleLbl.text = ""
        self.eventTitleContentView.isHidden = true
        self.mySessionContainerView.isHidden = true
        self.descTitleLbl.isHidden = true
        self.mapTitleLbl.isHidden = true
        self.mapView.isHidden = true
        self.amendRegistrationBtn.isHidden = true
        self.cancelRegistrationBtn.isHidden = true
        
        self.cancelRegistrationBtn.setTitle("", for: .normal)
    }
    
    func showUI()  {
        self.eventTitleContentView.isHidden = false
        self.descTitleLbl.isHidden = false
        self.mapTitleLbl.isHidden = false
        self.mapView.isHidden = false
        //self.amendRegistrationBtn.isHidden = false
        
        if self.eventDetails?.eventCancellationBtnText != "" {
            self.cancelRegistrationBtn.isHidden = false
        }
        
        self.mySessionContainerView.isHidden = ((self.eventDetails?.sessionChoices!.count)! >= 1) ? false : true
        self.sessionTitleLbl.text = ((self.eventDetails?.sessionChoices!.count)! >= 1) ? "My Sessions" : ""

    }
    func showAmmendSessionBtn(){
        self.amendRegistrationBtn.isHidden = false
    }
    
    func getLatLngForZip(zipCode: String) {
        
        let zipString = zipCode.removingWhitespaces()
        
        let baseUrl = kGoogleMapsBaseUrl
        let apikey = kGoogleMapsAPIKey
        
        if zipString != ""{
            let url = NSURL(string: "\(baseUrl)address=\(zipString)&key=\(apikey)")
            let data = NSData(contentsOf: url! as URL)
            if let json = try? JSON(data: data! as Data) {
                
                if let result = json["results"].array {
                    if result.count > 0{
                        if let geometry = result[0]["geometry"].dictionary {
                            if let location = geometry["location"]?.dictionary {
                                let latitude = location["lat"]?.doubleValue
                                let longitude = location["lng"]?.doubleValue
                                self.latitudeString = location["lat"]?.stringValue
                                self.longitudeString = location["lng"]?.stringValue
                                self.latitude = latitude
                                self.longitude = longitude
                                print("\n\(String(describing: latitude)), \(String(describing: longitude))")
                            }
                        }
                    }
                }
            }

        }
    }

    @objc func actionViewLargerMapLblTapped() {
        
        if(self.latitudeString == nil || self.longitudeString == nil){
            self.view.makeToast("Address not found", duration: 3.0, position: .center)
        }else{
            let markerLat = self.latitudeString
            let markerLong = self.longitudeString
            
            if (markerLat != "" && markerLong != ""){
                
                if (UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL)) {
                    
                    let urlString = "https://www.google.com/maps/search/?api=1&query=" + markerLat! + "," + markerLong!
                    
                    UIApplication.shared.open(NSURL(string:urlString)! as URL, options: [:], completionHandler: nil)
                }
                else {
                    
                    let alert = UIAlertController(title: "Google Maps not found", message: "Please install Google Maps in your device.", preferredStyle: UIAlertController.Style.alert)
                    
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else
            {
                self.view.makeToast("Address not found", duration: 3.0, position: .center)
            }
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        print("You tapped : \(marker.position.latitude),\(marker.position.longitude)")
        return true
    }

    func mapView(_ mapView: GMSMapView, didLongPressInfoWindowOf marker: GMSMarker) {
        
    }

    func getTimeFromDateString(dateString:String) -> String {
        let array = dateString.components(separatedBy: .whitespaces)
        let time = array.last
        let timeComponentsArray = time?.components(separatedBy: ":")
        let timeString = timeComponentsArray![0] + ":" + timeComponentsArray![1]
        return timeString
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension String {
    
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }

    func removingWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
    
    func toDate(withFormat format: String = "yyyy-MM-dd HH:mm:ss")-> Date?{
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(identifier: "Europe/London")//Asia/Tehran
        dateFormatter.locale = Locale(identifier: "en_GB")//fa-IR
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = format
        let date = dateFormatter.date(from: self)
        
        return date
    }
    
    func toDateFromString(withFormat format: String = "yyyy-MM-dd")-> Date?{
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(identifier: "UTC")//Asia/Tehran
        dateFormatter.locale = Locale(identifier: "en_GB")//fa-IR
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = format
        let date = dateFormatter.date(from: self)
        
        return date
    }
}

extension Date {
    
    func toString(withFormat format: String = "dd'-'MM'-'yyyy") -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(identifier: "Europe/London")//Asia/Tehran
        dateFormatter.locale = Locale(identifier: "en_GB")//fa-IR
        dateFormatter.calendar = Calendar(identifier: .gregorian)

        dateFormatter.dateFormat = format

        let dateStr = dateFormatter.string(from: self)
        
        return dateStr
    }
}

