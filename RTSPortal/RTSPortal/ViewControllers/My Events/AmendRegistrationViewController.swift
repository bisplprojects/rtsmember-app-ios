//
//  AmendRegistrationViewController.swift
//  RTSPortal
//
//  Created by sajan on 18/07/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import SVProgressHUD
import Toast_Swift

protocol UpdateAmendSessionDelegate {
    
    func didFinishUpdatingAmendSessions(isUpdated:Bool)
}

class AmendRegistrationViewController: UIViewController {

    var updateAmendSessionDelegate: UpdateAmendSessionDelegate?
    
    @IBOutlet weak var amendSessionTableView: UITableView!
    @IBOutlet weak var updateBtn: UIButton!
    
    let cellIdentifier = "AmendSessionCell"

    var eventDetails : Event?
    var priceFieldList:[PriceField] = []
    var selectedOptionsArray:[Option] = []
    
    var isRequiredFieldSelected:Bool = false
    var requiredPriceCount: Int = 0
    
    var selectedPriceOptionsStringArray:[String] = []
    
    var participantId : String?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.amendSessionTableView.sectionHeaderHeight = UITableView.automaticDimension
        self.amendSessionTableView.estimatedSectionHeaderHeight = 40

    }
    
    // MARK: - IBActions

    @IBAction func actionBackBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionUpdateBtnTapped(_ sender: Any) {
        
        var messageString = ""
        
        for price in self.priceFieldList{
            for opt in price.options{
                if opt.isAmendOptionSelected == true{
                    if !selectedOptionsArray.contains(opt){
                        self.selectedOptionsArray.append(opt)
                    }
                }
                else{
                    if selectedOptionsArray.contains(opt){
                        for selOpt in selectedOptionsArray{
                            if selOpt == opt{
                                selectedOptionsArray.removeAll { $0.optionId == selOpt.optionId }
                            }
                        }
                    }
                }
            }
        }
        
        self.requiredPriceCount = 0
        
        for priceField in self.priceFieldList {
            if priceField.isRequired == "1"{
                
                self.requiredPriceCount += 1
                
                messageString = priceField.priceFieldLabel!
                for opt in priceField.options{
                    if self.selectedOptionsArray.contains(opt){
                        self.isRequiredFieldSelected = true
                    }
                }
            }
        }
        
        if self.requiredPriceCount >= 1{
            if self.isRequiredFieldSelected == false{
                self.view.makeToast("Please select one from \(messageString)", duration: 3.0, position: .center)
            }
            else{
                self.selectedPriceOptionsStringArray.removeAll()
                for opt in self.selectedOptionsArray{
                    self.selectedPriceOptionsStringArray.append(opt.optionId!)
                }
                self.updateAmendSessionsApiCallFunc()
            }
        }
        else{
            self.selectedPriceOptionsStringArray.removeAll()
            for opt in self.selectedOptionsArray{
                self.selectedPriceOptionsStringArray.append(opt.optionId!)
            }
            self.updateAmendSessionsApiCallFunc()

        }
        
    }
    
    // MARK: - Webservice
    
    func updateAmendSessionsApiCallFunc() {
        self.updateBtn.isUserInteractionEnabled = false
        
        SVProgressHUD.show()
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        
        // logintype : veda or live
        let token = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "sessionId": dict["sessionId"],
                     "logintype": "live"];
        let tokenString = JSONStringify(value: token as! [String : String])
        
        let priceOptionString = convertIntoJSONString(arrayObject: self.selectedPriceOptionsStringArray)
        
        let param = ["entity" : "CiviMobileParticipant",
                     "action" : "amendsessions",
                     "token" : tokenString,
                     "pid" :  self.participantId!,
                     "session_options" : priceOptionString as Any] as [String : Any];

        print("Update Amend Session request params : \(param)")
        
        apiObj.postData(url: kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("Update Amend Session response : \(json)")
            if status
            {
                
                if json["is_error"] == 0{
                    let dictonary = json["values"]
                    
                    if dictonary["validuser"] == true{
                        
                        let dataDictonary = dictonary["data"]
                        
                        let msg = dataDictonary["message"].stringValue
                        if dataDictonary["status"] == true{
                            
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                //  Display toast msg
                                let style = ToastStyle()
                                self.view?.makeToast(msg, duration: 2.0, position: ToastPosition.center, title:"", image:nil, style: style, completion: { (true) in
                                    // Move to Participant details
                                    self.updateBtn.isUserInteractionEnabled = true
                                self.updateAmendSessionDelegate?.didFinishUpdatingAmendSessions(isUpdated: true)
                                    self.navigationController?.popViewController(animated: true)
                                })
                            }
                        }
                        else{
                            //Display msg
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                let style = ToastStyle()
                                self.view?.makeToast(msg, duration: 2.0, position: ToastPosition.center, title:"", image:nil, style: style, completion: { (true) in
                                    // Move to Participant details
                                    self.updateBtn.isUserInteractionEnabled = true
                                    self.updateAmendSessionDelegate?.didFinishUpdatingAmendSessions(isUpdated: true)
                                    self.navigationController?.popViewController(animated: true)
                                })
                            }
                        }
                        
                    }
                    else{
                        //Display msg
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                            self.view.makeToast("Could not update", duration: 3.0, position: .center)
                            self.updateBtn.isUserInteractionEnabled = true
                        }
                    }
                }
                else{
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.view.makeToast("Could not update", duration: 3.0, position: .center)
                        self.updateBtn.isUserInteractionEnabled = true
                    }
                }
            }
            else{
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                    self.updateBtn.isUserInteractionEnabled = true
                }
            }
        }
    }
    
    func getSymbol(forCurrencyCode code: String) -> String? {
        let locale = NSLocale(localeIdentifier: code)
        if locale.displayName(forKey: .currencySymbol, value: code) == code {
            let newlocale = NSLocale(localeIdentifier: code.dropLast() + "_en")
            return newlocale.displayName(forKey: .currencySymbol, value: code)
        }
        return locale.displayName(forKey: .currencySymbol, value: code)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension AmendRegistrationViewController: UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.priceFieldList[section].options.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let chooseSessionCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! ChooseSessionCell
        
        let option:Option = self.priceFieldList[indexPath.section].options[indexPath.row]

        chooseSessionCell.titleLbl.text = option.label

        let currencySymbol = getSymbol(forCurrencyCode: "GBP")

        let temp:Double = (Double(option.taxAmount!))!
        let taxAmount = String(format: "%.2f", temp)
        chooseSessionCell.priceLbl.text = currencySymbol! + option.displayAmount! + " (includes VAT of " + currencySymbol! + taxAmount + ")"

        if(option.optionId != nil) {
            chooseSessionCell.selectButton.isSelected = true
        }
        else {
            chooseSessionCell.selectButton.isSelected = false
        }

        chooseSessionCell.radioImgView.image = option.isAmendOptionSelected! ?  UIImage.init(named: "radio_on_blue_icon") : UIImage.init(named: "radio_off_icon")
        
        return chooseSessionCell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let option:Option = self.priceFieldList[indexPath.section].options[indexPath.row]
        
        let cell = tableView.cellForRow(at: indexPath) as! ChooseSessionCell
        
        self.isRequiredFieldSelected = false
        
        for opt in self.priceFieldList[indexPath.section].options {
            if opt == option{
                opt.isAmendOptionSelected = opt.isAmendOptionSelected! ? false : true
            }
            else{
                opt.isAmendOptionSelected = false
            }
        }
        
        cell.radioImgView.image = option.isAmendOptionSelected! ?  UIImage.init(named: "radio_on_blue_icon") : UIImage.init(named: "radio_off_icon")
        
        self.amendSessionTableView.reloadSections([indexPath.section], with: .fade)
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.priceFieldList.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 40))
        view.backgroundColor = UIColor.white
        
        let label = UILabel(frame: CGRect(x: 10, y: 0, width: tableView.frame.size.width - 20, height: 40))
        label.numberOfLines = 0
        label.font = UIFont(name: "Arial", size: 15)
        label.textColor = UIColor(red: 19.0/255.0 , green: 106.0/255.0, blue: 244.0/255.0, alpha: 1.0)
        
        let priceField:PriceField = self.priceFieldList[section]
        label.text = priceField.priceFieldLabel
        
        view.addSubview(label)
        
        return view
        
    }
    
}
