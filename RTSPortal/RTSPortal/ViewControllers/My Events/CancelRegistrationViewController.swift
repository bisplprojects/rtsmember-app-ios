//
//  CancelRegistrationViewController.swift
//  RTSPortal
//
//  Created by sajan on 06/09/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit

class CancelRegistrationViewController: UIViewController,UITextViewDelegate,UIGestureRecognizerDelegate {

    var cancelBtnText:String?
    var cancelText:String?
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var cancellationTextView: UITextView!
    
    // MARK: -

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.cancellationTextView.delegate = self
        self.cancellationTextView.attributedText = self.cancelText?.htmlToAttributedString
        self.cancellationTextView.isUserInteractionEnabled = true
        self.cancellationTextView.isEditable = false
        self.cancellationTextView.isSelectable = true
        self.cancellationTextView.dataDetectorTypes = [.all]
        cancellationTextView.isScrollEnabled = false

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(backViewTapped(_:)))
        tapGestureRecognizer.delegate = self
        self.backView.addGestureRecognizer(tapGestureRecognizer)

    }

    // MARK: - UITextView  Delegate Methods

    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        if UIApplication.shared.canOpenURL(URL) {
            UIApplication.shared.open(URL)
        }
        return false
    }
    
    // MARK: - IBAction Methods

    @IBAction func actionCloseBtnTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - TapGesture Methods
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        // Prevent subviews of a specific view to send touch events to the view's gesture recognizers.
        if let touchedView = touch.view, let gestureView = gestureRecognizer.view, touchedView.isDescendant(of: gestureView), touchedView !== gestureView {
            return false
        }
        return true
    }

    @objc func backViewTapped(_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

