//
//  ForgotPinViewController.swift
//  RTSPortal
//
//  Created by sajan on 08/07/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import SVProgressHUD
import Toast_Swift
import GoogleMaps
import GooglePlaces

class ForgotPinViewController: UIViewController {

    @IBOutlet weak var emailTxtField: UITextField!
    
    @IBOutlet weak var navigationBarLine: UIView!
    
    var emailString : String?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.addShadowToNavigation()
        self.emailTxtField.clearButtonMode = UITextField.ViewMode.whileEditing
    }
   
    // MARK: - IBActions

    @IBAction func actionSubmitBtnTapped(_ sender: Any) {
        self.emailTxtField.resignFirstResponder()
        let valid = validateData()
        if (valid) {
            print("valid fields")
            self.forgotPinApiCallFunc()
        }
    }
    
    @IBAction func actionBackBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func clearData()  {
        self.emailTxtField.text = ""
    }

    // MARK: - Webservice
    
    func forgotPinApiCallFunc(){
        
        SVProgressHUD.show()
        let apiObj = ApiHandler()
        let email = self.emailTxtField.text
        self.emailString = self.emailTxtField.text
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()

        let param = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "entity" : "CiviMobileContact",
                     "action": "forgotpin",
                     "email": email];

        print("Forgot Pin request params : \(param)")
        
        apiObj.postData(url: kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("Forgot pin response : \(json)")
            if status
            {
                let dictonary = json["values"].dictionaryValue
                let msg = dictonary["message"]?.stringValue
                let title = dictonary["title"]?.stringValue

                if dictonary["status"] == true {

                    let otpStr = dictonary["otp"]?.stringValue

                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.clearData()
                        var style = ToastStyle()
                        style.messageColor = UIColor.white
                        style.titleFont.withSize(16)
                        self.view?.makeToast(msg, duration: 3.0, position: ToastPosition.center, title:title, image:nil, style: style, completion: { (true) in
                            //Action
                            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                            let otpVC = storyboard.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                            otpVC.emailAddress = self.emailString
                            otpVC.otpText = otpStr
                            otpVC.isFromForgotPIN = true
                            self.navigationController?.pushViewController(otpVC, animated: true)

                        })
                    }
                }
                else {
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.clearData()
                        var style = ToastStyle()
                        style.messageColor = UIColor.white
                        style.titleFont.withSize(16)
                        self.view?.makeToast(msg, duration: 3.0, position: ToastPosition.center, title:title, image:nil, style: style, completion: { (true) in
                            //Action
                        })
                    }
                }
            }
            else
            {
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                    self.view.makeToast("Error...!!!", duration: 3.0, position: .center)
                }
            }
        }
    }

    // MARK: - Validation
    
    func validateData() -> Bool{
        
        if(self.emailTxtField.text?.trimmingCharacters(in: .whitespaces).isEmpty)!
        {
            self.view.makeToast("Enter email address", duration: 3.0, position: .center)
            return false
        }
        else
        {
            let temp = isEmailValid(self.emailTxtField.text!)
            if (temp == false)
            {
                self.view.makeToast("Enter valid email", duration: 3.0, position: .center)
                return false
            }
        }
        return true
    }
    
    func isEmailValid(_ email : String) -> Bool{
        let emailTest = NSPredicate(format: "SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}")
        return emailTest.evaluate(with: email)
    }

    func addShadowToNavigation() {
        self.navigationBarLine.layer.shadowColor = UIColor.black.cgColor
        self.navigationBarLine.layer.shadowOpacity = 1
        self.navigationBarLine.layer.shadowOffset = .zero
        self.navigationBarLine.layer.shadowRadius = 2
        self.navigationBarLine.layer.shadowPath = UIBezierPath(rect: self.navigationBarLine.bounds).cgPath
        self.navigationBarLine.layer.shouldRasterize = true
        self.navigationBarLine.layer.rasterizationScale = UIScreen.main.scale
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
