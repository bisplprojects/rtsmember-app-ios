//
//  AboutPageViewController.swift
//  RTSPortal
//
//  Created by Rohit Singh on 15/10/2019.
//  Copyright © 2019 BISPL. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD
import Toast_Swift

class AboutPageViewController: UIViewController,UICollectionViewDelegate {
    
    @IBOutlet var navigationBarLine: UIView!
    @IBOutlet var lblAppVersion: UILabel!
    
    @IBOutlet var lblReleaseDate: UILabel!
    
    @IBOutlet var lblInfo: UILabel!
    
    @IBOutlet var navigationBarView: UIView!
    
    @IBOutlet var headerView: UIView!
    @IBOutlet var headerImgView: UIImageView!
    
    @IBOutlet var menuBtn: UIButton!
    override func viewDidLoad() {
        self.addShadowToNavigation()
     
        let appVersionString: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        lblAppVersion.text = "Version "+appVersionString
        lblAppVersion.font = UIFont(name: "Arial", size: 14)
        lblAppVersion.textAlignment = .center
        lblAppVersion.textColor = UIColor.white
        
        
        
       if let executableURL = Bundle.main.executableURL,
           let creation = (try? executableURL.resourceValues(forKeys: [.creationDateKey]))?.creationDate {
           print(creation)
           
           let dateFormatter = DateFormatter()
           dateFormatter.dateFormat = "dd MMM yy"
           let buildDateStr = dateFormatter.string(from: creation)

           lblReleaseDate.text = buildDateStr
           
       }


        showInfo()
        
        

        
    }
    @IBAction func actionMenuBtnTapped(_ sender: Any) {
        UserActionManager.displayLeftMenu()
    }
   
    func addShadowToNavigation() {
        self.navigationBarLine.layer.shadowColor = UIColor.black.cgColor
        self.navigationBarLine.layer.shadowOpacity = 1
        self.navigationBarLine.layer.shadowOffset = .zero
        self.navigationBarLine.layer.shadowRadius = 1
        self.navigationBarLine.layer.shadowPath = UIBezierPath(rect: self.navigationBarLine.bounds).cgPath
        self.navigationBarLine.layer.shouldRasterize = true
        self.navigationBarLine.layer.rasterizationScale = UIScreen.main.scale
        
    }
    
   func showInfo() {
    self.view.endEditing(true)
    SVProgressHUD.show()
    
    let apiObj = ApiHandler()
    var deviceToken = UserDefaults.standard.getDeviceToken()
    if deviceToken == nil
    {
    if let uuid = UIDevice.current.identifierForVendor?.uuidString {
    print(uuid)
    UserDefaults.standard.setDeviceToken(value: uuid)
    }
    }
    deviceToken = UserDefaults.standard.getDeviceToken()
    
    let param = ["deviceId": deviceToken,
                 "appName": "CiviMemberAPP",
                 "entity": "CiviMobileApp",
                 "action": "info"
                 ];
    print("App Info Request params : \(param)")
    
    apiObj.postData(url:kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
    
    print("App Info response : \(json)")
    if status
    {
    let dictonary = json["values"].dictionaryValue
    //let dataDict = dictonary["data"]
    
    if dictonary["status"] == true {
    
        let infoStr = dictonary["info"]?.stringValue
        print("info string: ", infoStr!)
    //let messageStr = dataDict?["message"].stringValue
       // let data = infoStr?.data(using: String.Encoding.unicode)!
        //let attrStr = try? NSAttributedString(
           // data: data!,
           // options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.plain], documentAttributes: nil
       // )
        
        
        self.lblInfo.text = infoStr
       // self.lblInfo.attributedText = attrStr
        self.lblInfo.preferredMaxLayoutWidth = 300
        
        //self.lblInfo.sizeToFit()
        
        self.lblInfo.font = UIFont(name: "Arial", size: 14)
        
        self.lblInfo.textColor = UIColor.white
        self.lblInfo.textAlignment = .center
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }

    }
    else if dictonary["status"] == false{
    //Display msg
   // let messageStr = "Something went wrong. Please contact admin!"
    DispatchQueue.main.async {
    SVProgressHUD.dismiss()
    //self.view.makeToast(messageStr, duration: 3.0, position: .center)
    }
    }
    else {
    //Display msg
   // let messageStr = "Something went wrong. Please contact admin!"
    DispatchQueue.main.async {
    SVProgressHUD.dismiss()
    //self.view.makeToast(messageStr, duration: 3.0, position: .center)
    }
    }
    }
    else
    {
    //Display msg
    DispatchQueue.main.async {
    SVProgressHUD.dismiss()
    }
    
    }
    }
    }
}
