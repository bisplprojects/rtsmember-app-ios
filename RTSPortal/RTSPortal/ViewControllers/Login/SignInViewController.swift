//
//  ViewController.swift
//  RTSPortal
//
//  Created by sajan on 08/07/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import Foundation
import LocalAuthentication
import SVProgressHUD
import Toast_Swift
import Alamofire
import SwiftyJSON
import IQKeyboardManagerSwift

class SignInViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var indicatorView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var signInContainerView: UIView!
    
    @IBOutlet var signInLbl: UIButton!
    @IBOutlet var signUpLbl: UIButton!
    
    var pinTxtField : UITextField!
    var emailTxtField : UITextField!
    var emailOrig : String = ""
    var registeredStatus : String?
    var registeredFlag:Bool?
    
    @IBOutlet weak var logoImgViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var welcomeLblTopConstraint: NSLayoutConstraint!
    
    
    var appOpenCounter : Int = 0
    
//    let context = LAContext()
//    var error: NSError?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let modelName = UIDevice.modelName
        if modelName == "iPhone 5s" {
            self.logoImgViewTopConstraint.constant = 10
            self.welcomeLblTopConstraint.constant = 5
            self.view.layoutIfNeeded()
        }

        UIView.animate(withDuration: 0) {
            self.indicatorView.frame.origin = CGPoint(x: 0, y: 40)
        }
        // Auto Login
        let loggedIn = UserDefaults.standard.isLoggedIn()
        
        if loggedIn == true{
            
            self.autoLogin()
            
        }
            
    }
        
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let hasAlreadyLaunched = isAppAlreadyLaunchedOnce()
         registeredFlag = UserDefaults.standard.hasUserRegistered()
        if hasAlreadyLaunched == true {
            appOpenCounter = 1
            
        }
        //let hasAlreadyLaunched = isFirstLaunch()
        //let timesOfLaunch = getCurrentTimesOfOpenApp()
        print("value of hasAlreadyLaunched: ",hasAlreadyLaunched)
        //print("value of timesOfLaunch: ", timesOfLaunch)
       indicatorView.isHidden=false
       // if hasAlreadyLaunched == true {
        if appOpenCounter == 1 && registeredFlag == true {
        
            //setUpSignInUI()
            setUpUI()
            signInLbl.isHidden=false
            signInLbl.isEnabled=true
            signUpLbl.isHidden = false
            signUpLbl.isEnabled = true
            signInLbl.setTitle("SIGN IN", for: .normal)
            indicatorView.isHidden=false
            
        }
        else{
            indicatorView.isHidden=true
            //signInLbl.isHidden=true
          
           signUpLbl.isHidden = true
           signUpLbl.isEnabled = false
            signInLbl.setTitle("SIGN UP", for: .normal)
            signInLbl.isEnabled=false
        
            setUpSignUpUI()
            appOpenCounter = 1
        }
        //setUpUI()
        var frame: CGRect = self.scrollView.frame
        frame.origin.x = 0
        frame.origin.y = 0
        self.scrollView.scrollRectToVisible(frame, animated: false)
        UIView.animate(withDuration: 0) {
            self.indicatorView.frame.origin = CGPoint(x: 0, y: 40)
        }
        
        
        
        let isTouchIDEnabled = UserDefaults.standard.isTouchIDEnabled()
        if isTouchIDEnabled == true {
            //Display TouchID
            print("Display TouchID")
            let biometicType:BioMetricSupported = visAuthClass.supportedBiometricType()
            if (biometicType == .touchId) {
                self.displayBiometricLogin()
            }
        }

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        IQKeyboardManager.shared.enable = true

    }

    func displayBiometricLogin() {
        
        visAuthClass.isValidUer(reasonString: "Login to your RTS account ") {[unowned self] (isSuccess, stringValue) in
            
            DispatchQueue.main.async{
                if isSuccess
                {
                    print("evaluating...... successfully completed")
                        self.autoLogin()
                }
                else
                {
                    print("evaluating...... failed to recognise user \n reason = \(stringValue?.description ?? "invalid")")
                }

            }
//            if isSuccess
//            {
//                print("evaluating...... successfully completed")
//                    self.autoLogin()
//            }
//            else
//            {
//                print("evaluating...... failed to recognise user \n reason = \(stringValue?.description ?? "invalid")")
////                self.pinTxtField.becomeFirstResponder()
//            }
        }
    }
    
    func bioLogin()  {
//        self.view.endEditing(true)

        let context = LAContext()
        var error: NSError?

        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            let reason = "Identify yourself!"

            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) {
                [unowned self] success, authenticationError in

                DispatchQueue.main.async {
                    if success {
                        self.view.endEditing(true)
                        self.autoLogin()

                    } else {
                        print("invalid")
                    }
                }
            }
        }


    }
    
    /*func setUpSignUpUI(){
        
        self.scrollView.contentSize = CGSize(width: self.signInContainerView.frame.width * 2, height: self.scrollView.frame.height)
        let signUpView = UIView(frame: CGRect(x: 0, y: 0, width: self.signInContainerView.frame.width, height: 190))
        
        let emailTxtFieldView = UIView(frame: CGRect(x: 0, y: 0, width: self.signInContainerView.frame.width, height: 50))
        emailTxtFieldView.backgroundColor = UIColor.white
        signUpView.addSubview(emailTxtFieldView)
        
        let passwordIconImg: UIImage = UIImage(named: "password_icon")!
        let passwordIconImageView = UIImageView(image: passwordIconImg)
        passwordIconImageView.frame = CGRect(x: 20,y: 15,width: 20,height: 20)
        
        emailTxtFieldView.addSubview(passwordIconImageView)
        
        self.emailTxtField =  UITextField(frame: CGRect(x: 65, y: 0, width: signUpView.frame.width - 85, height: 50))
        self.emailTxtField.placeholder = "Email address"
        self.emailTxtField.font = UIFont(name: "Arial", size: 14)
        self.emailTxtField.borderStyle = UITextField.BorderStyle.none
        self.emailTxtField.clearButtonMode = UITextField.ViewMode.whileEditing
        
        self.emailTxtField.delegate = self as? UITextFieldDelegate
        self.emailTxtField.keyboardType = .emailAddress
        emailTxtFieldView.addSubview(self.emailTxtField)
        
        let signUpBtn:UIButton = UIButton(frame: CGRect(x: signUpView.frame.width * 0.5, y: 50, width: signUpView.frame.width * 0.5, height: 50))
        signUpBtn.setTitle("SIGN UP", for: .normal)
        signUpBtn.titleLabel?.font =  UIFont(name: "Arial", size: 14)
        signUpBtn.addTarget(self, action:#selector(self.signUpBtnClicked), for: .touchUpInside)
        let signInBtnImg = UIImage(named: "btnBackground")!
        signUpBtn.setBackgroundImage(signInBtnImg, for: UIControl.State.normal)
        signUpView.addSubview(signUpBtn)
        self.scrollView.addSubview(signUpView)
        
    }*/
    func setUpSignUpUI(){
        
        // Sign Up View
        self.scrollView.contentSize = CGSize(width: self.signInContainerView.frame.width * 2, height: self.scrollView.frame.height)
        
        let signUpView = UIView(frame: CGRect(x: self.signInContainerView.frame.width, y: 0, width: self.signInContainerView.frame.width, height: 100))
        
        let emailTxtFieldView = UIView(frame: CGRect(x: 0, y: 0, width: self.signInContainerView.frame.width, height: 50))
        emailTxtFieldView.backgroundColor = UIColor.white
        signUpView.addSubview(emailTxtFieldView)
        
        let passwordIconImg: UIImage = UIImage(named: "password_icon")!
        let passwordIconImageView = UIImageView(image: passwordIconImg)
        passwordIconImageView.frame = CGRect(x: 20,y: 15,width: 20,height: 20)
        
        emailTxtFieldView.addSubview(passwordIconImageView)
        
        self.emailTxtField =  UITextField(frame: CGRect(x: 65, y: 0, width: signUpView.frame.width - 85, height: 50))
        self.emailTxtField.placeholder = "Email address"
        self.emailTxtField.font = UIFont(name: "Arial", size: 14)
        self.emailTxtField.borderStyle = UITextField.BorderStyle.none
        self.emailTxtField.clearButtonMode = UITextField.ViewMode.whileEditing
        
        self.emailTxtField.delegate = self
        self.emailTxtField.keyboardType = .emailAddress
        emailTxtFieldView.addSubview(self.emailTxtField)
        
        let signUpBtn:UIButton = UIButton(frame: CGRect(x: signUpView.frame.width * 0.5, y: 50, width: signUpView.frame.width * 0.5, height: 50))
        signUpBtn.setTitle("SIGN UP", for: .normal)
        signUpBtn.titleLabel?.font =  UIFont(name: "Arial", size: 14)
        signUpBtn.addTarget(self, action:#selector(self.signUpBtnClicked), for: .touchUpInside)
       let signInBtnImg = UIImage(named: "btnBackground")!
        signUpBtn.setBackgroundImage(signInBtnImg, for: UIControl.State.normal)
        signUpView.addSubview(signUpBtn)
        
        self.scrollView.addSubview(signUpView)
        self.view.endEditing(true)
        var frame: CGRect = self.scrollView.frame
        frame.origin.x = frame.size.width
        frame.origin.y = 0
        self.scrollView.scrollRectToVisible(frame, animated: true)
        UIView.animate(withDuration: 0.5) {
            self.indicatorView.frame.origin = CGPoint(x: self.indicatorView.frame.width, y: 40)
        }
    }
    
    func setUpUI(){
        
        self.scrollView.contentSize = CGSize(width: self.signInContainerView.frame.width * 2, height: self.scrollView.frame.height)
        
        let signInView = UIView(frame: CGRect(x: 0, y: 0, width: self.signInContainerView.frame.width, height: 190))
        
        let pinTxtFieldView = UIView(frame: CGRect(x: 0, y: 0, width: self.signInContainerView.frame.width, height: 50))
        pinTxtFieldView.backgroundColor = UIColor.white
        signInView.addSubview(pinTxtFieldView)

        let userIconImg: UIImage = UIImage(named: "user_icon")!
        let userIconImageView = UIImageView(image: userIconImg)
        userIconImageView.frame = CGRect(x: 20,y: 15,width: 20,height: 20)

        pinTxtFieldView.addSubview(userIconImageView)
        
        self.pinTxtField =  UITextField(frame: CGRect(x: 65, y: 0, width: signInView.frame.width - 85, height: 50))
        self.pinTxtField.placeholder = "PIN"
        self.pinTxtField.font = UIFont(name: "Arial", size: 14)
        self.pinTxtField.borderStyle = UITextField.BorderStyle.none
        self.pinTxtField.clearButtonMode = UITextField.ViewMode.whileEditing
        self.pinTxtField.delegate = self
        self.pinTxtField.keyboardType = .numberPad
        self.pinTxtField.isSecureTextEntry = true
    
        pinTxtFieldView.addSubview(self.pinTxtField)

        let signInBtn:UIButton = UIButton(frame: CGRect(x: signInView.frame.width * 0.5, y: 50, width: signInView.frame.width * 0.5, height: 50))
        signInBtn.setTitle("SIGN IN", for: .normal)
        signInBtn.titleLabel?.font =  UIFont(name: "Arial", size: 14)
        signInBtn.addTarget(self, action:#selector(self.signInBtnClicked), for: .touchUpInside)
        let signInBtnImg = UIImage(named: "btnBackground")!
        signInBtn.setBackgroundImage(signInBtnImg, for: UIControl.State.normal)

        signInView.addSubview(signInBtn)

        let forgotPinBtn:UIButton = UIButton(frame: CGRect(x: 0, y: signInView.frame.height - 60, width: 100, height: 20))
        forgotPinBtn.center.x = signInView.center.x // for horizontal

        forgotPinBtn.setTitle("FORGOT PIN?", for: .normal)
        forgotPinBtn.titleLabel?.font =  UIFont(name: "Arial", size: 12)
        forgotPinBtn.addTarget(self, action:#selector(self.forgotPinBtnClicked), for: .touchUpInside)
        signInView.addSubview(forgotPinBtn)
        
        //link to Enter Verification code
        let enterVerificationCodeBtn:UIButton = UIButton(frame: CGRect(x: 0,   y: signInView.frame.height - 20, width:200, height:20))
        enterVerificationCodeBtn.center.x = signInView.center.x
        enterVerificationCodeBtn.setTitle("ENTER VERIFICATION CODE", for: .normal)
        enterVerificationCodeBtn.titleLabel?.font = UIFont(name: "Arial",size: 13)
        
        signInView.addSubview(enterVerificationCodeBtn)
        
        enterVerificationCodeBtn.addTarget(self, action:#selector(self.enterVerificationCodeBtnClicked), for: .touchUpInside)
        
        //About page link
        let aboutUsBtn:UIButton = UIButton(frame: CGRect(x:0, y:signInView.frame.height - 2, width:100, height:30))
        aboutUsBtn.center.x = signInView.center.x
        aboutUsBtn.setTitle("APP INFO", for: .normal)
        aboutUsBtn.titleLabel?.font = UIFont(name:"Arial", size:12)
        //signInView.addSubview(aboutUsBtn)
        
        aboutUsBtn.addTarget(self, action:#selector(self.appInfoClicked), for: .touchUpInside)
       

        self.scrollView.addSubview(signInView)

        
        // Sign Up View
        
        let signUpView = UIView(frame: CGRect(x: self.signInContainerView.frame.width, y: 0, width: self.signInContainerView.frame.width, height: 100))
        
        let emailTxtFieldView = UIView(frame: CGRect(x: 0, y: 0, width: self.signInContainerView.frame.width, height: 50))
        emailTxtFieldView.backgroundColor = UIColor.white
        signUpView.addSubview(emailTxtFieldView)

        let passwordIconImg: UIImage = UIImage(named: "password_icon")!
        let passwordIconImageView = UIImageView(image: passwordIconImg)
        passwordIconImageView.frame = CGRect(x: 20,y: 15,width: 20,height: 20)
        
        emailTxtFieldView.addSubview(passwordIconImageView)

        self.emailTxtField =  UITextField(frame: CGRect(x: 65, y: 0, width: signUpView.frame.width - 85, height: 50))
        self.emailTxtField.placeholder = "Email address"
        self.emailTxtField.font = UIFont(name: "Arial", size: 14)
        self.emailTxtField.borderStyle = UITextField.BorderStyle.none
        self.emailTxtField.clearButtonMode = UITextField.ViewMode.whileEditing
        
        self.emailTxtField.delegate = self
        self.emailTxtField.keyboardType = .emailAddress
        emailTxtFieldView.addSubview(self.emailTxtField)

        let signUpBtn:UIButton = UIButton(frame: CGRect(x: signUpView.frame.width * 0.5, y: 50, width: signUpView.frame.width * 0.5, height: 50))
        signUpBtn.setTitle("SIGN UP", for: .normal)
        signUpBtn.titleLabel?.font =  UIFont(name: "Arial", size: 14)
        signUpBtn.addTarget(self, action:#selector(self.signUpBtnClicked), for: .touchUpInside)
        signUpBtn.setBackgroundImage(signInBtnImg, for: UIControl.State.normal)
        signUpView.addSubview(signUpBtn)
        
        self.scrollView.addSubview(signUpView)
            
    
    }
    
    // MARK: - IBActions

    @IBAction func actionSignInTabBtnTapped(_ sender: Any) {
        self.view.endEditing(true)
        var frame: CGRect = self.scrollView.frame
        frame.origin.x = 0
        frame.origin.y = 0
        self.scrollView.scrollRectToVisible(frame, animated: true)
        UIView.animate(withDuration: 0.5) {
            self.indicatorView.frame.origin = CGPoint(x: 0, y: 40)
        }
    }
    
    @IBAction func actionSignUpTabBtnTapped(_ sender: Any) {
        self.view.endEditing(true)
        var frame: CGRect = self.scrollView.frame
        frame.origin.x = frame.size.width
        frame.origin.y = 0
        self.scrollView.scrollRectToVisible(frame, animated: true)
        UIView.animate(withDuration: 0.5) {
            self.indicatorView.frame.origin = CGPoint(x: self.indicatorView.frame.width, y: 40)
        }
    }
    
    @objc func signInBtnClicked() {
        let valid = validateSignInCredentials()
        if valid {
            print("Valid Fields")
            self.signInApiCallFunc()
        }
    }

    @objc func signUpBtnClicked() {
        let valid = validateSignUpCredentials()
        if (valid) {
            print("valid fields")
            self.view.endEditing(true)
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let verificationVC = storyboard.instantiateViewController(withIdentifier: "VerificationViewController") as! VerificationViewController
            verificationVC.emailAddress = self.emailTxtField.text
            let defaults = UserDefaults.standard
            
            defaults.set(self.emailTxtField.text, forKey: emailOrig)
            
            
            self.emailTxtField.text = ""
            self.navigationController?.pushViewController(verificationVC, animated: true)

        }
    }
    
    @objc func forgotPinBtnClicked() {
        print("Forgot Pin Button Clicked")
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let forgotPinVC = storyboard.instantiateViewController(withIdentifier: "ForgotPinViewController") as! ForgotPinViewController
        self.navigationController?.pushViewController(forgotPinVC, animated: true)
        
    }
    //Enter verification code link on home page: tweaked on 1st oct 2019
    @objc func enterVerificationCodeBtnClicked(){
        print("verification Button Clicked")
        let storyboard = UIStoryboard(name:"Main", bundle: Bundle.main)
        let verificationCodeVC = storyboard.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
        let defaults = UserDefaults.standard
        let emailString = defaults.string(forKey: emailOrig)
        print("cached email id is: ",  emailString)
        if (emailString == nil){
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                var style = ToastStyle()
                style.messageColor = UIColor.white
                self.view?.makeToast("Please Signup Using Email Id", duration: 3.0, position: ToastPosition.center, title:"Not Signed Up", image:nil, style: style, completion: { (true) in
                })
            }
        }
            else{
       verificationCodeVC.emailAddress = emailString
        self.navigationController?.pushViewController(verificationCodeVC, animated: true)
        }
        
        
    }
    
    @objc func appInfoClicked(){
        let storyboard = UIStoryboard(name:"Main", bundle: Bundle.main)
        let AboutpageVC = storyboard.instantiateViewController(withIdentifier: "AboutPageViewController") as! AboutPageViewController
        self.navigationController?.pushViewController(AboutpageVC, animated: true)
        
    }
    
    // MARK: - UITextField delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
       /* if textField == self.pinTxtField {

            let isTouchIDEnabled = UserDefaults.standard.isTouchIDEnabled()
            if isTouchIDEnabled == true {
                //Display TouchID
                print("Display TouchID")
                let biometicType:BioMetricSupported = visAuthClass.supportedBiometricType()
                if (biometicType == .touchId) {
//                    self.displayBiometricLogin()
                    self.bioLogin()
                }
            }
        }*/
    }
    
    
    // MARK: - Webservice

    func signUpApiCallFunc() {
        self.view.endEditing(true)
        SVProgressHUD.show()
        
        let apiObj = ApiHandler()
        let email = self.emailTxtField.text

        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        
        let param = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "entity" : "CiviMobileContact",
                     "action": "register",
                     "email": email];
        print("Sign Up request params : \(param)")
        
        apiObj.postData(url:kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("Sign Up response : \(json)")
            if status
            {
                let dictonary = json["values"].dictionaryValue

                if dictonary["status"] == true {
                
                    let msg = dictonary["message"]?.stringValue
                    let title = dictonary["title"]?.stringValue

                    DispatchQueue.main.async {
                        print("Sign Up success")
                        SVProgressHUD.dismiss()
                        var style = ToastStyle()
                        style.messageColor = UIColor.white
                        style.titleFont.withSize(16)
                        self.view?.makeToast(msg, duration: 3.0, position: ToastPosition.center, title:title, image:nil, style: style, completion: { (true) in
                            
                        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                        let verificationVC = storyboard.instantiateViewController(withIdentifier: "VerificationViewController") as! VerificationViewController
                        verificationVC.emailAddress = self.emailTxtField.text
                        self.navigationController?.pushViewController(verificationVC, animated: true)

                        })
                    }
                }
                else if dictonary["status"] == false{
                    //Display msg
                    let msg = dictonary["message"]?.stringValue
                    let title = dictonary["title"]?.stringValue

                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        var style = ToastStyle()
                        style.messageColor = UIColor.white
                        self.view?.makeToast(msg, duration: 3.0, position: ToastPosition.center, title:title, image:nil, style: style, completion: { (true) in
                        })
                    }
                }
                else {
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                }
            }
            else
            {
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
        }
    }

    func signInApiCallFunc() {
        self.view.endEditing(true)
        SVProgressHUD.show()
        let apiObj = ApiHandler()
        let pinText = self.pinTxtField.text
        
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        
        // logintype : veda or live
        let token = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "pin": pinText,
                     "logintype": "live"];
        let tokenString = JSONStringify(value: token as! [String : String])
        let param = ["entity" : "CiviMobileContact",
                     "action" : "login",
                     "token" : tokenString];
        print("Sign In request params : \(param)")
        
        apiObj.postData(url:kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("Sign In response : \(json)")
            if status
            {
                let dictonary = json["values"]
                let msgString = dictonary["message"].stringValue

                if dictonary["validuser"] == true {

                    let dataDict = dictonary["data"].dictionaryValue
                    let nameStr = dataDict["name"]?.stringValue
                    var userImageString:String?
                    if (dataDict["image_URL"]?.stringValue != nil || dataDict["image_URL"]?.stringValue != ""){
                        userImageString = dataDict["image_URL"]!.stringValue
                    }
                    else{
                        userImageString = ""
                    }
                    if dataDict["status"] == true{
                        
                        let sessionDetails = dataDict["session"]?.arrayValue
                        if sessionDetails!.count > 0 {
                            
                            let responseDict = sessionDetails?.first
                            let sessionIDStr = responseDict?["session_id"].stringValue
                            let contactIdStr = responseDict?["contact_id"].stringValue
                            let rolesArr = responseDict?["roles"].arrayValue
                            let roleStr = rolesArr?.first?.stringValue
                            
                            let bgsUserDict = ["UserName": nameStr, "ContactId": contactIdStr, "PIN" : self.pinTxtField.text, "sessionId" : sessionIDStr, "role" : roleStr, "ImageString" : userImageString]
                            
                            var userRoles:[String] = []
                            if let roles = responseDict?["roles"].array {
                                for role in roles {
                                    print(role.stringValue)
                                    userRoles.append(role.stringValue)
                                }
                            }

                            UserDefaults.standard.setLoggedIn(value: true)          // Bool
                            
                            UserDefaults.standard.setBGSUserData(dict: bgsUserDict as! [String : String])
                            UserDefaults.standard.setUserRoles(value: userRoles)
                            
                            UserDefaults.standard.setTouchIDEnabled(value: false)
                            
                            DispatchQueue.main.async {
                                print("Sign In success")
                                SVProgressHUD.dismiss()
                                self.pinTxtField.text = ""
                                
                                self.saveFCMTokenAPICallFucn()
                                
                                let storyboard = UIStoryboard(name: "Profile", bundle: Bundle.main)
                                let vc = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                        }
                        else{
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                self.pinTxtField.text = ""
                                self.view.makeToast(msgString, duration: 3.0, position: .center)
                            }
                        }
                    }
                    else{
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                            self.pinTxtField.text = ""
                            self.view.makeToast(msgString, duration: 3.0, position: .center)
                        }
                    }

                }
                else if dictonary["validuser"] == false{
                    //Display msg
                    let errorMsg = "Enter Valid PIN"
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.pinTxtField.text = ""
                        self.view.makeToast(errorMsg, duration: 3.0, position: .center)
                    }
                }
                else {
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                }
            }
            else
            {
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
        }
    }
    
    func autoLogin() {
        self.view.endEditing(true)
        SVProgressHUD.show()
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]

        // logintype : veda or live
        let token = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "pin": dict["PIN"],
                     "logintype": "live"];
        let tokenString = JSONStringify(value: token as! [String : String])
        let param = ["entity" : "CiviMobileContact",
                     "action" : "login",
                     "token" : tokenString];
        print("Sign In request params : \(param)")
        
        apiObj.postData(url:kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("Sign In response : \(json)")
            if status
            {
                
                let dictonary = json["values"]
                let msgString = dictonary["message"].stringValue

                if dictonary["validuser"] == true {
                    let dataDict = dictonary["data"].dictionaryValue
                    let nameStr = dataDict["name"]?.stringValue
                    var userImageString:String?
                    if (dataDict["image_URL"]?.stringValue != nil || dataDict["image_URL"]?.stringValue != ""){
                        userImageString = dataDict["image_URL"]!.stringValue
                    }
                    else{
                        userImageString = ""
                    }

                    if dataDict["status"] == true{
                        
                        let sessionDetails = dataDict["session"]?.arrayValue
                        if sessionDetails!.count > 0 {
                            
                            let responseDict = sessionDetails?.first
                            let sessionIDStr = responseDict?["session_id"].stringValue
                            let contactIdStr = responseDict?["contact_id"].stringValue
                            let rolesArr = responseDict?["roles"].arrayValue
                            let roleStr = rolesArr?.first?.stringValue

                            var userRoles:[String] = []
                            if let roles = responseDict?["roles"].array {
                                for role in roles {
                                    print(role.stringValue)
                                    userRoles.append(role.stringValue)
                                }
                            }

                            let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
                            let pinStr = dict["PIN"]
                            
                            let bgsUserDict = ["UserName": nameStr, "ContactId": contactIdStr, "PIN" : pinStr, "sessionId" : sessionIDStr, "role" : roleStr, "ImageString" : userImageString]
                            UserDefaults.standard.setBGSUserData(dict: bgsUserDict as! [String : String])
                            UserDefaults.standard.setUserRoles(value: userRoles)
                            UserDefaults.standard.setLoggedIn(value: true)
                            UserDefaults.standard.setTouchIDEnabled(value: false)

                            DispatchQueue.main.async {
                                print("Sign In success")
                                SVProgressHUD.dismiss()
                                self.pinTxtField.text = ""
                                
                                self.saveFCMTokenAPICallFucn()
                                
                                let storyboard = UIStoryboard(name: "Profile", bundle: Bundle.main)
                                let vc = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                        }
                        else{
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                self.pinTxtField.text = ""
                                self.view.makeToast(msgString, duration: 3.0, position: .center)
                            }
                        }
                    }
                    else{
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                            self.pinTxtField.text = ""
                            self.view.makeToast(msgString, duration: 3.0, position: .center)
                        }
                    }
                }
                else if dictonary["validuser"] == false{
                    //Display msg
                    let errorMsg = "Enter Valid PIN"
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.pinTxtField.text = ""
                        self.view.makeToast(errorMsg, duration: 3.0, position: .center)
                    }
                }
                else {
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                }
            }
            else
            {
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
        }

    }
    
    
    func saveFCMTokenAPICallFucn()  {
        
        SVProgressHUD.show()
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let FCMToken = UserDefaults.standard.getFCMToken()

        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        
        
        // logintype : veda or live
        let token = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "sessionId": dict["sessionId"],
                     "logintype": "live"];
        let tokenString = JSONStringify(value: token as! [String : String])
        let param = ["entity" : "CiviMobileContact",
                     "action" : "saveregistrationid",
                     "token" : tokenString,
                     "platform" : "iOS",
                     "cid" :  dict["ContactId"],
                     "registration_id" : FCMToken];
        print("FCM Token  params : \(param)")
        
        apiObj.postData(url: kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("FCM Token response : \(json)")
            
            if status
            {
                if json["is_error"] == 0{
                    
                    let dictonary = json["values"]
                    if dictonary["validuser"] == true{
                        
                        let dataDict = dictonary["data"].dictionaryValue
                        let message = dataDict["message"]?.stringValue
                        
                        if dataDict["status"] == true{
                            

                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                //TODO : Add data to modal class
                            }
                        }
                        else{
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                self.view.makeToast(message, duration: 3.0, position: .center)
                            }
                        }
                    }
                    else{
//                        let message = dictonary["data"]["message"].stringValue
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
//                            self.view.makeToast(message, duration: 3.0, position: .center)
                        }
                    }
                }
                else{
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
//                        self.view.makeToast("Could not load Comms Preference", duration: 3.0, position: .center)
                    }
                }
            }
            else{
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
//                    self.view.makeToast("Could not load Comms Preference", duration: 3.0, position: .center)
                }
            }
        }

    }
    
    
    // MARK: -
    
    func isConnectedToInternet() -> Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    func clearData()  {
        self.pinTxtField.text = ""
        self.emailTxtField.text = ""
    }

    // MARK: - Validation
    
    func validateSignUpCredentials() -> Bool{
        
        if(self.emailTxtField.text?.trimmingCharacters(in: .whitespaces).isEmpty)!
        {
            self.view.makeToast("Enter email address", duration: 3.0, position: .center)
            return false
        }
        else
        {
            let temp = isEmailValid(self.emailTxtField.text!)
            if (temp == false)
            {
                self.view.makeToast("Enter valid email", duration: 3.0, position: .center)
                return false
            }
        }
        return true
    }
    
    func validateSignInCredentials() -> Bool{
        
        if(self.pinTxtField.text?.trimmingCharacters(in: .whitespaces).isEmpty)!
        {
            self.view.makeToast("Enter PIN", duration: 3.0, position: .center)
            return false
        }
        else
        {
            let temp = isPinValid(self.pinTxtField.text!)
            if (temp == false)
            {
                self.view.makeToast("Enter valid PIN", duration: 3.0, position: .center)
                return false
            }
        }
        return true
    }

    func isEmailValid(_ email : String) -> Bool{
        let emailTest = NSPredicate(format: "SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}")
        return emailTest.evaluate(with: email)
    }

    func isPinValid(_ pin : String) -> Bool{
        let pinTest = NSPredicate(format: "SELF MATCHES %@", "[0-9]{4,6}")
        return pinTest.evaluate(with: pin)
    }

}
