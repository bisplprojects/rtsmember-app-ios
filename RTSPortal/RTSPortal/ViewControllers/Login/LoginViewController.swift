//
//  LoginViewController.swift
//  RTSPortal
//
//  Created by sajan on 08/07/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import SVProgressHUD
import Toast_Swift
import GoogleMaps

class LoginViewController: UIViewController {

    @IBOutlet weak var pinTxtField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.pinTxtField.clearButtonMode = UITextField.ViewMode.whileEditing
    }
    
    // MARK: - IBActions

    @IBAction func actionLoginBtnTapped(_ sender: Any) {
        
    }
    
    @IBAction func actionBackBtnTapped(_ sender: Any) {

    }
    
    @IBAction func actionLogoutBtnTapped(_ sender: Any) {
        self.logoutApiCallFunc()
    }
    
    func logoutApiCallFunc() {
        SVProgressHUD.show()
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]

        // logintype : veda or live
        let token = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "pin": dict["PIN"],
                     "logintype": "live"];
        let tokenString = JSONStringify(value: token as! [String : String])
        let param = ["entity" : "CiviMobileContact",
                     "action" : "logout",
                     "cid" : dict["ContactId"],
                     "token" : tokenString];
        print("Logout request params : \(param)")
        
        apiObj.postData(url:kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("Logout response : \(json)")
            if status
            {
                let dictonary = json["values"].dictionaryValue
                let dataDict = dictonary["data"]

                if dataDict?["status"] == true {
                    
                    let titleStr = dataDict?["title"].stringValue
                    let messageStr = dataDict?["message"].stringValue

                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        var style = ToastStyle()
                        style.messageColor = UIColor.white
                        self.view?.makeToast(messageStr, duration: 3.0, position: ToastPosition.center, title:titleStr, image:nil, style: style, completion: { (true) in
                            self.navigationController?.popToRootViewController(animated: true)
                        })
                    }
                }
                else if dataDict?["status"] == false{
                    //Display msg
                    let messageStr = "Something went wrong. Please contact admin!"
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.view.makeToast(messageStr, duration: 3.0, position: .center)
                    }
                }
                else {
                    //Display msg
                    let messageStr = "Something went wrong. Please contact admin!"
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.view.makeToast(messageStr, duration: 3.0, position: .center)
                    }
                }
            }
            else
            {
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
                
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
