//
//  OptionScreenViewController.swift
//  RTSPortal
//
//  Created by Rohit Singh on 14/10/2019.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import SVProgressHUD
import Toast_Swift


class OptionScreenViewController: UIViewController {

    
    @IBOutlet var displayMessage: UILabel!
    @IBOutlet var titleMessage: UILabel!
    var currentVC:UIViewController?
    var emailAddress:String?
    var phone:String?
    
    
    @IBOutlet var signInBtn: UIButton!
    
    var labelTitle: String?
    var display: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        
        let data = display?.data(using: String.Encoding.unicode)!
        let attrStr = try? NSAttributedString(
            data: data!,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil
        )
        
        
        //displayMessage.text = display
        displayMessage.attributedText = attrStr
        displayMessage.preferredMaxLayoutWidth = 300
        
       displayMessage.sizeToFit()
        
        titleMessage.text = labelTitle
        
        
        displayMessage.font = UIFont(name: "Arial", size: 13)
        displayMessage.textAlignment = .left
        displayMessage.clipsToBounds = true
        
       // displayMessage.textColor = UIColor(red: 120.0/255.0 , green: 106.0/255.0, blue: 19.0/255.0, alpha: 1.0)
        displayMessage.textColor = UIColor.white
        displayMessage.numberOfLines = 0
    
        
        titleMessage.font = UIFont(name: "Arial", size:20)
        titleMessage.textAlignment = .center
        //titleMessage.textColor = UIColor(red: 19.0/255.0 , green: 106.0/255.0, blue: 19.0/255.0, alpha: 1.0)
        titleMessage.textColor = UIColor.white
        
       // self.addShadowToNavigation()
        
    }
    
    
    @IBOutlet var resendVerificationClicked: UIButton!
    
    @IBAction func verification(_ sender: Any) {
        self.resendOTPApiCallFunc()
        
        
        
    }
    
    
    @IBAction func backBtnClicked(_ sender: Any) {
       print("back button clicked")
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func signInBtnClicked(_ sender: Any) {
        //                            Move to Login Screen
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func resendOTPApiCallFunc() {
        self.view.endEditing(true)
        SVProgressHUD.show()
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let param = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "entity": "CiviMobileContact",
                     "action": "resendotp",
                     "email": self.emailAddress!,
                     "phone": self.phone,
                     "otp": ""];
        
        apiObj.postData(url:kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("Verification response : \(json)")
            if status
            {
                let dictonary = json["values"].dictionaryValue
                if dictonary["status"] == true {
                    let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                    
                    
                   // let otpStr = dictonary["otp"]?.stringValue
                   // self.otpText = otpStr
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        print("Verification success")
                        //let msg = "PIN sent"
                        let msg = dictonary["message"]?.stringValue
                        self.view.makeToast(msg, duration: 3.0, position: .center)
                    }
                    let otpVC = storyboard.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                   otpVC.emailAddress = self.emailAddress
                    self.navigationController?.pushViewController(otpVC, animated: true)
                }
                else if dictonary["status"] == false{
                    //Display msg
                    let title = dictonary["title"]?.stringValue
                    let errorMsg = dictonary["message"]?.stringValue
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        var style = ToastStyle()
                        style.messageColor = UIColor.white
                        self.view?.makeToast(errorMsg, duration: 3.0, position: ToastPosition.center, title:title, image:nil, style: style, completion: { (true) in
                        })
                    }
                }
                else {
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                }
            }
            else
            {
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
                
            }
        }
    }
    
   

}
