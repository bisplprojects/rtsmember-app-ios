//
//  PinTextField.swift
//  BGSPortal
//
//  Created by sajan on 29/11/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit

protocol MyTextFieldDelegate {
    func textFieldDidDelete(textField: UITextField)
}

class PinTextField: UITextField {
    
    var myDelegate: MyTextFieldDelegate?

    override func deleteBackward() {
        super.deleteBackward()
        myDelegate?.textFieldDidDelete(textField: self)
    }


    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
