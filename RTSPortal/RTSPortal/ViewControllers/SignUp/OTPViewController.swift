//
//  OTPViewController.swift
//  RTSPortal
//
//  Created by sajan on 10/07/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import SVProgressHUD
import Toast_Swift

class OTPViewController: UIViewController {

    var emailAddress : String?
    var phoneNumber : String?
    var otpText : String?
    
    var isFromForgotPIN : Bool?

    
    @IBOutlet weak var navigationBarLine: UIView!
    @IBOutlet weak var otpTxtField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.addShadowToNavigation()
        self.otpTxtField.clearButtonMode = UITextField.ViewMode.whileEditing
    }
    
    // MARK: - IBActions

    @IBAction func actionVerifyBtnTapped(_ sender: Any) {
        self.view.endEditing(true)
        
       /* //let valid = validateOTP()
        //if valid {
           // print("Valid OTP")
            self.signUpApiCallFunc()
        //}*/
        
        let valid = validateOTP()
        if valid {
            print("Valid OTP")
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let createPinVC = storyboard.instantiateViewController(withIdentifier: "CreatePINViewController") as! CreatePINViewController
            createPinVC.emailAddress = self.emailAddress
            createPinVC.otpText = self.otpTxtField.text
            createPinVC.isFromForgotPIN = self.isFromForgotPIN
            self.navigationController?.pushViewController(createPinVC, animated: true)
        }

    }
    
    @IBAction func actionResendVerificationBtnTapped(_ sender: Any) {
        self.otpTxtField.text = ""
        self.resendOTPApiCallFunc()
    }
    
    @IBAction func actionBackBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Webservice
    
    func resendOTPApiCallFunc() {
        self.view.endEditing(true)
        SVProgressHUD.show()
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let param = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "entity": "CiviMobileContact",
                     "action": "resendotp",
                     "email": self.emailAddress!,
                     "phone": self.phoneNumber,
            "otp": self.otpText];
        
        apiObj.postData(url:kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("Verification response : \(json)")
            if status
            {
                let dictonary = json["values"].dictionaryValue
                if dictonary["status"] == true {
                    
                    let otpStr = dictonary["otp"]?.stringValue
                    self.otpText = otpStr
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        print("Verification success")
                        //let msg = "PIN sent"
                        let msg = dictonary["message"]?.stringValue
                        self.view.makeToast(msg, duration: 3.0, position: .center)
                    }
                }
                else if dictonary["status"] == false{
                    //Display msg
                    let title = dictonary["title"]?.stringValue
                    let errorMsg = dictonary["message"]?.stringValue
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        var style = ToastStyle()
                        style.messageColor = UIColor.white
                        self.view?.makeToast(errorMsg, duration: 3.0, position: ToastPosition.center, title:title, image:nil, style: style, completion: { (true) in
                        })
                    }
                }
                else {
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                }
            }
            else
            {
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
                
            }
        }
    }

    func signUpApiCallFunc() {
        SVProgressHUD.show()
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        
        let param = ["deviceId": deviceToken!,
                     "appName": "CiviMemberAPP",
                     "entity" : "CiviMobileContact",
                     "action": "register",
                     "email": self.emailAddress,
                     "otp" : self.otpTxtField.text!] as [String : Any];
        print("Sign Up request params : \(param)")
        
        apiObj.postData(url:kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("Sign Up response : \(json)")
            if status
            {
                let dictonary = json["values"].dictionaryValue
                
                if dictonary["status"] == true {
                    
                    let msg = dictonary["message"]?.stringValue
                    let title = dictonary["title"]?.stringValue
                    
                    UserDefaults.standard.setTouchIDEnabled(value: false)

                    DispatchQueue.main.async {
                        print("Sign Up success")
                        SVProgressHUD.dismiss()
                        var style = ToastStyle()
                        style.messageColor = UIColor.white
                        style.titleFont.withSize(16)
                        self.view?.makeToast(msg, duration: 3.0, position: ToastPosition.center, title:title, image:nil, style: style, completion: { (true) in
                            
                            //                            Move to Login Screen
                            self.navigationController?.popToRootViewController(animated: true)
                            
                        })
                    }
                    
                }
                else if dictonary["status"] == false{
                    //Display msg
                    let msg = dictonary["message"]?.stringValue
                    let title = dictonary["title"]?.stringValue
                    
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        var style = ToastStyle()
                        style.messageColor = UIColor.white
                        self.view?.makeToast(msg, duration: 3.0, position: ToastPosition.center, title:title, image:nil, style: style, completion: { (true) in
                        })
                    }
                }
                else {
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                }
            }
            else
            {
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
        }
    }

    func validateOTP() -> Bool{
        
        if(self.otpTxtField.text?.trimmingCharacters(in: .whitespaces).isEmpty)!
        {
            self.view.makeToast("Enter 6-digit code", duration: 3.0, position: .center)
            return false
        }
        else
        {
            let temp = isPinValid(self.otpTxtField.text!)
            if (temp == false)
            {
                self.view.makeToast("Enter valid 6-digit code", duration: 3.0, position: .center)
                return false
            }
        }
        if(self.otpTxtField.text != self.otpText)
        {
            self.view.makeToast("Enter valid OTP", duration: 3.0, position: .center)
            return false
        }

        return true
    }
    
    func isPinValid(_ pin : String) -> Bool{
        let pinTest = NSPredicate(format: "SELF MATCHES %@", "[0-9]{6,6}")
        return pinTest.evaluate(with: pin)
    }
    
    func addShadowToNavigation() {
        self.navigationBarLine.layer.shadowColor = UIColor.black.cgColor
        self.navigationBarLine.layer.shadowOpacity = 1
        self.navigationBarLine.layer.shadowOffset = .zero
        self.navigationBarLine.layer.shadowRadius = 2
        self.navigationBarLine.layer.shadowPath = UIBezierPath(rect: self.navigationBarLine.bounds).cgPath
        self.navigationBarLine.layer.shouldRasterize = true
        self.navigationBarLine.layer.rasterizationScale = UIScreen.main.scale
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
