//
//  QRScanViewController.swift
//  RTSPortal
//
//  Created by sajan on 17/09/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import AVFoundation
import Foundation
import SVProgressHUD
import Toast_Swift

class QRScanViewController: UIViewController,AVCaptureMetadataOutputObjectsDelegate {

    @IBOutlet weak var scannerView: UIView!
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!

    var participantId:String?
    var candidateId:String?
    var tId:String?
    var resetId:String?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        view.backgroundColor = UIColor.black
        captureSession = AVCaptureSession()
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]//.ean8, .ean13, .pdf417,
        } else {
            failed()
            return
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = self.scannerView.bounds
        
        previewLayer.videoGravity = .resizeAspectFill
        self.scannerView.layer.addSublayer(previewLayer)
        
        captureSession.startRunning()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        previewLayer.frame = self.scannerView.bounds
        self.view.layoutIfNeeded()
    }

    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()
        
//        let systemSoundID: SystemSoundID = 1004//1106
        

        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            //for vibrate
            //kSystemSoundID_Vibrate
            found(code: stringValue)
        }
        
        dismiss(animated: true)
    }
    
    func found(code: String) {
        print(code)
        
        if let endIndex = code.range(of: "tid=")?.upperBound {
            print(code[endIndex...])
            self.tId = String(code[endIndex...])
            
            if let result1 = code.range(of: "&pid") {
                if let result2 = code.range(of: "cid=") {
                    let startIndex = result2.upperBound
                    let endIndex = result1.lowerBound
                    print(String(code[startIndex..<endIndex]))
                    self.candidateId = String(code[startIndex..<endIndex])
                }
            }

            if let result1 = code.range(of: "&tid") {
                if let result2 = code.range(of: "pid=") {
                    let startIndex = result2.upperBound
                    let endIndex = result1.lowerBound
                    print(String(code[startIndex..<endIndex]))
                    self.participantId = String(code[startIndex..<endIndex])
                }
            }

            if let result1 = code.range(of: "&cid") {
                if let result2 = code.range(of: "reset=") {
                    let startIndex = result2.upperBound
                    let endIndex = result1.lowerBound
                    print(String(code[startIndex..<endIndex]))
                    self.resetId = String(code[startIndex..<endIndex])
                }
            }

            if self.participantId != nil{
                self.moveToParticipantDetailsViewController()
            }
        }
        else if let endIndex = code.range(of: "pid=")?.upperBound{
            print(code[endIndex...])
            self.participantId = String(code[endIndex...])

            if let result1 = code.range(of: "&pid") {
                if let result2 = code.range(of: "cid=") {
                    let startIndex = result2.upperBound
                    let endIndex = result1.lowerBound
                    print(String(code[startIndex..<endIndex]))
                    self.candidateId = String(code[startIndex..<endIndex])
                }
            }
            if let result1 = code.range(of: "&cid") {
                if let result2 = code.range(of: "reset=") {
                    let startIndex = result2.upperBound
                    let endIndex = result1.lowerBound
                    print(String(code[startIndex..<endIndex]))
                    self.resetId = String(code[startIndex..<endIndex])
                }
            }

            if self.participantId != nil{
                self.moveToParticipantDetailsViewController()
            }

        }
        else{
            
            if let path = Bundle.main.path(forResource: "beep", ofType: "mp3") {
                let url = URL(fileURLWithPath: path) as CFURL
                var soundID: SystemSoundID = 0
                AudioServicesCreateSystemSoundID(url, &soundID)
                AudioServicesPlaySystemSound(soundID)
            } else {
                print("file not found!")
            }


            let style = ToastStyle()
            self.view?.makeToast("Invalid QR Code", duration: 2.0, position: ToastPosition.center, title:"", image:nil, style: style, completion: { (true) in
                if (self.captureSession?.isRunning == false) {
                    self.captureSession.startRunning()
                }
            })
        }
    }

    // MARK: - IBActions

    @IBAction func actionBackBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func moveToParticipantStatusViewController()  {
        let statusVC = UIViewController.participantStatusViewController() as! ParticipantStatusViewController
        statusVC.participantID = self.participantId
        self.navigationController?.pushViewController(statusVC, animated: true)
    }
    
    func moveToParticipantDetailsViewController()  {
        let detailVC = UIViewController.participantDetailsViewController() as! ParticipantDetailsViewController
        detailVC.participantID = self.participantId
        detailVC.tID = self.tId
        detailVC.resetID = self.resetId
        detailVC.isLoadedFromScan = true
        self.navigationController?.pushViewController(detailVC, animated: true)
    }

    // MARK: - Webservice
    
    func qrCodeScanApiCallFunc() {
        
        SVProgressHUD.show()
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        
        let token = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "sessionId": dict["sessionId"],
                     "logintype": "live"];
        let tokenString = JSONStringify(value: token as! [String : String])
        
        let param = ["entity" : "CiviMobileParticipant",
                     "action" : "get",
                     "token" : tokenString,
                     "pid" :  self.participantId,
                     "scope" : "scan"];
        
        print("Update My Details request params : \(param)")
        
        apiObj.postData(url: kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("Update My Details response : \(json)")
            if status
            {
                if json["is_error"] == 0{
                    let dictonary = json["values"]
                    
                    if dictonary["validuser"] == true{
                        
                        let dataDictonary = dictonary["data"]
                        
                        let msg = dataDictonary["message"].stringValue
                        if dataDictonary["status"] == true{
                            
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                //  Display toast msg
                                self.view.makeToast(msg, duration: 3.0, position: .center)
                            }
                        }
                        else{
                            //Display msg
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                self.view.makeToast(msg, duration: 3.0, position: .center)
                            }
                        }
                    }
                    else{
                        //Display msg
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                        }
                    }
                }
                else{
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                }
            }
            else{
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
