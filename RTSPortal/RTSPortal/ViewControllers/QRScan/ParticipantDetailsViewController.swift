//
//  ParticipantDetailsViewController.swift
//  RTSPortal
//
//  Created by sajan on 18/09/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import SVProgressHUD
import Toast_Swift

protocol PaticipantStatusDelegate {
    func didFinishUpdatingPaticipantStatus(participantDetails:Paticipant?)
}

protocol PaticipantLookupDelegate {
    func didClickOnFindButton(eventID:String?)
}

class ParticipantDetailsViewController: UIViewController {

    var paticipantStatusDelegate: PaticipantStatusDelegate?
    var participantLookupDelegate: PaticipantLookupDelegate?

    @IBOutlet weak var eventTitleLbl: UILabel!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var paticipantImageView: UIImageView!
    @IBOutlet weak var participantNameLbl: UILabel!
    @IBOutlet weak var participantRoleLbl: UILabel!
    @IBOutlet weak var statusTitleLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var regDateTitleLbl: UILabel!
    @IBOutlet weak var regDateLbl: UILabel!
    @IBOutlet weak var lastAttendTitleLbl: UILabel!
    @IBOutlet weak var lastAttendLbl: UILabel!
    @IBOutlet weak var markBtn: UIButton!
    @IBOutlet weak var scanBtn: UIButton!
    @IBOutlet weak var findBtn: UIButton!
    @IBOutlet weak var participantStatusLbl: UILabel!
    
    @IBOutlet weak var paticipantStatusLblContraintHeight: NSLayoutConstraint!
    
    var currentVC:UIViewController?
    var participantID:String?
    var tID:String?
    var resetID:String?
    var participantDetails : Paticipant?
    var participantStatus : Paticipant?
    var participantTicketDetails : Paticipant?
    var isTicketedEvent:Bool?
    
    var status:String?

    var isLoadedFromScan : Bool?

    // MARK: -

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.currentVC = self
        
        self.headerView.layer.cornerRadius = 5
        self.headerView.layer.shadowColor = UIColor.gray.cgColor
        self.headerView.layer.shadowOpacity = 0.3
        self.headerView.layer.shadowOffset = .zero //CGSize(width: 1, height: 1)
        self.headerView.layer.shadowRadius = 4

        self.hideUI()
        if self.participantID != nil{
            if self.isLoadedFromScan == true {
                self.paticipantStatusLblContraintHeight.constant = 50
                self.view.layoutIfNeeded()
                self.getParticipantStatusDetailsFromScanApiCallFunc()
            }
            else{
                self.paticipantStatusLblContraintHeight.constant = 0
                self.view.layoutIfNeeded()
                self.getParticipantStatusDetailsFromFindApiCallFunc()
            }
        }else{
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                self.view!.makeToast("There is some issue with the Participant ID", duration: 3.0, position: .center)
//                self.moveToQRScanViewController()
            }
        }

    }
    
    func hideUI()  {
        self.eventTitleLbl.text = ""
        self.participantNameLbl.text = ""
        self.participantRoleLbl.text = ""
        self.statusLbl.text = ""
        self.participantStatusLbl.text = ""
        self.regDateLbl.text = ""
        self.lastAttendLbl.text = ""
        self.headerView.isHidden = true
        self.statusTitleLbl.isHidden = true
        self.regDateTitleLbl.isHidden = true
        self.lastAttendTitleLbl.isHidden = true
        self.markBtn.isHidden = true
        self.scanBtn.isHidden = true
        self.findBtn.isHidden = true

        
    }
    
    func showUI()  {
        self.headerView.isHidden = false
        self.statusTitleLbl.isHidden = false
        self.regDateTitleLbl.isHidden = false
        self.lastAttendTitleLbl.isHidden = false
        self.markBtn.isHidden = false
        self.scanBtn.isHidden = false
        self.findBtn.isHidden = false

    }

    func updateUI()  {
        
        if self.isTicketedEvent == true {
            
            if (self.participantDetails?.participantDisplayName != nil){
                self.paticipantImageView.sd_setImage(with: URL(string:self.participantDetails!.participantImageURL!), placeholderImage: UIImage(named: ""))
            }
            paticipantImageView?.contentMode = .scaleAspectFill
            paticipantImageView?.layer.cornerRadius = (paticipantImageView?.frame.size.width)! * 0.5
            paticipantImageView?.layer.borderWidth = 0.1
            paticipantImageView?.layer.masksToBounds = false
            paticipantImageView?.layer.borderColor = UIColor(red: 28.0/255.0 , green: 20.0/255.0, blue: 50.0/255.0, alpha: 0.1).cgColor
            paticipantImageView?.clipsToBounds = true

            self.eventTitleLbl.text = self.participantDetails?.eventTitle
//            self.statusLbl.text = self.participantDetails?.status

            self.participantStatusLbl.text = self.participantDetails?.message
            self.participantStatusLbl.layer.borderWidth = 2
            self.participantStatusLbl.layer.cornerRadius = 5

            switch self.participantDetails?.statusClass {
            case "success":
                //green
                self.statusLbl.textColor = UIColor(red: 77.0/255.0, green: 120.0/255.0, blue: 39.0/255.0, alpha: 1.0)
                self.lastAttendLbl.textColor = UIColor(red: 77.0/255.0, green: 120.0/255.0, blue: 39.0/255.0, alpha: 1.0)
                
                self.participantStatusLbl.textColor = UIColor(red: 77.0/255.0, green: 120.0/255.0, blue: 39.0/255.0, alpha: 1.0)
                self.participantStatusLbl.layer.borderColor =  UIColor(red: 77.0/255.0, green: 120.0/255.0, blue: 39.0/255.0, alpha: 1.0).cgColor

                break
            case "warning":
                //amber
                self.statusLbl.textColor = UIColor(red: 225.0/255.0, green: 152.0/255.0, blue: 48.0/255.0, alpha: 1.0)
                self.lastAttendLbl.textColor = UIColor(red: 225.0/255.0, green: 152.0/255.0, blue: 48.0/255.0, alpha: 1.0)
                
                self.participantStatusLbl.textColor = UIColor(red: 225.0/255.0, green: 152.0/255.0, blue: 48.0/255.0, alpha: 1.0)
                self.participantStatusLbl.layer.borderColor = UIColor(red: 225.0/255.0, green: 152.0/255.0, blue: 48.0/255.0, alpha: 1.0).cgColor

                break
            case "alert":
                //red
                self.statusLbl.textColor = UIColor(red: 199.0/255.0, green: 72.0/255.0, blue: 59.0/255.0, alpha: 1.0)
                self.lastAttendLbl.textColor = UIColor(red: 199.0/255.0, green: 72.0/255.0, blue: 59.0/255.0, alpha: 1.0)
                
                self.participantStatusLbl.textColor = UIColor(red: 199.0/255.0, green: 72.0/255.0, blue: 59.0/255.0, alpha: 1.0)
                self.participantStatusLbl.layer.borderColor = UIColor(red: 199.0/255.0, green: 72.0/255.0, blue: 59.0/255.0, alpha: 1.0).cgColor

                break
            default:
                break
            }
            
            let regDate = self.participantDetails?.registerDate?.toDate()!
            let regDateString = regDate?.toString()
            self.regDateLbl.text = regDateString

            
            if self.participantDetails?.message != "" {
                self.paticipantStatusLblContraintHeight.constant = 50
                self.view.layoutIfNeeded()
            }
            else{
                self.paticipantStatusLblContraintHeight.constant = 0
                self.view.layoutIfNeeded()
            }
            
            
            if let ticketDetails = self.participantTicketDetails{
                self.statusLbl.text = ticketDetails.status
                self.participantNameLbl.text = ticketDetails.ticketDisplayName
                self.participantRoleLbl.text = ticketDetails.ticketNumber
                if let lastDate = ticketDetails.scannedTime?.toDate(){
                    let lastDateString = lastDate.toString()
                    self.lastAttendLbl.text = lastDateString
                }
                if (ticketDetails.isScanned == "1") {
                    self.status = "unattended"
                    self.markBtn.setTitle("MARK AS UNATTENDED", for: .normal)
                    self.markBtn.setBackgroundImage(UIImage(imageLiteralResourceName: "orange_btn_background_icon"), for: UIControl.State.normal)
                }
                else if(ticketDetails.isScanned == "0"){
                    self.status = "attended"
                    self.markBtn.setTitle("MARK AS ATTENDED", for: .normal)
                    self.markBtn.setBackgroundImage(UIImage(imageLiteralResourceName: "blue_btn_background_icon"), for: UIControl.State.normal)
                }

            }

        }
        else{
            if (self.participantDetails?.participantDisplayName != nil){
                self.paticipantImageView.sd_setImage(with: URL(string:self.participantDetails!.participantImageURL!), placeholderImage: UIImage(named: ""))
            }
            paticipantImageView?.contentMode = .scaleAspectFill
            paticipantImageView?.layer.cornerRadius = (paticipantImageView?.frame.size.width)! * 0.5
            paticipantImageView?.layer.borderWidth = 0.1
            paticipantImageView?.layer.masksToBounds = false
            paticipantImageView?.layer.borderColor = UIColor(red: 28.0/255.0 , green: 20.0/255.0, blue: 50.0/255.0, alpha: 0.1).cgColor
            paticipantImageView?.clipsToBounds = true

            self.eventTitleLbl.text = self.participantDetails?.eventTitle
            self.participantNameLbl.text = self.participantDetails?.participantDisplayName
            self.participantRoleLbl.text = self.participantDetails?.participantRole
            self.statusLbl.text = self.participantDetails?.status
            
            self.participantStatusLbl.text = self.participantDetails?.message
            self.participantStatusLbl.layer.borderWidth = 2
            self.participantStatusLbl.layer.cornerRadius = 5

            switch self.participantDetails?.statusClass {
            case "success":
                //green
                self.statusLbl.textColor = UIColor(red: 77.0/255.0, green: 120.0/255.0, blue: 39.0/255.0, alpha: 1.0)
                self.lastAttendLbl.textColor = UIColor(red: 77.0/255.0, green: 120.0/255.0, blue: 39.0/255.0, alpha: 1.0)
                
                self.participantStatusLbl.textColor = UIColor(red: 77.0/255.0, green: 120.0/255.0, blue: 39.0/255.0, alpha: 1.0)
                self.participantStatusLbl.layer.borderColor =  UIColor(red: 77.0/255.0, green: 120.0/255.0, blue: 39.0/255.0, alpha: 1.0).cgColor

                break
            case "warning":
                //amber
                self.statusLbl.textColor = UIColor(red: 225.0/255.0, green: 152.0/255.0, blue: 48.0/255.0, alpha: 1.0)
                self.lastAttendLbl.textColor = UIColor(red: 225.0/255.0, green: 152.0/255.0, blue: 48.0/255.0, alpha: 1.0)
                
                self.participantStatusLbl.textColor = UIColor(red: 225.0/255.0, green: 152.0/255.0, blue: 48.0/255.0, alpha: 1.0)
                self.participantStatusLbl.layer.borderColor = UIColor(red: 225.0/255.0, green: 152.0/255.0, blue: 48.0/255.0, alpha: 1.0).cgColor

                break
            case "alert":
                //red
                self.statusLbl.textColor = UIColor(red: 199.0/255.0, green: 72.0/255.0, blue: 59.0/255.0, alpha: 1.0)
                self.lastAttendLbl.textColor = UIColor(red: 199.0/255.0, green: 72.0/255.0, blue: 59.0/255.0, alpha: 1.0)
                
                self.participantStatusLbl.textColor = UIColor(red: 199.0/255.0, green: 72.0/255.0, blue: 59.0/255.0, alpha: 1.0)
                self.participantStatusLbl.layer.borderColor = UIColor(red: 199.0/255.0, green: 72.0/255.0, blue: 59.0/255.0, alpha: 1.0).cgColor

                break
            default:
                break
            }
            
            let regDate = self.participantDetails?.registerDate?.toDate()!
            let regDateString = regDate?.toString()
            self.regDateLbl.text = regDateString
            if let lastDate = self.participantDetails?.lastAttendedDate?.toDate(){
                let lastDateString = lastDate.toString()
                self.lastAttendLbl.text = lastDateString
            }

            if (self.participantDetails?.isAttended == "1") {
                self.status = "unattended"
                self.markBtn.setTitle("MARK AS UNATTENDED", for: .normal)
                self.markBtn.setBackgroundImage(UIImage(imageLiteralResourceName: "orange_btn_background_icon"), for: UIControl.State.normal)
            }
            else if(self.participantDetails?.isAttended == "0"){
                self.status = "attended"
                self.markBtn.setTitle("MARK AS ATTENDED", for: .normal)
                self.markBtn.setBackgroundImage(UIImage(imageLiteralResourceName: "blue_btn_background_icon"), for: UIControl.State.normal)
            }
            
            if self.participantDetails?.message != "" {
                self.paticipantStatusLblContraintHeight.constant = 50
                self.view.layoutIfNeeded()
            }
            else{
                self.paticipantStatusLblContraintHeight.constant = 0
                self.view.layoutIfNeeded()
            }

        }
        
    }

    // MARK: - IBActions
    
    @IBAction func actionBackBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        self.participantLookupDelegate?.didClickOnFindButton(eventID: self.participantDetails?.eventID)

    }
    
    @IBAction func actionScanBtnTapped(_ sender: Any) {
        self.moveToQRScanViewController()

    }
    
    @IBAction func actionFindBtnTapped(_ sender: Any) {
        self.moveToFindViewController()
    }
    
    @IBAction func actionMarkBtnTapped(_ sender: Any) {
        
        let alert = UIAlertController(title: "Status Update", message: "Are you sure you want to update the status?", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            self.updateParticipantStatusApiCallFunc()
        }))
        alert.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)

    }
    
    func moveToQRScanViewController(){
      let viewControllers = ( self.currentVC?.navigationController?.viewControllers)!
 
      if (viewControllers[1].isKind(of: DashboardViewController.self)){
          
          if (viewControllers[2].isKind(of: QRScanViewController.self)){
          self.currentVC?.navigationController?.setViewControllers([viewControllers[0],viewControllers[1],viewControllers[2]], animated: true)
          }
          else{
              let qrScanVC = UIViewController.qrScanViewController() as! QRScanViewController
              self.currentVC?.navigationController?.setViewControllers([viewControllers[0],viewControllers[1],qrScanVC], animated: true)
          }
      
      }else{
          
          if (viewControllers[3].isKind(of: QRScanViewController.self)){
          self.currentVC?.navigationController?.setViewControllers([viewControllers[0],viewControllers[1],viewControllers[2],viewControllers[3]], animated: true)
          }
          else{
              let qrScanVC = UIViewController.qrScanViewController() as! QRScanViewController
              self.currentVC?.navigationController?.setViewControllers([viewControllers[0],viewControllers[1],viewControllers[2],qrScanVC], animated: true)
          }
      }
    }

    func moveToFindViewController() {
       let viewControllers = ( self.currentVC?.navigationController?.viewControllers)!
  
       if (viewControllers[1].isKind(of: DashboardViewController.self)){
           
           if (viewControllers[2].isKind(of: StatusUpdateListViewController.self)){
           self.currentVC?.navigationController?.setViewControllers([viewControllers[0],viewControllers[1],viewControllers[2]], animated: true)
             self.participantLookupDelegate?.didClickOnFindButton(eventID: self.participantDetails?.eventID)

           }
           else{
               let findListVC = UIViewController.statusUpdateListViewController() as! StatusUpdateListViewController
               findListVC.eventID = self.participantDetails?.eventID
             self.currentVC?.navigationController?.setViewControllers([viewControllers[0],viewControllers[1],findListVC], animated: true)
           }
       
       }else{
           
           if (viewControllers[3].isKind(of: StatusUpdateListViewController.self)){
           self.currentVC?.navigationController?.setViewControllers([viewControllers[0],viewControllers[1],viewControllers[2],viewControllers[3]], animated: true)
             self.participantLookupDelegate?.didClickOnFindButton(eventID: self.participantDetails?.eventID)

           }
           else{
               let findListVC = UIViewController.statusUpdateListViewController() as! StatusUpdateListViewController
             findListVC.eventID = self.participantDetails?.eventID
         self.currentVC?.navigationController?.setViewControllers([viewControllers[0],viewControllers[1],viewControllers[2],findListVC], animated: true)
             
           }
       }
    }

    // MARK: - Webservice

    func getParticipantStatusDetailsFromScanApiCallFunc() {
        
        SVProgressHUD.show()
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        
        let token = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "sessionId": dict["sessionId"],
                     "logintype": "live"];
        let tokenString = JSONStringify(value: token as! [String : String])
        
        let param = ["entity" : "CiviMobileParticipant",
                     "action" : "get",
                     "token" : tokenString,
                     "pid" :  self.participantID,
                     "tid" : self.tID,
                     "reset" : self.resetID,
                     "scope" : "scan"];
        
        print("Get Participant status Details request params : \(param)")
        
        apiObj.postData(url: kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("Get Participant status Details response : \(json)")
            if status
            {
                if json["is_error"] == 0{
                    let dictonary = json["values"]
                    
                    if dictonary["validuser"] == true{
                        
                        let dataDictonary = dictonary["data"]
                        
                        let msg = dataDictonary["message"].stringValue
                        if dataDictonary["status"] == true{
                            
                            self.isTicketedEvent = dataDictonary["is_ticketed_event"].boolValue
                            
//                            let participantDetail = dataDictonary["participant_details"].dictionaryValue
                            
//                            let ticketDetail = dataDictonary["ticket_details"].dictionaryValue

                            
                            let participantDetail = dataDictonary["participant_details"].arrayValue
                            let ticketDetail = dataDictonary["ticket_details"].arrayValue
                            
                            if participantDetail.count > 0 {
                                let resDict = participantDetail.first
                                self.participantDetails = Paticipant(participantDetailsDict: (resDict?.dictionary)!)
                                self.participantDetails?.message = msg
                            }
                            if ticketDetail.count > 0  {
                                let responseDict = ticketDetail.first
                                self.participantTicketDetails = Paticipant(participantTicketDetails:  (responseDict?.dictionary)!)
                            }
                            
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                self.updateUI()
                                self.showUI()
                            }

                        }
                        else{
                            //Display msg
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                self.view.makeToast(msg, duration: 3.0, position: .center)
                            }
                        }
                    }
                    else{
                        //Display msg
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                        }
                    }
                }
                else{
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                }
            }
            else{
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
        }
    }
    
    func getParticipantStatusDetailsFromFindApiCallFunc() {
        
        SVProgressHUD.show()
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        
        let token = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "sessionId": dict["sessionId"],
                     "logintype": "live"];
        let tokenString = JSONStringify(value: token as! [String : String])
        
        let param = ["entity" : "CiviMobileParticipant",
                     "action" : "get",
                     "token" : tokenString,
                     "pid" :  self.participantID,
                     "tid" : self.tID,
                     "reset" : self.resetID,
                     "scope" : "view"];
        
        print("Get Participant status Details request params : \(param)")
        
        apiObj.postData(url: kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("Get Participant status Details response : \(json)")
            if status
            {
                if json["is_error"] == 0{
                    let dictonary = json["values"]
                    
                    if dictonary["validuser"] == true{
                        
                        let dataDictonary = dictonary["data"]
                        
                        let msg = dataDictonary["message"].stringValue
                        if dataDictonary["status"] == true{
                            
                            self.isTicketedEvent = dataDictonary["is_ticketed_event"].boolValue

                            let participantDetail = dataDictonary["participant_details"].arrayValue
                            let ticketDetail = dataDictonary["ticket_details"].arrayValue
                            
                            if participantDetail.count > 0 {
                                let resDict = participantDetail.first
                                self.participantDetails = Paticipant(participantDetailsDict: (resDict?.dictionary)!)
                                self.participantDetails?.message = msg
                            }
                            if ticketDetail.count > 0  {
                                let responseDict = ticketDetail.first
                                self.participantTicketDetails = Paticipant(participantTicketDetails:  (responseDict?.dictionary)!)
                            }

                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                self.updateUI()
                                self.showUI()
                            }

//                            let participantDetail = dataDictonary["participant_details"].dictionaryValue
//
//                            let ticketDetail = dataDictonary["ticket_details"].dictionaryValue
//
//                            if ticketDetail.count > 0  {
//                                self.participantTicketDetails = Paticipant(participantTicketDetails:  ticketDetail)
//                            }
//
//                            if participantDetail.count > 0 {
//
//                                self.participantDetails = Paticipant(participantDetailsDict: participantDetail)
//                                self.participantDetails?.message = msg
//
//                                DispatchQueue.main.async {
//                                    SVProgressHUD.dismiss()
//                                    self.updateUI()
//                                    self.showUI()
//                                }
//                            }
//                            else {
//                                //Display msg
//                                DispatchQueue.main.async {
//                                    SVProgressHUD.dismiss()
//                                    self.view.makeToast(msg, duration: 3.0, position: .center)
//                                }
//                            }
                        }
                        else{
                            //Display msg
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                self.view.makeToast(msg, duration: 3.0, position: .center)
                            }
                        }
                    }
                    else{
                        //Display msg
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                        }
                    }
                }
                else{
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                }
            }
            else{
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
        }
    }
    func updateParticipantStatusApiCallFunc() {
        SVProgressHUD.show()
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        
        // logintype : veda or live
        let token = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "sessionId": dict["sessionId"],
                     "logintype": "live"];
        let tokenString = JSONStringify(value: token as! [String : String])
        
        let param = ["entity" : "CiviMobileParticipant",
                     "action" : "updatestatus",
                     "token" : tokenString,
                     "pid" :  self.participantID,
                     "tid" : self.tID,
                     "reset" : self.resetID,
                     "status" : self.status];
        
        print("Get Participant status Details request params : \(param)")
        
        apiObj.postData(url: kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("Get Participant status Details response : \(json)")
            if status
            {
                if json["is_error"] == 0{
                    let dictonary = json["values"]
                    
                    if dictonary["validuser"] == true{
                        
                        let dataDictonary = dictonary["data"]
                        
                        let msg = dataDictonary["message"].stringValue
                        if dataDictonary["status"] == true{
                            self.isTicketedEvent = dataDictonary["is_ticketed_event"].boolValue

                            let participantDetail = dataDictonary["participant_details"].arrayValue
                            let ticketDetail = dataDictonary["ticket_details"].arrayValue
                            
                            if participantDetail.count > 0 {
                                let resDict = participantDetail.first
                                self.participantDetails = Paticipant(participantDetailsDict: (resDict?.dictionary)!)
                                self.participantDetails?.message = msg
                            }
                            if ticketDetail.count > 0  {
                                let responseDict = ticketDetail.first
                                self.participantTicketDetails = Paticipant(participantTicketDetails:  (responseDict?.dictionary)!)
                            }
                            
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                let style = ToastStyle()
                                self.view?.makeToast(msg, duration: 2.0, position: ToastPosition.center, title:"", image:nil, style: style, completion: { (true) in
                                    self.updateUI()
                                })
                            }


//                            if participantDetail.count > 0 {
//
//                                let responseDict = participantDetail.first
//                                self.participantStatus = Paticipant(participantStatusDict: (responseDict?.dictionary)!)
//
//                                self.participantStatus?.message = msg
//
//                                let statusString = responseDict?.dictionary!["status"]?.stringValue
//                                let isAttended = responseDict?.dictionary!["is_attended"]?.stringValue
//                                let statusClass = responseDict?.dictionary!["status_class"]?.stringValue
//                                let lastAttendedDate = responseDict?.dictionary!["last_attended_date"]?.stringValue
//
//                                self.participantDetails?.status = statusString
//                                self.participantDetails?.message = msg
//                                self.participantDetails?.isAttended = isAttended
//                                self.participantDetails?.statusClass = statusClass
//                                self.participantDetails?.lastAttendedDate = lastAttendedDate
//
//                                DispatchQueue.main.async {
//                                    SVProgressHUD.dismiss()
//                                    let style = ToastStyle()
//                                    self.view?.makeToast(msg, duration: 2.0, position: ToastPosition.center, title:"", image:nil, style: style, completion: { (true) in
//                                        self.updateUI()
//                                    })
//                                }
//                            }
//                            else {
//                                //Display msg
//                                DispatchQueue.main.async {
//                                    SVProgressHUD.dismiss()
//                                    self.view.makeToast(msg, duration: 3.0, position: .center)
//                                }
//                            }
                        }
                        else{
                            //Display msg
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                self.view.makeToast(msg, duration: 3.0, position: .center)
                            }
                        }
                    }
                    else{
                        //Display msg
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                        }
                    }
                }
                else{
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                }
            }
            else{
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
        }

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
