//
//  ParticipantStatusViewController.swift
//  RTSPortal
//
//  Created by sajan on 18/09/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import SVProgressHUD
import AVFoundation
import Toast_Swift

class ParticipantStatusViewController: UIViewController,PaticipantStatusDelegate {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var roleLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var viewRegistrationBtn: UIButton!
    @IBOutlet weak var scanBtn: UIButton!
    
    var participantID:String?
    var participantDetails : Paticipant?

    let systemSoundID: SystemSoundID = 1004//1106

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideUI()
        // Do any additional setup after loading the view.
        if self.participantID != nil{
            self.qrCodeScanApiCallFunc()
        }
    }
    
    func hideUI()  {
        self.nameLbl.text = ""
        self.statusLbl.text = ""
        self.roleLbl.text = ""
        self.viewRegistrationBtn.isHidden = true
        self.scanBtn.isHidden = true
    }
    
    func showUI()  {
        self.viewRegistrationBtn.isHidden = false
        self.scanBtn.isHidden = false
    }

    func updateUI()  {
        
        if (self.participantDetails?.participantDisplayName != nil){
            self.imgView.sd_setImage(with: URL(string:self.participantDetails!.participantImageURL!), placeholderImage: UIImage(named: ""))
        }
        self.nameLbl.text = self.participantDetails?.participantDisplayName
        self.roleLbl.text = self.participantDetails?.participantRole
        self.statusLbl.text = self.participantDetails?.message
        
//        self.statusLbl.layer.borderWidth = 2
//        self.statusLbl.layer.cornerRadius = 5
        
        switch self.participantDetails?.statusClass {
        case "success":
            //green
            self.statusLbl.textColor = UIColor.white//(red: 77.0/255.0, green: 120.0/255.0, blue: 39.0/255.0, alpha: 1.0)
//            self.statusLbl.layer.borderColor =  UIColor(red: 77.0/255.0, green: 120.0/255.0, blue: 39.0/255.0, alpha: 1.0).cgColor
            self.statusLbl.backgroundColor = UIColor(red: 77.0/255.0, green: 120.0/255.0, blue: 39.0/255.0, alpha: 1.0)

            break
        case "warning":
            //amber
            self.statusLbl.textColor = UIColor.white//(red: 225.0/255.0, green: 152.0/255.0, blue: 48.0/255.0, alpha: 1.0)
//            self.statusLbl.layer.borderColor = UIColor(red: 225.0/255.0, green: 152.0/255.0, blue: 48.0/255.0, alpha: 1.0).cgColor
            self.statusLbl.backgroundColor = UIColor(red: 225.0/255.0, green: 152.0/255.0, blue: 48.0/255.0, alpha: 1.0)

            break
        case "alert":
            //red
            self.statusLbl.textColor = UIColor.white//(red: 199.0/255.0, green: 72.0/255.0, blue: 59.0/255.0, alpha: 1.0)
//            self.statusLbl.layer.borderColor = UIColor(red: 199.0/255.0, green: 72.0/255.0, blue: 59.0/255.0, alpha: 1.0).cgColor
            self.statusLbl.backgroundColor = UIColor(red: 199.0/255.0, green: 72.0/255.0, blue: 59.0/255.0, alpha: 1.0)
            break
        default:
            break
        }
    }

    // MARK: - PaticipantStatus Delegate Method
    
    func didFinishUpdatingPaticipantStatus(participantDetails: Paticipant?) {
        if participantDetails != nil{
            self.participantDetails = participantDetails
            self.participantID = participantDetails?.participantId
            self.updateUI()
        }
    }

    // MARK: - IBActions

    @IBAction func actionBackBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionViewRegistrationBtnTapped(_ sender: Any) {
        self.moveToParticipantDetailsViewController()
    }
    
    @IBAction func actionScanBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Webservice

    func qrCodeScanApiCallFunc() {
        
        SVProgressHUD.show()
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        
        let token = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "sessionId": dict["sessionId"],
                     "logintype": "live"];
        let tokenString = JSONStringify(value: token as! [String : String])
        
        let param = ["entity" : "CiviMobileParticipant",
                     "action" : "get",
                     "token" : tokenString,
                     "pid" :  self.participantID,
                     "scope" : "scan"];
        
        print("Get Participant Status request params : \(param)")
        
        apiObj.postData(url: kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("Get Participant Status response : \(json)")
            if status
            {
                if json["is_error"] == 0{
                    let dictonary = json["values"]
                    
                    if dictonary["validuser"] == true{
                        
                        let dataDictonary = dictonary["data"]
                        
                        let msg = dataDictonary["message"].stringValue
                        if dataDictonary["status"] == true{
                            
//                            AudioServicesPlaySystemSound(SystemSoundID(self.systemSoundID))
                            
                            let participantDetail = dataDictonary["participant_details"].arrayValue
                            if participantDetail.count > 0 {
                                
                                let responseDict = participantDetail.first
                                self.participantDetails = Paticipant(participantStatusDict: (responseDict?.dictionary)!)
                                self.participantDetails?.message = msg
                                
                                DispatchQueue.main.async {
                                    SVProgressHUD.dismiss()
                                    self.updateUI()
                                    self.showUI()
                                }
                            }
                            else {
                                //Display msg
                                DispatchQueue.main.async {
                                    SVProgressHUD.dismiss()
                                    self.view.makeToast(msg, duration: 3.0, position: .center)
                                }
                            }
                        }
                        else{
                            //Display msg
                            DispatchQueue.main.async {
                            
//                            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
//                                let systemSoundID: SystemSoundID = 1016//1117//1209
//                                AudioServicesPlaySystemSound(SystemSoundID(systemSoundID))
                                if let path = Bundle.main.path(forResource: "beep", ofType: "mp3") {
                                    let url = URL(fileURLWithPath: path) as CFURL
                                    var soundID: SystemSoundID = 0
                                    AudioServicesCreateSystemSoundID(url, &soundID)
                                    AudioServicesPlaySystemSound(soundID)
                                } else {
                                    print("file not found!")
                                }


                                SVProgressHUD.dismiss()
//                                self.view.makeToast(msg, duration: 3.0, position: .center)
                                var style = ToastStyle()
                                self.view?.makeToast(msg, duration: 3.0, position: ToastPosition.center, title:"", image:nil, style: style, completion: { (true) in
                                    self.navigationController?.popViewController(animated: true)
                                })

                            }
                        }
                    }
                    else{
                        //Display msg
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                        }
                    }
                }
                else{
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                }
            }
            else{
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
        }
    }

    func moveToParticipantDetailsViewController()  {
        let detailVC = UIViewController.participantDetailsViewController() as! ParticipantDetailsViewController
        detailVC.participantID = self.participantID
        detailVC.paticipantStatusDelegate = self
        self.navigationController?.pushViewController(detailVC, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
