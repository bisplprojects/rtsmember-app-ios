//
//  CommsPreferenceViewController.swift
//  RTSPortal
//
//  Created by sajan on 26/08/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import SVProgressHUD
import PDFKit
import WebKit
class CommsPreferenceViewController: UIViewController,MTSwitchViewDelegate,WKNavigationDelegate {
    
    let cellIdentifier = "CommsPreferenceCell"
    let tcCellIdentifier = "TCCell"

    @IBOutlet weak var commsPreferenceTableView: UITableView!
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var updateBtn: UIButton!
    
    
    
    //@IBOutlet weak var subHeaderLbl: UILabel!
    @IBOutlet var advisoryLbl: UILabel!
    
    var webView: WKWebView!

    var commsPreferenceDetails : CommsPreference?
    var isPrivacyChecked:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.hideUI()
        self.loadCommsPreferenceApiCallFunc()
    }
    
    func hideUI()  {
        self.headerLbl.text = ""
        self.advisoryLbl.text = ""
        
        self.updateBtn.isHidden = true
    }
    
    func updateUI()  {
    let infoStr = "We want to ensure we are only sending you information by email that is of interest to you, in a way you are happy to receive. Please complete the preferences form below, otherwise you'll stop receiving these communications from us. If you wish not to receive all communications from us, you will need to untick all the boxes and click Save"
        
        let blackColor = UIColor.black
        let attributedStringColor = [NSAttributedString.Key.foregroundColor : blackColor];
        let attributedString = NSAttributedString(string: infoStr, attributes: attributedStringColor)
        
        
       // self.headerLbl.text = (self.commsPreferenceDetails?.title?.uppercased())!+"\n\n\n"+infoStr
        self.headerLbl.text = (self.commsPreferenceDetails?.title?.uppercased())!
        
        let data = infoStr.data(using: String.Encoding.unicode)!
        let attrStr = try? NSAttributedString(
            data: data,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.plain], documentAttributes: nil
        )
        
        
        
        self.advisoryLbl.attributedText = attrStr
        
        self.advisoryLbl.textColor = UIColor.black
        //self.advisoryLbl.textAlignment = .left
        
        
        self.updateBtn.isHidden = false
    }
    
    @objc func actionCheckBtnTapped(_ sender: UIButton) {
        sender.isSelected = (sender.isSelected == true) ? false : true
        sender.setImage(UIImage(imageLiteralResourceName: "checked_icon"), for: .selected)
        sender.setImage(UIImage(imageLiteralResourceName: "unchecked_icon"), for: .normal)
        self.isPrivacyChecked = (sender.isSelected == true) ? true : false
    }
    
    @objc func actionViewBGSPrivacyBtnTapped(_ sender: UIButton) {

        guard let url = URL(string: self.commsPreferenceDetails!.tcUrl!) else {
            self.view.makeToast("Cannot open the document", duration: 3.0, position: .center)
            return
        }
        self.webView = WKWebView(frame: self.view.frame)
        self.webView.navigationDelegate = self
        self.webView.load(URLRequest(url: url))
        self.webView.allowsBackForwardNavigationGestures = true

        let closeBtn   = UIButton(type: UIButton.ButtonType.system) as UIButton
        let image = UIImage(named: "close_btn_icon") as UIImage?
        closeBtn.frame = CGRect(x: self.view.frame.width - 60, y: 50, width: 35, height: 35)
        closeBtn.backgroundColor = UIColor.black
        closeBtn.tintColor = UIColor.white
        closeBtn.layer.cornerRadius = 3
        closeBtn.setImage(image, for: .normal)
        closeBtn.addTarget(self, action:#selector(closeWebView), for: UIControl.Event.touchUpInside)
        
        self.webView.addSubview(closeBtn)
        self.view.addSubview(self.webView)
    }

    @objc func closeWebView()  {
        if (self.webView != nil) {
            self.webView.removeFromSuperview()
        }
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Strat to load")
        SVProgressHUD.show()

    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("finish to load")
        SVProgressHUD.dismiss()
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        SVProgressHUD.dismiss()
    }

    // MARK: - MTSwitchViewDelegate Method

    func switchView(switchView: MitraSwitchView, stateChanged switchState: MTSwithState) {
        print("Switch Tag: \(switchView.tag)")
        let group:Groups = ((self.commsPreferenceDetails?.groups[switchView.tag])!)
        if (switchState == .ON) {
            group.preferenceIsAdded = 1
        }
        else if(switchState == .OFF){
            group.preferenceIsAdded = 0
        }
    }

    // MARK: - IBActions

    @IBAction func actionBackBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionUpdateBtnTapped(_ sender: Any) {

        if self.isPrivacyChecked == true {
            self.updateCommsPreferenceApiCallFunc()
        }else{
            self.view.makeToast("Please accept the BGS Privacy Statement to update", duration: 3.0, position: .center)
        }
    }
    
    // MARK: - Webservice

    func loadCommsPreferenceApiCallFunc() {
        
        SVProgressHUD.show()
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        
        // logintype : veda or live
        let token = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "pin": dict["PIN"],
                     "sessionId": dict["sessionId"],
                     "logintype": "live"];
        let tokenString = JSONStringify(value: token as! [String : String])
        let param = ["entity" : "CiviMobileCommspref",
                     "action" : "getlist",
                     "token" : tokenString,
                     "cid" :  dict["ContactId"]];
        print("Comms Preference  params : \(param)")
        
        apiObj.postData(url: kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("Comms Preference response : \(json)")
            
            if status
            {
                if json["is_error"] == 0{
                    
                    let dictonary = json["values"]
                    if dictonary["validuser"] == true{
                        
                        let dataDict = dictonary["data"].dictionaryValue
                        let message = dataDict["message"]?.stringValue
                        
                        if dataDict["status"] == true{
                            
                            if dataDict["commspref"]!.count > 0{
                                
                            }
                            self.commsPreferenceDetails = CommsPreference(dict: dataDict["commspref"]!.dictionaryValue)

                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                //TODO : Add data to modal class
                                self.updateUI()
                                self.commsPreferenceTableView.reloadData()
                            }
                        }
                        else{
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                self.view.makeToast(message, duration: 3.0, position: .center)
                            }
                        }
                    }
                    else{
                        let message = dictonary["data"]["message"].stringValue
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                            self.view.makeToast(message, duration: 3.0, position: .center)
                        }
                    }
                }
                else{
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.view.makeToast("Could not load Comms Preference", duration: 3.0, position: .center)
                    }
                }
            }
            else{
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                    self.view.makeToast("Could not load Comms Preference", duration: 3.0, position: .center)
                }
            }
        }
    }

    func updateCommsPreferenceApiCallFunc() {
        
        SVProgressHUD.show()
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        
//        let groups = ["60": "1","61": "0","66": "1"];
        
        var preferenceDict:Dictionary<String,String> = [:]
        
        for group in self.commsPreferenceDetails!.groups{
            
            let isAdded = String(group.preferenceIsAdded!)
            let dict:[String:String] = [group.preferenceId!:isAdded]
            preferenceDict.merge(dict){(current, _) in current}
        }
        print(preferenceDict)

        let preferenceGroupsString = JSONStringify(value: preferenceDict )
        
        // logintype : veda or live
        let token = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "pin": dict["PIN"],
                     "sessionId": dict["sessionId"],
                     "logintype": "live"];
        let tokenString = JSONStringify(value: token as! [String : String])
        let param = ["entity" : "CiviMobileCommspref",
                     "action" : "update",
                     "token" : tokenString,
                     "cid" :  dict["ContactId"],
                     "groups" : preferenceGroupsString];
        print("Comms Preference request params : \(param)")
        
        apiObj.postData(url: kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("Comms Preference response : \(json)")
            
            if status
            {
                if json["is_error"] == 0{
                    
                    let dictonary = json["values"]
                    if dictonary["validuser"] == true{
                        
                        let dataDict = dictonary["data"].dictionaryValue
                        let message = dataDict["message"]?.stringValue
                        
                        if dataDict["status"] == true{
                            
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                self.view.makeToast(message, duration: 3.0, position: .center)
                            }
                        }
                        else{
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                self.view.makeToast(message, duration: 3.0, position: .center)
                            }
                        }
                    }
                    else{
                        let message = dictonary["data"]["message"].stringValue
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                            self.view.makeToast(message, duration: 3.0, position: .center)
                        }
                    }
                }
                else{
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.view.makeToast("Could not update Comms Preference", duration: 3.0, position: .center)
                    }
                }
            }
            else{
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                    self.view.makeToast("Could not update Comms Preference", duration: 3.0, position: .center)
                }
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CommsPreferenceViewController: UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 0){
            return (self.commsPreferenceDetails != nil) ? (self.commsPreferenceDetails?.groups.count)! : 0
        }else{
            return (self.commsPreferenceDetails != nil) ? 1 : 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let prefCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! CommsPreferenceCell
            
            let group:Groups = (self.commsPreferenceDetails?.groups[indexPath.row])!
            
            prefCell.titleLbl.text = group.preferenceTitle
            prefCell.preferenceSwitch.tag = indexPath.row
            prefCell.preferenceSwitch.delegate = self
            if (group.preferenceIsAdded == 0){
                prefCell.preferenceSwitch.changeToSwitchState(state: .OFF, animated: false)
            }
            else if(group.preferenceIsAdded == 1){
                prefCell.preferenceSwitch.changeToSwitchState(state: .ON, animated: false)
            }
            
            return prefCell
        }
        else{
            let tcCell = tableView.dequeueReusableCell(withIdentifier: tcCellIdentifier) as! TCCell
            
            tcCell.tcTitleLbl.text = self.commsPreferenceDetails?.tcLabel
            tcCell.tcBtn.setTitle("View BGS Privacy Statement", for: .normal)
            tcCell.tcBtn.addTarget(self, action:#selector(actionViewBGSPrivacyBtnTapped(_:)), for: .touchUpInside)

            tcCell.checkBtn.setImage(UIImage(imageLiteralResourceName: "checked_icon"), for: .selected)
            tcCell.checkBtn.setImage(UIImage(imageLiteralResourceName: "unchecked_icon"), for: .normal)

            tcCell.checkBtn.addTarget(self, action:#selector(actionCheckBtnTapped(_:)), for: .touchUpInside)
            
            self.isPrivacyChecked = (tcCell.checkBtn.isSelected == true) ? true : false

            return tcCell
        }
    }
    
}
