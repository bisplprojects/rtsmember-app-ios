//
//  TCCell.swift
//  RTSPortal
//
//  Created by sajan on 27/08/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit

class TCCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var checkBtn: UIButton!
    @IBOutlet weak var tcTitleLbl: UILabel!
    @IBOutlet weak var tcBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
