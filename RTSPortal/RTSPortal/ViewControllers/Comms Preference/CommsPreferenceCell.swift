//
//  CommsPreferenceCell.swift
//  RTSPortal
//
//  Created by sajan on 27/08/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit

class CommsPreferenceCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var preferenceSwitch: MitraSwitchView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
