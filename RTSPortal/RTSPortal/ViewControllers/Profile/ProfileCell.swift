//
//  ProfileCell.swift
//  RTSPortal
//
//  Created by sajan on 18/07/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit

class ProfileCell: UICollectionViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var itemBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var badgeView: UIView!
    @IBOutlet weak var badgeCountLbl: UILabel!
}
