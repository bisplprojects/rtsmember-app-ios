//
//  NewsDetailsViewController.swift
//  RTSPortal
//
//  Created by sajan on 24/02/20.
//  Copyright © 2020 BISPL. All rights reserved.
//

import UIKit
import SVProgressHUD

class NewsDetailsViewController: UIViewController,UITextViewDelegate {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var newsImgView: UIImageView!
    @IBOutlet weak var introductoryTextLbl: UILabel!
    
    var NewsID:String?
    var newsDetails : News?

    @IBOutlet weak var txtView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.newsImgView.layer.cornerRadius = 6.0
        self.newsImgView.clipsToBounds = true
        
        self.hideUI()
        self.loadNewsDetailsApiCallFunc()
        
        self.txtView.delegate = self
        self.txtView.isUserInteractionEnabled = true
        self.txtView.isEditable = false
        self.txtView.isSelectable = true
        self.txtView.dataDetectorTypes = [.all]
//        txtView.isScrollEnabled = false

    }
    
    func hideUI() {
        self.titleLbl.text = ""
        self.dateLbl.text = ""
        self.introductoryTextLbl.text = ""
        self.txtView.text = ""
    }
    
    func updateUI() {
        
        if let imgURL = self.newsDetails?.newsImageUrl {
            if imgURL != "" {
                self.newsImgView.sd_setImage(with: URL(string:imgURL), placeholderImage: UIImage(named: "img_placeholder"))
            }
        }
        self.titleLbl.text = self.newsDetails?.newsTitle?.uppercased()
        self.dateLbl.text = self.newsDetails?.newsCreatedDate
        
//        self.introductoryTextLbl.text = self.newsDetails?.newsIntroductoryText
//        self.descLbl.text = self.newsDetails?.newsBody
        
        self.introductoryTextLbl.attributedText = self.newsDetails?.newsIntroductoryText?.htmlAttributedString()
//        self.descLbl.attributedText = self.newsDetails?.newsBody?.htmlAttributedString()
    
        self.txtView.attributedText = self.newsDetails?.newsBody?.htmlAttributedString()
        self.adjustUITextViewHeight(arg: self.txtView)

        
//        var myMutableString = NSMutableAttributedString(string: self.newsDetails?.newsBody ?? "", attributes: [NSAttributedString.Key.font :UIFont(name: "Georgia", size: 18.0)!])
//        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red, range: NSRange(location:2,length:4))
//        self.descLbl.attributedText = myMutableString
        
//        let attributedString = NSMutableAttributedString(string: (self.newsDetails?.newsBody)!)
//        // *** Create instance of `NSMutableParagraphStyle`
//        let paragraphStyle = NSMutableParagraphStyle()
//        // *** set LineSpacing property in points ***
//        paragraphStyle.lineSpacing = 3 // Whatever line spacing you want in points
////        paragraphStyle.alignment = .justified
//        // *** Apply attribute to string ***
//        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
//        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red, range: NSMakeRange(0, attributedString.length))
//
//        // *** Set Attributed String to your label ***
//        self.descLbl.attributedText = attributedString


    }
    
    func adjustUITextViewHeight(arg : UITextView)
    {
        arg.translatesAutoresizingMaskIntoConstraints = true
        arg.sizeToFit()
        arg.isScrollEnabled = false
    }

    // MARK: - IBActions

    @IBAction func actionBackBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Webservice

    func loadNewsDetailsApiCallFunc() {
        
        SVProgressHUD.show()
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        
        // logintype : veda or live
        let token = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "sessionId": dict["sessionId"],
                     "logintype": "live"];
        let tokenString = JSONStringify(value: token as! [String : String])
        let param = ["entity" : "CiviMobileNews",
                     "action" : "getdetails",
                     "token" : tokenString,
                     "cid" :  dict["ContactId"],
                     "nid" : self.NewsID];
        print("News Details  params : \(param)")
        
        apiObj.postData(url: kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("News Details response : \(json)")
            
            if status
            {
                if json["is_error"] == 0{
                    
                    let dictonary = json["values"]
                    if dictonary["validuser"] == true{
                        
                        let dataDict = dictonary["data"].dictionaryValue
                        let message = dataDict["message"]?.stringValue
                        
                        if dataDict["status"] == true{
                            
                            if dataDict["newsdetails"]!.count > 0{
                                
                            }
                            self.newsDetails = News(detailDict: dataDict["newsdetails"]!.dictionaryValue)

                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                self.updateUI()
                            }
                        }
                        else{
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                self.view.makeToast(message, duration: 3.0, position: .center)
                            }
                        }
                    }
                    else{
                        let message = dictonary["data"]["message"].stringValue
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                            self.view.makeToast(message, duration: 3.0, position: .center)
                        }
                    }
                }
                else{
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.view.makeToast("Could not load news", duration: 3.0, position: .center)
                    }
                }
            }
            else{
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                    self.view.makeToast("Could not load news", duration: 3.0, position: .center)
                }
            }
        }
    }
    
    // MARK: - UITextView  Delegate Methods

    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        if UIApplication.shared.canOpenURL(URL) {
            UIApplication.shared.open(URL)
        }
        return false
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension String {
    func htmlAttributedString() -> NSAttributedString? {
        
//        // Custom color
//        let greenColor = UIColor(red: 10/255, green: 190/255, blue: 50/255, alpha: 1)
//        // create the attributed colour
//        let attributedStringColor = [NSAttributedString.Key.foregroundColor : greenColor];

        guard let data = self.data(using: String.Encoding.utf16, allowLossyConversion: false) else { return nil }
        guard let html = try? NSMutableAttributedString(
            data: data,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
            documentAttributes: nil) else { return nil }
        
//        html.addAttribute(NSAttributedString.Key.foregroundColor, value: attributedStringColor, range:NSRange(location: 0, length: html.string.count))

        return html
    }
}
