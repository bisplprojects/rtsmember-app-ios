//
//  MySIGListViewController.swift
//  RTSPortal
//
//  Created by sajan on 30/07/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import SVProgressHUD

class MySIGListViewController: UIViewController,ChooseSigDelegate,SigJoinDelegate {
    
    let cellIdentifier = "MySigListCell"

    @IBOutlet weak var mySigTableView: UITableView!
    
    var mySigsList:[Sig] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.loadMySigsListApiCallFunc()
    }
    
    // MARK: - IBActions

    @IBAction func actionBackBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionAddSigBtnTapped(_ sender: Any) {
        
        // Safe Present
        if let vc = UIStoryboard(name: "Profile", bundle: Bundle.main).instantiateViewController(withIdentifier: "ChooseSIGViewController") as? ChooseSIGViewController
        {
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.chooseSigDelegate = self
            present(vc, animated: true, completion: nil)
        }
    }
    
    // MARK: - ChooseSig Delegate

    func didFinishChoosingSIG(_ sigId: String) {
        
        let storyboard = UIStoryboard(name: "Profile", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: "MySIGDetailViewController") as! MySIGDetailViewController
        vc.sigId = sigId
        vc.sigJoinDelegate = self
        self.navigationController?.pushViewController(vc, animated: false)

    }

    func didFinishedJoinigSIG(message: String) {
        if message == "success" {
            self.loadMySigsListApiCallFunc()
        }
    }

    // MARK: - Webservice
    
    func loadMySigsListApiCallFunc() {
        
        SVProgressHUD.show()
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]

        // logintype : veda or live
        let token = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "pin": dict["PIN"],
                     "sessionId": dict["sessionId"],
                     "logintype": "live"];
        let tokenString = JSONStringify(value: token as! [String : String])
        let param = ["entity" : "CiviMobileSig",
                     "action" : "mysigs",
                     "token" : tokenString,
                     "cid" : dict["ContactId"]];
        print("All Events request params : \(param)")
        
        apiObj.postData(url: kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("All Events List response : \(json)")

            if status
            {
                if json["is_error"] == 0{
                    let dictonary = json["values"]
                    if dictonary["validuser"] == true{
                        
                        let dataDict = dictonary["data"].dictionaryValue
                        let mySigs = dataDict["mysigs"]?.arrayValue
                        let message = dataDict["message"]?.stringValue
                        
                        if mySigs!.count > 0 {
                            
                            let responseArray = mySigs
                            
                            self.mySigsList.removeAll()
                            for (_, sigDict) in (responseArray?.enumerated())! {
                                let sigObj = Sig(dict: sigDict.dictionary!)
                                self.mySigsList.append(sigObj)
                            }
                            
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                // TO DO : Display toast msg
                                self.mySigTableView.reloadData()
                                if self.mySigsList.count == 0 {
                                    self.mySigTableView.setEmptyMessage("Nothing found")
                                } else {
                                    self.mySigTableView.restore()
                                }
                            }
                        }
                        else {
                            //Display msg
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                self.view.makeToast(message, duration: 3.0, position: .center)
                            }
                        }
                    }
                    else
                    {
                        //Display msg
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                        }
                    }
                }
                else
                {
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                }
            }
            else
            {
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MySIGListViewController: UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.mySigsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let mySigCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! MySigListCell
        
        mySigCell.containerView.layer.cornerRadius = 5
        mySigCell.containerView.layer.shadowColor = UIColor.lightGray.cgColor
        mySigCell.containerView.layer.shadowOpacity = 0.3
        mySigCell.containerView.layer.shadowOffset = .zero //CGSize(width: 1, height: 1)
        mySigCell.containerView.layer.shadowRadius = 4
        
        let mySig:Sig = self.mySigsList[indexPath.row]
        
        mySigCell.titleLbl.text = mySig.sigDisplayName
//        mySigCell.descriptionLbl.text = mySig.sigSummary
        mySigCell.imgView.sd_setImage(with: URL(string:mySig.sigImageURL!))

        
        let attributedString = NSMutableAttributedString(string: (mySig.sigSummary?.htmlToString)!)
        // *** Create instance of `NSMutableParagraphStyle`
        let paragraphStyle = NSMutableParagraphStyle()
        // *** set LineSpacing property in points ***
        paragraphStyle.lineSpacing = 4 // Whatever line spacing you want in points
//        paragraphStyle.alignment = .justified
        // *** Apply attribute to string ***
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        
        // *** Set Attributed String to your label ***
        mySigCell.descriptionLbl.attributedText = attributedString

        return mySigCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}
