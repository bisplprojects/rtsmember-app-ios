//
//  ChooseSIGViewController.swift
//  RTSPortal
//
//  Created by sajan on 31/07/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import SVProgressHUD
import Toast_Swift

protocol ChooseSigDelegate {
    func didFinishChoosingSIG(_ sigId:String)
}

class ChooseSIGViewController: UIViewController {
    
    var chooseSigDelegate: ChooseSigDelegate?

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var chooseSigTableView: UITableView!
    
    let cellIdentifier = "ChooseSigCell"
    var chooseSigsList:[Sig] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.containerView.layer.cornerRadius = 5
        self.containerView.layer.shadowColor = UIColor.gray.cgColor
        self.containerView.layer.shadowOpacity = 0.3
        self.containerView.layer.shadowOffset = .zero //CGSize(width: 1, height: 1)
        self.containerView.layer.shadowRadius = 4

        self.loadChooseSigsListApiCallFunc()
    }
    

    // MARK: - Webservice
    
    func loadChooseSigsListApiCallFunc() {
        
        SVProgressHUD.show()
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]

        // logintype : veda or live
        let token = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "pin": dict["PIN"],
                     "sessionId": dict["sessionId"],
                     "logintype": "live"];
        let tokenString = JSONStringify(value: token as! [String : String])
        let param = ["entity" : "CiviMobileSig",
                     "action" : "choosesig",
                     "token" : tokenString,
                     "cid" : dict["ContactId"]];
        print("All Events request params : \(param)")
        
        apiObj.postData(url: kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("All Events List response : \(json)")
            let style = ToastStyle()

            if status
            {
                if json["is_error"] == 0{
                    let dictonary = json["values"]
                    if dictonary["validuser"] == true{
                        
                        let dataDict = dictonary["data"].dictionaryValue
                        let chooseSigsArray = dataDict["siglist"]?.arrayValue
                        
                        if chooseSigsArray!.count > 0 {
                            
                            let responseArray = chooseSigsArray
                            
                            for (_, sigDict) in (responseArray?.enumerated())! {
                                let sigObj = Sig(chooseSigDict: sigDict.dictionary!)
                                self.chooseSigsList.append(sigObj)
                            }
                            
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                self.chooseSigTableView.reloadData()
                                if self.chooseSigsList.count == 0 {
                                    self.chooseSigTableView.setEmptyMessage("Nothing found")
                                } else {
                                    self.chooseSigTableView.restore()
                                }
                            }
                        }
                        else {
                            //Display msg
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                self.view?.makeToast("No SIGs found", duration: 2.0, position: ToastPosition.center, title:"", image:nil, style: style, completion: { (true) in
                                    self.dismiss(animated: true, completion: nil)
                                })
                            }
                        }
                    }
                    else
                    {
                        //Display msg
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                }
                else
                {
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.dismiss(animated: true, completion: nil)
                    }
                }
            }
            else
            {
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension ChooseSIGViewController: UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.chooseSigsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let chooseSigCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! ChooseSigCell
        
        let mySig:Sig = self.chooseSigsList[indexPath.row]
        
        chooseSigCell.titleLbl.text = mySig.sigDisplayName
        
        return chooseSigCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mySig:Sig = self.chooseSigsList[indexPath.row]
        self.chooseSigDelegate?.didFinishChoosingSIG(mySig.sigId!)
        self.dismiss(animated: true, completion: nil)
    }
}
