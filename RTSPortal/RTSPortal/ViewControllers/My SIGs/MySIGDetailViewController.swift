//
//  MySIGDetailViewController.swift
//  RTSPortal
//
//  Created by sajan on 30/07/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import SVProgressHUD
import Toast_Swift

protocol SigJoinDelegate {
    func didFinishedJoinigSIG(message : String)
}
class MySIGDetailViewController: UIViewController {

    var sigJoinDelegate:SigJoinDelegate?
    
    @IBOutlet weak var sigImageView: UIImageView!
    @IBOutlet weak var titleContainerView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
   
    var sigId:String?
    var sigDetails : Sig?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.titleContainerView.layer.cornerRadius = 5
        self.titleContainerView.layer.shadowColor = UIColor.lightGray.cgColor
        self.titleContainerView.layer.shadowOpacity = 0.3
        self.titleContainerView.layer.shadowOffset = .zero //CGSize(width: 1, height: 1)
        self.titleContainerView.layer.shadowRadius = 4

        self.hideUI()
        self.loadSigDetailsApiCallFunc()
    }
    
    // MARK: - IBActions

    @IBAction func actionBackBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionJoinSIGBtnTapped(_ sender: Any) {
        if (!self.sigId!.trimmingCharacters(in: .whitespaces).isEmpty ) {
            self.joinSIGApiCallFunc()
        }
    }
    
    // MARK: - Webservice
    
    func loadSigDetailsApiCallFunc() {
        
        SVProgressHUD.show()
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        
        // logintype : veda or live
        let token = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "pin": dict["PIN"],
                     "sessionId": dict["sessionId"],
                     "logintype": "live"];
        let tokenString = JSONStringify(value: token as! [String : String])
        let param = ["entity" : "CiviMobileSig",
                     "action" : "getdetails",
                     "token" : tokenString,
                     "sig_id" : self.sigId,
                     "cid" :  dict["ContactId"]];
        print("Events Details request params : \(param)")
        
        apiObj.postData(url: kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("Events Details response : \(json)")
            
            if status
            {
                if json["is_error"] == 0{
                    let dictonary = json["values"]
                    if dictonary["validuser"] == true{
                        
                        let dataDict = dictonary["data"].dictionaryValue
                        let sigDetail = dataDict["sigdetails"]?.dictionaryValue
                        print(sigDetail?.count as Any)
                        
                        self.sigDetails = Sig(sigDetailDict: sigDetail!)
                        
                        if sigDetail!.count > 0 {
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                // TO DO : Display toast msg
                                self.updateUI()
                                self.showUI()
                            }
                        }
                        else {
                            //Display msg
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                self.view.makeToast("SIG details not found", duration: 3.0, position: .center)
                            }
                        }
                    }
                    else{
                        //Display msg
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                        }
                    }
                }
                else{
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                }
            }
            else{
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
        }
    }

    
    func joinSIGApiCallFunc() {
        
        SVProgressHUD.show()
        let apiObj = ApiHandler()
        var deviceToken = UserDefaults.standard.getDeviceToken()
        if deviceToken == nil
        {
            if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                print(uuid)
                UserDefaults.standard.setDeviceToken(value: uuid)
            }
        }
        deviceToken = UserDefaults.standard.getDeviceToken()
        let dict = UserDefaults.standard.getBGSUserData() as! [String : String]
        
        // logintype : veda or live
        let token = ["deviceId": deviceToken,
                     "appName": "CiviMemberAPP",
                     "pin": dict["PIN"],
                     "sessionId": dict["sessionId"],
                     "logintype": "live"];
        let tokenString = JSONStringify(value: token as! [String : String])
        let param = ["entity" : "CiviMobileSig",
                     "action" : "join",
                     "token" : tokenString,
                     "sig_id" : self.sigId,
                     "cid" :  dict["ContactId"]];
        print("SIG Registration request params : \(param)")
        
        apiObj.postData(url: kBaseUrl , parameter: param as [String : AnyObject]) { (json, status) in
            
            print("SIG Registration response : \(json)")
            
            if status
            {
                if json["is_error"] == 0{
                    
                    var style = ToastStyle()
                    style.messageColor = UIColor.white
                    style.titleFont.withSize(16)
                    
                    let dictonary = json["values"]
                    if dictonary["validuser"] == true{
                        
                        let dataDict = dictonary["data"].dictionaryValue
                        let message = dataDict["message"]?.stringValue

                        if dataDict["status"] == true{
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                self.view?.makeToast(message, duration: 3.0, position: ToastPosition.center, title:"", image:nil, style: style, completion: { (true) in
                                    self.sigJoinDelegate?.didFinishedJoinigSIG(message: "success")
                                    self.navigationController?.popViewController(animated: true)
                                })
                            }
                        }
                        else{
                            DispatchQueue.main.async {
                                SVProgressHUD.dismiss()
                                self.view?.makeToast(message, duration: 3.0, position: ToastPosition.center, title:"", image:nil, style: style, completion: { (true) in
                                    self.navigationController?.popViewController(animated: true)
                                })
                            }
                        }
                    }
                    else{
                        let message = dictonary["data"]["message"].stringValue
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                            self.view?.makeToast(message, duration: 3.0, position: ToastPosition.center, title:"", image:nil, style: style, completion: { (true) in
                                self.navigationController?.popViewController(animated: true)
                            })
                        }
                    }
                }
                else{
                    //Display msg
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.view.makeToast("Could not join", duration: 3.0, position: .center)
                    }
                }
            }
            else{
                //Display msg
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                    self.view.makeToast("Could not join", duration: 3.0, position: .center)
                }
            }
        }
    }

    func hideUI()  {
        self.titleLbl.text = ""
        self.descriptionLbl.text = ""
        self.titleContainerView.isHidden = true
    }

    func updateUI()  {
        self.sigImageView.sd_setImage(with: URL(string:self.sigDetails!.sigImageURL!), placeholderImage: UIImage(named: ""))
        self.titleLbl.text = sigDetails?.sigDisplayName
        
        let attributedString = NSMutableAttributedString(string: (sigDetails?.sigDescription?.htmlToString)!)
        // *** Create instance of `NSMutableParagraphStyle`
        let paragraphStyle = NSMutableParagraphStyle()
        // *** set LineSpacing property in points ***
        paragraphStyle.lineSpacing = 4 // Whatever line spacing you want in points
        //        paragraphStyle.alignment = .justified
        // *** Apply attribute to string ***
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        
        // *** Set Attributed String to your label ***
        self.descriptionLbl.attributedText = attributedString

    }
    
    func showUI()  {
        self.titleContainerView.isHidden = false
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
