//
//  AppDelegate.swift
//  RTSPortal
//
//  Created by sajan on 08/07/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications
import IQKeyboardManagerSwift
import SVProgressHUD
import SDWebImage
import Toast_Swift
import GoogleMaps
import GooglePlaces
import Stripe
import Firebase
import SafariServices

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, CLLocationManagerDelegate,MessagingDelegate {

    var window: UIWindow?
    
    let gcmMessageIDKey = "gcm.message_id"

    var priceFieldList:[PriceField]?
    let locationManager = CLLocationManager()
    

//8061914444 7749 9611874003
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        SVProgressHUD.setDefaultMaskType(.black)
        IQKeyboardManager.shared.enable = true
        RunLoop.current.run(until: Date(timeIntervalSinceNow : 3.0))
        
        
        STPPaymentConfiguration.shared().publishableKey = kStripePublishableKey
        
        GMSServices.provideAPIKey(kGoogleMapsAPIKey)
        GMSPlacesClient.provideAPIKey(kGoogleMapsAPIKey)
        
        locationManager.delegate = self
        //setupLocationService()
        let locationAuthorizationStatus = CLLocationManager.authorizationStatus()
        
           switch locationAuthorizationStatus {
             case .notDetermined:
            locationManager.requestWhenInUseAuthorization() // This is where you request permission to use location services
            locationManager.requestAlwaysAuthorization()
            case .authorizedWhenInUse, .authorizedAlways:
                    if CLLocationManager.locationServicesEnabled() {
                           locationManager.startUpdatingLocation()
                        }
                case .restricted, .denied:
                   print("Enable location services")
            locationManager.requestWhenInUseAuthorization()
            locationManager.requestAlwaysAuthorization()
                  
            }
            
        if let option = launchOptions {
            let info = option[UIApplication.LaunchOptionsKey.remoteNotification]
            if (info != nil) {
                self.showNotification(application, withUserInfo: info as! [AnyHashable : Any])
            }
        }

        FirebaseApp.configure()
        Messaging.messaging().delegate = self

        UNUserNotificationCenter.current().delegate = self
         
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
         
        UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (_, error) in
            guard error == nil else{
                print(error!.localizedDescription)
                return
            }
        }
         
        //get application instance ID
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                UserDefaults.standard.setFCMToken(value: result.token)
            }
        }

        application.registerForRemoteNotifications()

        if let uuid = UIDevice.current.identifierForVendor?.uuidString {
            print(uuid)
            UserDefaults.standard.setDeviceToken(value: uuid)
        }

        return true
    }
    
    
//}

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }


    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        saveTimesOfOpenApp()
        
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "RTSPortal")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

    //MARK: - Remote Notification
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
//        let deviceIdentifier: String = UIDevice.current.identifierForVendor!.uuidString
//        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        
        let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
        let token = tokenParts.joined()
        print("Device Token: \(token)")
//        UserDefaults.standard.setDeviceToken(value: token)
        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Push failed to register with error: %@", error)
    }


    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
        print("my push is: %@", userInfo)
        guard
            let aps = userInfo[AnyHashable("aps")] as? NSDictionary,
            let alert = aps["alert"] as? String,
//            let body = userInfo[AnyHashable("body")] as? String,
//            let title = alert["title"] as? String,
            let urlLink = userInfo[AnyHashable("url")] as? String
            else {
                // handle any error here
                return
        }
        
//        print("Title: \(title) \nBody:\(body) \nURL:\(urlLink)")
        print("Body:\(alert) \nURL:\(urlLink)")

        let loggedIn = UserDefaults.standard.isLoggedIn()
        if loggedIn == true {
            
            if (application.applicationState == .active) {
                let alert = UIAlertController(title: "", message: alert, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Read", style: .default, handler: { action in
                    // Open msg in Safari Viewcoroller
                    self.openMessagesInWebView(urlLink: urlLink)
                }))
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
                self.window?.rootViewController?.present(alert, animated: true, completion: nil)

            }
            else{
                // Open msg in Safari Viewcoroller
                self.openMessagesInWebView(urlLink: urlLink)
            }
        }

    }
    

    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        UserDefaults.standard.setFCMToken(value: fcmToken)

         
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
     
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }


    func showNotification(_ application:UIApplication, withUserInfo: Dictionary<AnyHashable, Any>) {
        //Check login status
        let loggedIn = UserDefaults.standard.isLoggedIn()
        if loggedIn == true{
            //navigate to home
            self.showDashboardWindow()
            DispatchQueue.main.async {
                self.showPopupMessageForBackground(withUserInfo)
            }
        }
        else{
            //Redirect to login screen
            self.showLoginWindow()
        }

    }
            
        
    func showDashboardWindow(){
                
        let storyboard1  = UIStoryboard(name: "Main", bundle: nil)
        let storyboard2  = UIStoryboard(name: "Profile", bundle: nil)

        let navigationController:UINavigationController = storyboard1.instantiateInitialViewController() as! UINavigationController
        
        let rootViewController:UIViewController = storyboard1.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
        
        let dashboardVC = storyboard2.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
//        dashboardVC.shouldOpenNotidfication = true
        
        navigationController.viewControllers = [rootViewController,dashboardVC]
        
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()

    }

    func showLoginWindow(){
        let storyboard  = UIStoryboard(name: "Main", bundle: nil)
        let navigationController:UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
        
        let rootViewController:UIViewController = storyboard.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
        
        navigationController.viewControllers = [rootViewController]
        
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
    }
        
    func showPopupMessageForBackground(_ withUserInfo: Dictionary<AnyHashable, Any>)  {

        print("my push is: %@", withUserInfo)
        guard
            let aps = withUserInfo[AnyHashable("aps")] as? NSDictionary,
            let alert = aps["alert"] as? String,
//            let body = userInfo[AnyHashable("body")] as? String,
//            let title = alert["title"] as? String,
            let urlLink = withUserInfo[AnyHashable("url")] as? String
            else {
                // handle any error here
                return
        }
        
        //        print("Title: \(title) \nBody:\(body) \nURL:\(urlLink)")
        print("Body:\(alert) \nURL:\(urlLink)")

        // Open msg in Safari Viewcoroller
        self.openMessagesInWebView(urlLink: urlLink)

    }
        
    func openMessagesInWebView(urlLink : String)  {
        
        let urlString:String? = urlLink
        guard let stringURL = urlString else {
            return
        }
        //"http://jesusguerra.io/"
        
        guard let url = URL(string: stringURL) else {
            // We should handle an invalid stringURL
            return
        }

        // Present SFSafariViewController
        let safariVC = SFSafariViewController(url: url)
        safariVC.preferredBarTintColor = UIColor(red: 17.0/255.0, green: 131.0/255.0, blue: 247.0/255.0, alpha: 1.0)
        safariVC.preferredControlTintColor = UIColor.white
        safariVC.dismissButtonStyle = .close
        self.window?.rootViewController?.present(safariVC, animated: true, completion: nil)

    }

}

func isAppAlreadyLaunchedOnce()->Bool{
    let defaults = UserDefaults.standard
    
    
    if let isAppAlreadyLaunchedOnce = defaults.string(forKey: "hasLaunchedOnce"){
        print("App already launched : \(isAppAlreadyLaunchedOnce)")
        return true
    }else{
        defaults.set(true, forKey: "hasLaunchedOnce")
        print("App launched first time")
        return false
    }
}

func isFirstLaunch() -> Bool {
    let defaults = UserDefaults.standard
    
    if let isFirstLaunch = defaults.string(forKey: "isFirstLaunch"){
        print("is First Launch?: \(isFirstLaunch)")
        return true
    }
    /*if UserDefaults.standard.bool(forKey: "isFirstLaunch") {
        UserDefaults.standard.set(true, forKey: "isFirstLaunch")
        UserDefaults.standard.synchronize()
        return true
    }*/
    else{
        defaults.set(true, forKey: "isFirstLaunch")
    return false
    }
}

func saveTimesOfOpenApp() -> Void {
    let userDefaults = UserDefaults.standard
    var currentTimesOfOpenApp:Int = 0
    currentTimesOfOpenApp = getCurrentTimesOfOpenApp()
    userDefaults.set(currentTimesOfOpenApp, forKey: "timesOfOpenApp")
}

func getCurrentTimesOfOpenApp() -> Int {
    let userDefaults = UserDefaults.standard
    return userDefaults.integer(forKey: "timesOfOpenApp") + 1
}

    



