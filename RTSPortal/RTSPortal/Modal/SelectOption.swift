//
//  SelectOption.swift
//  RTSPortal
//
//  Created by sajan on 07/08/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import SwiftyJSON

class SelectOption: NSObject {
    
    var optionId : String?
    var name : String?

    var isOptionSelected : Bool?
    
    init(dict:[String : JSON]) {
        
        self.optionId = dict["id"]?.stringValue
        self.name = dict["name"]?.stringValue
        
        self.isOptionSelected = false
    }

}

/*
 {
 "id" : 1,
 "name" : "Advanced Clinical Practitioner"
 }
*/
