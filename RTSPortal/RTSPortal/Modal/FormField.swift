//
//  FormField.swift
//  RTSPortal
//
//  Created by sajan on 07/08/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import SwiftyJSON

class FormField: NSObject {

    var groupId : String?
    var title : String?
    var name : String?
    var defaultValue : Array<Any>?
    var htmlType : String?
    var isRequired : String?
    var fieldId : String?
    
    var selectOptions = [SelectOption]()
    
    var dietaryRequirementOptions = [SelectOption]()

    var helpPost : String?
    var helpPre : String?

    
    init(dict:[String : JSON]) {
        
        self.groupId = dict["group_id"]?.stringValue
        self.title = dict["title"]?.stringValue
        self.name = dict["name"]?.stringValue
        self.defaultValue = dict["default_value"]?.arrayObject
        self.htmlType = dict["html_type"]?.string
        self.isRequired = dict["is_required"]?.string
        self.fieldId = dict["field_id"]?.string
        
        self.helpPost = dict["help_post"]?.string
        self.helpPre = dict["help_pre"]?.string

        if (dict["html_type"]?.string == "Select" && dict["name"]?.string == "custom_107"){
            if dict["options"]!.arrayValue.count > 0{
                for opt in (dict["options"]?.array)! {
                    let optn = SelectOption(dict: opt.dictionaryValue)
                    self.selectOptions.append(optn)
                }
            }
        }
        
        if (dict["html_type"]?.string == "Radio" && dict["name"]?.string == "custom_324"){
            if dict["options"]!.arrayValue.count > 0{
                for opt in (dict["options"]?.array)! {
                    let optn = SelectOption(dict: opt.dictionaryValue)
                    self.dietaryRequirementOptions.append(optn)
                }
            }
        }


    }

}


/*
 {
 "group_id" : "39",
 "visibility" : "User and User Admin Only",
 "groupTitle" : "Your details",
 "groupHelpPre" : "",
 "name" : "email-Primary",
 "is_view" : null,
 "groupName" : "General_Speaker_39",
 "website_type_id" : null,
 "bao" : "CRM_Core_BAO_Email",
 "help_post" : null,
 "phone_type_id" : null,
 "title" : "Email (Primary)",
 "rule" : "email",
 "attributes" : {
 "maxlength" : 254,
 "size" : 30
 },
 "default_value" : "appmember1@vedaconsulting.co.uk",
 "html_type" : "Text",
 "dbName" : null,
 "data_type" : "String",
 "skipDisplay" : 0,
 "add_captcha" : "0",
 "is_required" : "1",
 "location_type_id" : null,
 "in_selector" : "0",
 "field_id" : "313",
 "help_pre" : null,
 "add_to_group_id" : null,
 "pseudoconstant" : null,
 "where" : "civicrm_email.email",
 "groupHelpPost" : "",
 "field_type" : "Contact"
 }
 */
