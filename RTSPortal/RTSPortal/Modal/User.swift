//
//  User.swift
//  RTSPortal
//
//  Created by sajan on 24/07/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import SwiftyJSON

class User: NSObject {
    var userName : String?
    var userEmail : String?
    var userPhoneNumber : String?
    var userProfileImgURL : String?
    var streetName : String?
    var supplimentalAddress1 : String?
    var supplimentalAddress2 : String?
    var city : String?
    var postcode : String?
    var stateName : String?
    var country : String?
    
    var contactId : String?
    var biography : String?
    var placeOfWork : String?
    var jobTitle : String?
    var profession : String?
    
    var myDetailsFields  = [MyDetailsField]()


    init(dict:[String : JSON]) {
        
        self.userName = dict["display_name"]?.stringValue
        self.userEmail = dict["email"]?.stringValue
        self.userPhoneNumber = dict["phone"]?.string
        self.userProfileImgURL = dict["image_URL"]?.string
        self.streetName = dict["street_address"]?.string
        self.supplimentalAddress1 = dict["supplemental_address_1"]?.string
        self.supplimentalAddress2 = dict["supplemental_address_2"]?.string
        self.city = dict["city"]?.string
        self.postcode = dict["postal_code"]?.string
        self.stateName = dict["state_province_name"]?.string
        self.country = dict["country"]?.string
        
        self.contactId = dict["cid"]?.string
        self.biography = dict["biography"]?.string
        self.placeOfWork = dict["place_of_work"]?.string
        self.jobTitle = dict["job_title"]?.string
        self.profession = dict["profession"]?.string
        
        if dict["fields"]!.arrayValue.count > 0{
            for formFieldItem in (dict["fields"]?.array)! {
                let formField = MyDetailsField(dict: formFieldItem.dictionaryValue)
                self.myDetailsFields.append(formField)
            }
        }
    }

}

/*
 "supplemental_address_1" : "Address Line 2",
 "postal_code" : "E3 2PX",
 "display_name" : "Apple Team",
 
 "fields" : [{
 "is_required" : "1",
 "default_value" : ["appleteam@vedaconsulting.co.uk"],
 "data_type" : "String",
 "field_id" : "313",
 "help_post" : null,
 "html_type" : "Text",
 "name" : "email-Primary",
 "title" : "Email (Primary)",
 "help_pre" : null}],
 
 "biography" : "Biography goes here",
 "city" : "London",
 "image_URL" : "https:\/\/bgs1.vedacrm.co.uk\/sites\/default\/civicrm_custom\/ext\/\/uk.co.vedaconsulting.module.vedacivimobile\/images\/default_avatar_male.png",
 "phone" : "0798764321",
 "place_of_work" : "Apple",
 "country" : "United Kingdom",
 "street_address" : "Address Line 1",
 "email" : "appleteam@vedaconsulting.co.uk",
 "job_title" : "Developer",
 "profession" : "Specialist Registrar",
 "supplemental_address_2" : "",
 "state_province_name" : "",
 "cid" : "25850"

*/

/*
 {
 "image_URL" : "https:\/\/bgs1.vedacrm.co.uk\/sites\/default\/civicrm_custom\/ext\/\/uk.co.vedaconsulting.module.vedacivimobile\/images\/default_avatar_male.png",
 "postal_code" : "E3 2PX",
 "display_name" : "App Member1",
 "state_province_name" : "",
 "supplemental_address_1" : "419, Wick Lane",
 "supplemental_address_2" : "Bow",
 "city" : "London",
 "street_address" : "Unit 4",
 "email" : "appmember1@vedaconsulting.co.uk",
 "cid" : "23538",
 "country" : "United Kingdom",
 "phone" : "07987654321"
 }
*/
