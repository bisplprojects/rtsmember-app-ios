//
//  Menu.swift
//  RTSPortal
//
//  Created by Rohit Singh on 30/10/2019.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import Foundation
import SwiftyJSON

class Menu: NSObject {
    
    var name : String?
    var display : String?
    var icon : String?
    var msgUrl :String?
    
    
    override init() {
        super.init()
}

    init(menuDict:[String : JSON]) {
        
        self.name = menuDict["name"]?.string
        self.display = menuDict["display"]?.stringValue
        self.icon = menuDict["icon"]?.string
        
        guard let urlLink = menuDict["url"]?.stringValue else {
            return
        }
        self.msgUrl = urlLink 

        
}
}
