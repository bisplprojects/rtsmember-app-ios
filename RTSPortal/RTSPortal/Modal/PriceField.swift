//
//  Price.swift
//  RTSPortal
//
//  Created by sajan on 01/08/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import SwiftyJSON

class PriceField: NSObject {

    var priceFieldId : String?
    var priceFieldName : String?
    var priceFieldLabel : String?
    var priceFieldHtmlType : String?
    var isEnterQty : String?
    var isDisplayAmounts : String?
    var weight : String?
    var optionsPerLine : String?
    var visibilityId : String?
    var visibility : String?
    var isRequired : String?
    var isActive : String?

    var options = [Option]()

    init(dict:[String : JSON]) {
        
        self.priceFieldId = dict["id"]?.stringValue
        self.priceFieldName = dict["name"]?.stringValue
        self.priceFieldLabel = dict["label"]?.string
        self.priceFieldHtmlType = dict["html_type"]?.string
        self.isEnterQty = dict["is_enter_qty"]?.string
        self.isDisplayAmounts = dict["is_display_amounts"]?.string
        self.weight = dict["weight"]?.stringValue
        self.optionsPerLine = dict["options_per_line"]?.stringValue
        self.visibilityId = dict["visibility_id"]?.string
        self.visibility = dict["visibility"]?.string
        self.isRequired = dict["is_required"]?.string
        self.isActive = dict["is_active"]?.string
        
        if dict["options"]!.arrayValue.count > 0{
            for opt in (dict["options"]?.array)! {
                let optn = Option(dict: opt.dictionaryValue, isRequired:(dict["is_required"]?.string)!)
                self.options.append(optn)
            }
        }
//        self.options = dict["session_choices"]?.arrayObject as? [String]

    }

}
/*
 {
 "is_display_amounts" : "1",
 "is_enter_qty" : "0",
 "is_active" : "1",
 "weight" : "5",
 "visibility_id" : "1",
 "options_per_line" : "1",
 "visibility" : "public",
 "is_required" : "1",
 "options" : [],
 "name" : "category_1_non_member_consultant_consultant_geriatrician_gp_",
 "id" : "160",
 "html_type" : "Radio",
 "label" : "Category 1 Non Member Senior Doctors – Post CCT\/CESR "
 }
*/
