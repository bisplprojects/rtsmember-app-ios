//
//  Option.swift
//  RTSPortal
//
//  Created by sajan on 05/08/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import SwiftyJSON

class Option: NSObject {

    var optionId : String?
    var name : String?
    var taxRate : String?
    var priceFieldId : String?
    var taxAmount : String?
    var label : String?
    var financialTypeId : String?
    var displayAmount : String?
    var amount : String?
    var nonDeductibleAmount : String?
    var isActive : String?
    var isDefault : String?

    var isOptionSelected : Bool?

    var isOptionRequired : Bool?

    var isAmendOptionSelected : Bool?

    init(dict:[String : JSON], isRequired:String) {
        
        self.optionId = dict["id"]?.stringValue
        self.name = dict["name"]?.stringValue
        self.taxRate = dict["tax_rate"]?.string
        self.priceFieldId = dict["price_field_id"]?.string
        self.taxAmount = dict["tax_amount"]?.stringValue
        self.label = dict["label"]?.string
        self.financialTypeId = dict["financial_type_id"]?.stringValue
        self.displayAmount = dict["display_amount"]?.stringValue
        self.amount = dict["amount"]?.string
        self.nonDeductibleAmount = dict["non_deductible_amount"]?.string
        self.isActive = dict["is_active"]?.string
        self.isDefault = dict["is_default"]?.stringValue
        
        self.isOptionSelected = false
        
        if isRequired == "1" {
            self.isOptionRequired = true
        }
        else{
            self.isOptionRequired = false
        }
        
        if self.isDefault == "1" {
            self.isAmendOptionSelected = true
        }
        else{
            self.isAmendOptionSelected = false
        }

        
    }

}

/*
 {
 "is_active" : "1",
 "weight" : "1",
 "display_amount" : "583.00",
 "financial_type_id" : "9",
 "name" : "full_meeting_category_1_non_member_consultant_consultant_geriatrician_gp_",
 "tax_rate" : "20.00000000",
 "id" : "374",
 "price_field_id" : "160",
 "tax_amount" : 97.166666599999999,
 "label" : "Full Meeting Senior Doctors – Post CCT\/CESR  ",
 "amount" : "485.833333",
 "non_deductible_amount" : "0.00",
 "is_default" : "0"
 }
*/
