//
//  Sig.swift
//  RTSPortal
//
//  Created by sajan on 31/07/19.
//  Copyright © 2019 BISPL. All rights reserved.
//

import UIKit
import SwiftyJSON

class Sig: NSObject {
    
    var sigId : String?
    var sigDisplayName : String?
    var sigImageURL : String?
    var sigSummary : String?
    var sigDescription : String?
    var canJoin : String?
    
    init(dict:[String : JSON]) {
        
        self.sigId = dict["sig_id"]?.stringValue
        self.sigDisplayName = dict["display_name"]?.stringValue
        self.sigImageURL = dict["image_URL"]?.string
        self.sigSummary = dict["summary"]?.string
    }

    init(chooseSigDict:[String : JSON]) {
        
        self.sigId = chooseSigDict["sig_id"]?.stringValue
        self.sigDisplayName = chooseSigDict["display_name"]?.stringValue
    }

    init(sigDetailDict:[String : JSON]) {
        
        self.sigId = sigDetailDict["sig_id"]?.stringValue
        self.sigDisplayName = sigDetailDict["display_name"]?.stringValue
        self.sigImageURL = sigDetailDict["image_URL"]?.string
        self.sigSummary = sigDetailDict["summary"]?.string
        self.sigDescription = sigDetailDict["description"]?.string
        self.canJoin = (sigDetailDict["can_join"]?.intValue == 1) ? "1" : "0"

    }

}

/*
{
"summary" : "summary goes here",
"sig_id" : "16434",
"image_URL" : "https:\/\/bgs1.vedacrm.co.uk\/sites\/default\/files\/styles\/800-16x9\/public\/content\/group\/images\/2018-10-11\/Cardiovascular.png",
"display_name" : "Anaemia"
}
*/

/*
 {
 "sig_id": "16435",
 "display_name": "Bladder and Bowel Health"
 }
*/

/*
 {
 "display_name" : "Cardiovascular Section",
 "sig_id" : "16436",
 "summary" : "summary goes here",
 "can_join" : 1,
 "image_URL" : "https:\/\/bgs1.vedacrm.co.uk\/sites\/default\/files\/styles\/800-16x9\/public\/content\/group\/images\/2018-10-11\/Cardiovascular.png",
 "description" : "description goes here"
 }
 */
